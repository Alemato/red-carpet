<?php

namespace Boot\Foundation\Bootstrappers;

use App\Support\RequestInput;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Jenssegers\Blade\Blade;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpMethodNotAllowedException;
use Slim\Exception\HttpNotFoundException;
use Throwable;
use Zeuxisoo\Whoops\Slim\WhoopsMiddleware;

class LoadDebuggingPage extends Bootstrapper
{
    public function boot()
    {
        $whoops = function ($exception, $inspector, $run) {
            $message = $exception->getMessage();
            $title = $inspector->getExceptionName();
            $stack = $exception->getTrace();
            $code = $exception->getCode();
            $blade = $this->app->resolve(Blade::class);
            if (!($exception instanceof HttpNotFoundException) && !($exception instanceof HttpMethodNotAllowedException)) {
                $input = ($this->app->resolve(RequestInput::class))->all();
            } else {
                $request = $exception->getRequest();
                $input = (new RequestInput($request, null))->all();
            }
            $with = compact('exception', 'inspector', 'code', 'title', 'message', 'input', 'stack');

            $debug_page = $blade->make('exceptions.whoops', $with)->render();
            $time = Carbon::now();
            $path = Str::kebab(storage_path("error_logs/{$time} {$title}.html"));

            $filesystem = app()->resolve(Filesystem::class);

            $filesystem->put($path, $debug_page);
            echo $debug_page;
            exit;

        };

        $whoopserror = function ($exception, $inspector, $run) {
            $message = $exception->getMessage();
            $titlepage = $exception->getTitle();
            $code = $exception->getCode();
            $blade = $this->app->resolve(Blade::class);
            $with = compact('exception', 'inspector', 'code', 'titlepage', 'message');
            $debug_page = $blade->make('exceptions.errori', $with)->render();
            echo $debug_page;
            exit;
        };



        if (env('APP_DEBUG', false)) {
            $this->app->bind('whoops', new WhoopsMiddleware([], [$whoops]));
            $this->app->add(app()->resolve('whoops'));
        } else {
            $this->app->bind('whoops', new WhoopsMiddleware([], [$whoopserror]));
            $this->app->add(app()->resolve('whoops'));
            //$this->app->addErrorMiddleware(false, true, true);
        }
    }
}