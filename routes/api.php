<?php

use App\Http\Middleware\AmminisatrazioneMiddleware;
use App\Http\Middleware\CrezioneModificaFilmMiddleware;
use App\Http\Middleware\CrezioneModificaSerieTvMiddleware;
use App\Http\Middleware\CrezioneModificaUtenzaMiddleware;
use App\Http\Middleware\GestioneRecensioiMiddleware;
use App\Http\Middleware\RedirectIfGuestMiddleware;

use App\Http\Middleware\CancellazioneFilmMiddleware;
use App\Http\Middleware\CancellazioneSerieTvMiddleware;
use App\Http\Middleware\CancellazioneFilmmakerMiddleware;
use App\Http\Middleware\CancellazioneGruppiMiddleware;
use App\Http\Middleware\CancellazioneUtenzaMiddleware;
use App\Support\Route;

// api/example
Route::get('/example', 'ApiController@index');
Route::get('/cast', 'CastApiController@show')->add(AmminisatrazioneMiddleware::class);

Route::delete('/admin/user_list/{idUser}', 'BackManageUserApiController@deleteUser')->add(CancellazioneUtenzaMiddleware::class)->add(AmminisatrazioneMiddleware::class);

Route::put('/admin/user_list/{idUser}', 'BackManageUserApiController@toggleStateUser')->add(CrezioneModificaUtenzaMiddleware::class)->add(AmminisatrazioneMiddleware::class);

Route::put('/admin/film_toggle_insala/{idFilm}', 'FilmApiController@toggleInSala')->add(CrezioneModificaFilmMiddleware::class)->add(AmminisatrazioneMiddleware::class);

Route::put('/admin/serietv_toggle_insala/{idSerieTv}', 'SerieTvApiController@toggleInSala')->add(CrezioneModificaSerieTvMiddleware::class)->add(AmminisatrazioneMiddleware::class);

Route::delete('/admin/modify_user/{idUser}', 'BackManageUserApiController@deleteImmage')->add(CrezioneModificaUtenzaMiddleware::class)->add(AmminisatrazioneMiddleware::class);

Route::delete('/admin/groups_services/{idGroup}', 'BackManageGroupApiController@deleteGroup')->add(CancellazioneGruppiMiddleware::class)->add(AmminisatrazioneMiddleware::class);

Route::delete('/admin/review_list/{idRecensione}', 'RecensioniApiController@deleteRecensione')->add(GestioneRecensioiMiddleware::class)->add(AmminisatrazioneMiddleware::class);

Route::delete('/review/{idRecensione}', 'RecensioniApiController@deleteRecensione');

Route::delete('/impostazioni_account', 'ImpostazioniAccountApiController@deleteImg');

Route::delete('/admin/delete/film/{idFilm}', 'FilmApiController@deleteFilm');

Route::delete('/admin/delete/serietv/{idSerieTv}', 'SerieTvApiController@deleteSerieTv');

Route::delete('/admin/delete/filmmaker/{idFilmmaker}', 'CastApiController@deleteFilmmaker');

Route::delete('/admin/delete/genere/{idGenere}', 'GenereApiController@deleteGenere');

Route::post('/recensione/film/{idFilm}', 'RecensioniApiController@storeRecensioneFilm');

Route::post('/recensione/serietv/{idFilm}', 'RecensioniApiController@storeRecensioneFilm');