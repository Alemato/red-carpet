<?php

use App\Http\Middleware\RedirectIfAuthenticatedMiddleware;
use App\Http\Middleware\RedirectIfGuestMiddleware;
use App\Support\Route;


Route::get('/', 'IndexFrontController@show');

Route::get('/ricerca_avanzata', 'RicercaAvanzataController@show');
Route::post('/risultati_ricerca', 'RisultatiRicercaController@show');
Route::get('/risultati_ricerca/{offset}', 'RisultatiRicercaController@show');
Route::get('/risultati_ricerca_veloce', 'RisultatiRicercaController@showRisultatiRicerca');

Route::post('/login', 'AuthController@store')->add(RedirectIfAuthenticatedMiddleware::class);
Route::get('/logout', 'AuthController@logout')->add(RedirectIfGuestMiddleware::class);

Route::get('/search_suggest', 'SearchApiController@showSuggest');
Route::get('/search_related', 'SearchApiController@showRelated');

Route::get('/registrazione','RegistrationController@userNormalShow')->add(RedirectIfAuthenticatedMiddleware::class);
Route::post('/registrazione','RegistrationController@userNormal')->add(RedirectIfAuthenticatedMiddleware::class);

Route::get('/registrazione_critico','RegistrationController@userCriticaShow')->add(RedirectIfAuthenticatedMiddleware::class);
Route::post('/registrazione_critico','RegistrationController@userCritica')->add(RedirectIfAuthenticatedMiddleware::class);

Route::get('/impostazioni_account', 'ImpostazioniAccount@show')->add(RedirectIfGuestMiddleware::class);
Route::post('/impostazioni_account/user_normal', 'ImpostazioniAccount@storeNormal')->add(RedirectIfGuestMiddleware::class);
Route::post('/impostazioni_account/user_critica', 'ImpostazioniAccount@storeCritica')->add(RedirectIfGuestMiddleware::class);
Route::post('/impostazioni_account/user_admin', 'ImpostazioniAccount@storeAdmin')->add(RedirectIfGuestMiddleware::class);

Route::get('/components/{componet}','ComponentsFrontController@show');
Route::get('/components/{componet}/{idSpecifico}','ComponentsFrontController@show');

Route::get('/film/{idFilm}', 'FilmShowController@show');
Route::get('/film/{idFilm}/{component}', 'FilmShowController@showComponent');

Route::get('/serietv/{idSerieTv}', 'SerieTvShowController@show');
Route::get('/serietv/{idSerieTv}/{component}', 'SerieTvShowController@showComponent');

Route::get('/lista_film', 'FilmAndSerieTvListController@showFilm');
Route::get('/lista_film/{limit}/{offset}', 'FilmAndSerieTvListController@showFilm');
Route::get('/lista_film/{tipo}', 'FilmAndSerieTvListController@showFilm');
Route::get('/lista_film/{tipo}/{limit}/{offset}', 'FilmAndSerieTvListController@showFilm');

Route::get('/lista_serietv', 'FilmAndSerieTvListController@showSerieTv');
Route::get('/lista_serietv/{limit}/{offset}', 'FilmAndSerieTvListController@showSerieTv');
Route::get('/lista_serietv/{tipo}', 'FilmAndSerieTvListController@showSerieTv');
Route::get('/lista_serietv/{tipo}/{limit}/{offset}', 'FilmAndSerieTvListController@showSerieTv');

Route::post('/recenzione_modal/film/{idFilm}', 'RecenzioneController@storeModalFilm');
Route::post('/recenzione_modal/serietv/{idSerieTv}', 'RecenzioneController@storeModalSerieTv');
Route::post('/recenzione_modal/{idRecenzione}', 'RecenzioneController@updateModal');

Route::get('/filmmaker/{idFilmmaker}', 'FilmmakerShowController@show');
Route::get('/filmmaker/{idFilmmaker}/{components}', 'FilmmakerShowController@components');

Route::get('/filmmaker_list/{tipologia}', 'FilmmakerListController@show');
Route::get('/filmmaker_list/{tipologia}/{offset}', 'FilmmakerListController@show');

Route::get('/premio/{idPremio}/{edizione}', 'PremioController@show');
Route::get('/premio/{idPremio}/{edizione}/{component}', 'PremioController@showComponent');

Route::get('/lista_recensioni/{entita}/{id}/{tipo}', 'ReviewListFrontController@show');
Route::get('/lista_recensioni/{entita}/{id}/{tipo}/{offset}', 'ReviewListFrontController@show');
Route::get('/lista_recensioni/{entita}/{id}/{tipo}/cerca', 'ReviewListFrontController@cerca');
Route::get('/lista_recensioni/{entita}/{id}/{tipo}/cerca/{offset}', 'ReviewListFrontController@cerca');

Route::get('/recensione/{idRecensione}', 'ReviewListFrontController@showModal');

Route::get('/lista_recensioni_personali', 'ReviewListFrontController@showPersonal');
Route::get('/lista_recensioni_personali/{offset}', 'ReviewListFrontController@showPersonal');
Route::get('/lista_recensioni_personal/cerca', 'ReviewListFrontController@cercaPersonal');
Route::get('/lista_recensioni_personal/cerca/{offset}', 'ReviewListFrontController@cercaPersonal');

Route::get('/lista_ultime_recensioni', 'ReviewListFrontController@showLast');
Route::get('/lista_ultime_recensioni/{offset}', 'ReviewListFrontController@showLast');
Route::get('/lista_ultime_recensione/cerca', 'ReviewListFrontController@cercaLast');
Route::get('/lista_ultime_recensione/cerca/{offset}', 'ReviewListFrontController@cercaLast');