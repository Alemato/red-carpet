<div class="modal-header">
    <h2 class="modal-title fs--18 m-0">
        Nuovo Distributore
    </h2>

    <button type="button" class="close pointer" data-dismiss="modal" aria-label="Close">
        <span class="fi fi-close fs--18" aria-hidden="true"></span>
    </button>
</div>

<!-- body -->
<div class="modal-body">
    <div class="accordion" id="accordionDropdownSignInUp">
        <div id="ajax_response_container"><!-- ajax response container --></div>

    </div>
    <form novalidate
          class="js-ajax bs-validate"
          method="post"
          action="{{asset('admin/modal/create_distributore')}}"
          id="distrib_form"
          data-ajax-container="#ajax_response_container"
          data-ajax-update-url="false"
          data-ajax-show-loading-icon="true"
    >
        @csrf
        <div class="form-label-group mb-3">
            <input placeholder="Nome distributore" id="nome_distributore" type="text" name="nome_distributore" value=""
                   class="form-control">
            <label for="nome_distributore">Nome Nuovo Distributore</label>
        </div>
        <div class="form-label-group mb-3">
            <input placeholder="Nome genere" id="descrizione_distributore" type="text" name="descrizione" value=""
                   class="form-control">
            <label for="descrizione">Descrizione</label>
        </div>

    </form>
</div>

<div class="modal-footer d-flex justify-content-between">
    <button type="button" id="distrib_an" class="btn btn-danger btn-lg btn-soft " data-dismiss="modal">
        Annulla
    </button>
    <button type="submit" id="distrib_sub" form="distrib_form" class="btn btn-success btn-lg btn-soft">
        Salva
    </button>
</div>
{{--<script>
    $(document).ready(function () {
        console.log($("#distrib_form"));
        console.log($("#distrib_form")[0][0]);
        console.log($("#distrib_form")[0][1]);
        $("#distrib_form")[0][0].remove();
        $("#distrib_form")[0][0].remove();
        let csrf = '@csrf';
        $("#distrib_form").append(csrf);
        //let c = $(csrf);
        //$("#distrib_form").append(c[0]);
        //$("#distrib_form").append(c[1]);
        console.log($("#distrib_form"));
        /*console.log($("#distrib_form")[0][0]);
        console.log($("#distrib_form")[0][1]);*/
    });
</script>--}}

