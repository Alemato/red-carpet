<!-- AJAX CONTENT -->
<div class="modal-header">
    <h2 class="modal-title fs--18 m-0 text-primary">
        Accedi
    </h2>

    <button type="button" class="close pointer" data-dismiss="modal" aria-label="Close">
        <span class="fi fi-close fs--18" aria-hidden="true"></span>
    </button>
</div>

<div class="modal-body p--30">

    <div class="row">

        <div class="col-12 col-md-6">

            <div class="pb--30 pt--30">
                <img class="img-fluid" src="{{asset("assets/images/undraw_mention_6k5d.svg")}}" alt="...">
            </div>
        </div>

        <div class="col-12 col-md-6">

            <div class="accordion" id="accordionDropdownSignInUp">

                <div id="ajax_response_container"><!-- ajax response container --></div>

                <!-- SIGN IN -->
                <form novalidate
                      class="js-ajax bs-validate"
                      method="post"
                      action="{{asset('login')}}"
                      id="accedi_form"
                      data-ajax-container="#ajax_response_container"
                      data-ajax-update-url="false"
                      data-ajax-show-loading-icon="true"

                      data-error-scroll-up="true"
                      data-error-toast-text="<i class='fi fi-circle-spin fi-spin float-start'></i> Perfavore rimempi i campi!"
                      data-error-toast-delay="2000"
                      data-error-toast-position="top-center"

                      data-error-scroll-up="false"
                      data-ajax-callback-function=""
                >
                    @csrf
                    <div class="p--30 pt--0">

                        <div class="form-label-group mb-4">
                            <input required placeholder="Email" id="signin_email" type="email" name="email"
                                   class="form-control form-control-clean">
                            <label for="signin_email">Email</label>
                        </div>

                        <div class="form-label-group mb-4">
                            <input required placeholder="Password" id="signin_password" name="password" type="password"
                                   class="form-control form-control-clean pr--27" autocomplete="new-password">
                            <label for="signin_password">Password</label>
                            <i class="fi fi-fw fi-eye field-icon toggle-password"></i>
                        </div>

                        <button type="submit" class="btn btn-primary btn-soft btn-block">
                            Accedi
                        </button>

                        <div class="text-center mt--30">
                            <a href="{{asset('registrazione')}}"
                               class="d-block text-gray-600 text-decoration-none text-primary-hover">
                                Registrati
                            </a>
                        </div>

                        <div class="text-center mt--30">
                            <a href="{{asset('registrazione_critico')}}"
                               class="d-block text-gray-600 text-decoration-none text-primary-hover">
                                Registrati come Critico
                            </a>
                        </div>

                    </div>
                </form>

            </div>

        </div>

    </div>

</div>

<div class="border-top text-muted p--15 fs--13 bg-light rounded-bottom pl--30 pr--30 pt--20 pb--20">
    <b>Privacy:</b> Tutti i dati sono al sicuro, mai trasferiti a un fornitore / servizio di terze parti.
</div>
<script>
    $(document).ready(function () {
        console.log("ready!");
        $(".toggle-password").click(function () {

            $(this).toggleClass("fi-eye fi-eye-disabled");
            var input = $(this).parent().children().first();
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
        console.log("ready2!");
    });
</script>