@foreach($lastUsers as $user)

    @if($user->getTipologiaUtenza() == 0)
    <!-- User -->
    <a href="{{asset('admin/modify_normal_user/').$user->getIdUser()}}"
       class="clearfix dropdown-item font-weight-medium p-3 rounded overflow-hidden border-bottom border-light">

        <!-- badge -->
        <span class="badge badge-success float-end font-weight-normal mt-1">Utente Normale</span>

        @if($user->getAvatar() == new \App\Model\Immagine())
        <!-- icon -->
            <i class="fi fi-user-male d-middle float-start fs--18 bg-light w--40 h--40 rounded-circle text-center"></i>
        @else
        <!-- profile image -->
            <!-- avatar -->
            <i class="float-start w--40 h--40 rounded-circle d-inline-block bg-light bg-cover lazy"
               data-background-image="{{$user->getAvatar()->getDato()}}"></i>
        @endif



    @elseif($user->getTipologiaUtenza() == 1)
    <!-- User -->
    <a href="{{asset('admin/modify_critico_user/').$user->getIdUser()}}"
       class="clearfix dropdown-item font-weight-medium p-3 rounded overflow-hidden border-bottom border-light">

        <!-- badge -->
        <span class="badge badge-success float-end font-weight-normal mt-1">Utente Critico</span>

        @if($user->getAvatar() == new \App\Model\Immagine())
        <!-- icon -->
            <i class="fi fi-user-male d-middle float-start fs--18 bg-light w--40 h--40 rounded-circle text-center"></i>
        @else
        <!-- profile image -->
            <i class="float-start w--40 h--40 rounded-circle d-inline-block bg-light bg-cover lazy"
               data-background-image="{{$user->getAvatar()->getDato()}}"></i>
        @endif




    @else
    <!-- User -->
    <a href="{{asset('admin/modify_admin_user/').$user->getIdUser()}}"
       class="clearfix dropdown-item font-weight-medium p-3 rounded overflow-hidden border-bottom border-light">

        <!-- badge -->
        <span class="badge badge-warning float-end font-weight-normal mt-1">Amministratore</span>

        @if($user->getAvatar() == new \App\Model\Immagine())
        <!-- icon -->
            <i class="fi fi-user-male d-middle float-start fs--18 bg-light w--40 h--40 rounded-circle text-center"></i>
        @else
        <!-- profile image -->
            <i class="float-start w--40 h--40 rounded-circle d-inline-block bg-light bg-cover lazy"
               data-background-image="{{$user->getAvatar()->getDato()}}"></i>
        @endif



    @endif




    <!-- NOTIFICATION -->
        <p class="fs--14 m-0 text-truncate font-weight-normal">
            {{$user->getNome()}} {{$user->getCognome()}}
        </p>

        <!-- date -->
        <small class="d-block fs--11 text-muted">
            Registrato: {{italian_date_str($user->createDate)}} {{date('G:i:s',strtotime($user->createDate))}}
        </small>

    </a>
    <!-- /User -->
@endforeach