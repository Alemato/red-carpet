<div class="modal-header">
    <h2 class="modal-title fs--18 m-0">
        Nuovo Genere
    </h2>

    <button type="button" class="close pointer" data-dismiss="modal" aria-label="Close">
        <span class="fi fi-close fs--18" aria-hidden="true"></span>
    </button>
</div>

<!-- body -->
<div class="modal-body">
    <div class="accordion" id="accordionDropdownSignInUp">
        <div id="ajax_response_container"><!-- ajax response container --></div>

    </div>
    <form novalidate
          class="js-ajax bs-validate"
          method="post"
          action="{{asset('admin/modal/create_genere')}}"
          id="generi_form"
          data-ajax-container="#ajax_response_container"
          data-ajax-update-url="false"
          data-ajax-show-loading-icon="true"
    >
        @csrf
        <div class="form-label-group mb-3">
            <input placeholder="Nome genere" id="nome_genere" type="text" name="nome_genere" value=""
                   class="form-control">
            <label for="nome_genere">Nome Nuovo Genere</label>
        </div>
        <div class="form-label-group mb-3">
            <input placeholder="Nome genere" id="descrizione" type="text" name="descrizione" value=""
                   class="form-control">
            <label for="descrizione">Descrizione</label>
        </div>

    </form>
</div>

<div class="modal-footer d-flex justify-content-between">
    <button type="button" class="btn btn-danger btn-lg btn-soft " data-dismiss="modal">
        Annulla
    </button>
    <button type="submit" form="generi_form" class="btn btn-success btn-lg btn-soft">
        Salva
    </button>
</div>
.
