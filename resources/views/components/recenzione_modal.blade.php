<div class="clearfix mt--5 p--15">
    <div class="row">
        <h3 class="font-weight-light mb-4 col-11">
            Recensisci: The Avengers
        </h3>

        <button type="button" class="close pointer col" data-dismiss="modal" aria-label="Close">
            <span class="fi fi-close fs--18" aria-hidden="true"></span>
        </button>
    </div>
    <!-- already logged in -->
    <span class="d-block text-muted fs--12 mb-1">
									Loggato come: <b>{{$user->getNome()}} {{$user->getCognome()}}</b>.
									non sei tu? Perfavore effetua il <a rel="nofollow" href="{{asset('logout')}}">sign out</a>
								</span>

    <!-- Write Comment Form -->
    <form id="recenzione_form" novalidate method="POST" action="@if($recenzioneObj->getIdRecensione() == null){{asset('recenzione_modal'.$path)}}@else{{asset('recenzione_modal/'.$recenzioneObj->getIdRecensione())}}@endif"
          class="js-ajax bs-validate d-block bg-white shadow-primary-xs rounded p-3 mb-5"
          data-ajax-container="#ajax_response_container"
          data-ajax-update-url="false"
          data-ajax-show-loading-icon="true"

          data-error-scroll-up="true"
          data-error-toast-text="<i class='fi fi-circle-spin fi-spin float-start'></i> Perfavore rimempi i campi!"
          data-error-toast-delay="2000"
          data-error-toast-position="top-center"

          data-error-scroll-up="false"
          data-ajax-callback-function="">
        <div id="ajax_response_container"><!-- ajax response container --></div>
        @csrf
        <div class="row">

            <div class="col-12 col-md-6">

                <div class="form-label-group mb-3">
                    <input type="hidden" name="idRecenzione" @if($recenzioneObj->getIdRecensione() != null) value="{{$recenzioneObj->getIdRecensione()}}" @else value="" @endif>
                    <input  placeholder="Titolo" id="comment_name" type="text" name="titolo" @if($recenzioneObj->getTitolo() != null) value="{{$recenzioneObj->getTitolo()}}" @else value="" @endif class="form-control">
                    <label for="comment_name">Titolo</label>
                </div>

            </div>

            <div class="col-12 col-md-6">

                <div class="form-label-group">
                    <label for="formControlRange">Valutazione complessiava: <i id="rank"
                                                                               class="rating-1_0 text-warning d-inline"></i>
                        <span class="font-weight-bold text-primary mt-1 valueSpan"></span> stelle</label>
                    <input type="range" class=" custom-range" min="1" max="5" step="1" @if($recenzioneObj->getVoto() != null) value="{{$recenzioneObj->getVoto()}}" @else value="1" @endif
                           id="formControlRange" name="valutazione" required>
                </div>
                <script>
                    $(document).ready(function () {

                        const $valueSpan = $('.valueSpan');
                        const $rank = $('#rank');
                        const $value = $('#formControlRange');
                        $valueSpan.html($value.val());
                        switch ($value.val()) {
                            case "0":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-0_0");
                                break;
                            case "0.5":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-0_5");
                                break;
                            case "1":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-1_0");
                                break;
                            case "1.5":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-1_5");
                                break;
                            case "2":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-2_0");
                                break;
                            case "2.5":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-2_5");
                                break;
                            case "3":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-3_0");
                                break;
                            case "3.5":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-3_5");
                                break;
                            case "4":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-4_0");
                                break;
                            case "4.5":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-4_5");
                                break;
                            case "5":
                                $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                $rank.addClass("rating-5_0");
                                break;
                        }
                        $value.on('input change', () => {
                            $valueSpan.html($value.val());
                            switch ($value.val()) {
                                case "0":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-0_0");
                                    break;
                                case "0.5":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-0_5");
                                    break;
                                case "1":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-1_0");
                                    break;
                                case "1.5":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-1_5");
                                    break;
                                case "2":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-2_0");
                                    break;
                                case "2.5":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-2_5");
                                    break;
                                case "3":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-3_0");
                                    break;
                                case "3.5":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-3_5");
                                    break;
                                case "4":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-4_0");
                                    break;
                                case "4.5":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-4_5");
                                    break;
                                case "5":
                                    $rank.removeClass("rating-0_0 rating-0_5 rating-1_0 rating-1_5 rating-2_0 rating-2_5 rating-3_0 rating-3_5 rating-4_0 rating-4_5 rating-5_0");
                                    $rank.addClass("rating-5_0");
                                    break;
                            }
                        });
                    });
                </script>
            </div>

        </div>


        <div class="clearfix mb-3">

            <div class="form-label-group">
											<textarea  rows="3" id="comment_description"
                                                      data-output-target=".js-form-advanced-char-left"
                                                      class="form-control js-form-advanced-char-count-down"
                                                      maxlength="3000" placeholder="La tua recenzione" name="contenuto" >@if($recenzioneObj->getContenutoRecensione() != null) {{$recenzioneObj->getContenutoRecensione()}} @endif</textarea>
                <label for="comment_description">La tua recenzione</label>
            </div>

            <span class="fs--12 text-muted text-align-end float-end mt-1">
											characters left: <span class="js-form-advanced-char-left">3000</span>
										</span>

        </div>

        <button type="submit" class="btn btn-primary btn-sm">
            Invia recenzione
        </button>
    </form>
    <!-- /Write Comment Form -->
</div>