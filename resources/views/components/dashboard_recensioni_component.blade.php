@isset($lastReview)
    @foreach($lastReview as $review)
        <!-- item -->
        <a href="#">
            <div class="p-3 border-bottom border-light">

                <!-- avatar -->
                <div class="clearfix mb-2 py-1">

                @if($review->getUtente()->getAvatar() != new \App\Model\Immagine())
                    <!-- avatar -->
                        <a href="#" class="float-start w--50 h--50 rounded-circle d-inline-block bg-light bg-cover lazy"
                           data-background-image="{{$review->getUtente()->getAvatar()->getDato()}}"></a>
                    @else
                        <a href="" class="w--50 h--50 btn bg-light bg-cover rounded-circle float-start ">
                            <i class="fi fi-user-male"></i>
                        </a>
                @endif

                <!-- name / time -->
                    <span class="float-start px-4 mt-1">
												Mark Cubanna, <span class="text-primary">{{$review->getTitolo()}}</span>
                                                @if($review->filmOrSerietv instanceof \App\Model\Film || $review->filmOrSerietv instanceof \App\Model\SerieTv)
                            <small class="d-block text-muted">{{tempo_fa($review->getDataRecensione())}}, per il titolo: {{$review->filmOrSerietv->getTitolo()}}</small>
                        @endif
											</span>

                </div>

                <p class="fs--14 mb-2 pt-1">
                    {{substr($review->getContenutoRecensione(), 0,150)}}...
                </p>

            </div>
        </a>
        <!-- /item -->
    @endforeach
@endisset