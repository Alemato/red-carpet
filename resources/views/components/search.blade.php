@section('search-component')
    <!-- SEARCH -->
    <form action="{{ asset('risultati_ricerca_veloce') }}"
          method="GET"
          data-autosuggest="on"
          data-container="#sow-search-container"
          data-input-min-length="2"
          data-input-delay="300"
          data-related-keywords=""
          data-related-url="{{ asset('search_related') }}"
          data-suggest-url="{{ asset('search_suggest') }}"
          data-related-action="related_get"
          data-suggest-action="suggest_get"
          class="js-ajax-search sow-search sow-search-over hide d-inline-flex">
        <div class="sow-search-input w-100">

            <div class="input-group-over d-flex align-items-center w-100 h-100 rounded shadow-md">

                <input placeholder="cosa vuoi cercare oggi?" name="search" type="text"
                       class="form-control-sow-search form-control form-control form-control-lg shadow-xs"
                       value="" autocomplete="off">

                <span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit"
                                            class="btn btn-primary btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 b--0 bg-transparent text-muted">
										<i class="fi fi-search fs--20"></i>
									</button>

                    <!-- close : mobile only (d-inline-block d-lg-none) -->
									<a href="javascript:;"
                                       class="btn-sow-search-toggler btn btn-light btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

            </div>

        </div>

        <!-- search suggestion container -->
        <div class="sow-search-container w-100 p--0 hide shadow-md" id="sow-search-container">
            <div class="sow-search-container-wrapper">

                <!-- main search container -->
                <div class="sow-search-loader p--15 text-center hide">
                    <i class="fi fi-circle-spin fi-spin text-muted fs--30"></i>
                </div>

                <!--
                    AJAX CONTENT CONTAINER
                    SHOULD ALWAYS BE AS IT IS : NO COMMENTS OR EVEN SPACES!
                -->
                <div class="sow-search-content rounded w-100 scrollable-vertical"></div>

            </div>
        </div>
        <!-- /search suggestion container -->

        <!--

            overlay combinations:
                overlay-dark opacity-* [1-9]
                overlay-light opacity-* [1-9]

        -->
        <div class="sow-search-backdrop overlay-dark opacity-3 hide"></div>

    </form>
    <!-- /SEARCH -->
@endsection