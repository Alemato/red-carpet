@if(count($film->getVideos())>0)
<div class="row">
    @foreach($film->getVideos() as $kv => $video)
    <div class="col-12 col-lg-4">
        <a class="d-inline-block fancybox overlay-dark overlay-opacity-4 " href="#trailer{{$video->getIdVideo()}}">
            <img class="img-fluid" src="{{asset($video->getImmagine()->getDato())}}" alt="{{$video->getImmagine()->getNome()}}"/>

            <!-- play button -->
            <span class="absolute-full d-flex align-items-center justify-content-center">
		<span class="d-inline-flex bg-white text-dark rounded-circle w--70 h--70 align-items-center justify-content-center shadow-lg">
			<i class="fi fi-arrow-end-full line-height-1 fs--30 mt--2"></i>
		</span>
	</span>
        </a>

        <video class="hide img-fluid" controls id="trailer{{$video->getIdVideo()}}">
            <source src="{{asset($video->getDato())}}" type="video/mp4">
            Your browser doesn't support HTML5 video tag.
        </video>
    </div>
    @endforeach
</div>
@else
<div class="h--300 d-flex align-items-center">
    <h1 class="font-weight-100 text-justify text-center">Questo Titolo non ha trailer</h1>
</div>
@endif