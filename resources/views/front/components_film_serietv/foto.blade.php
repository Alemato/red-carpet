@if(count($array_foto) > 0)
    @if(count($array_foto[0]) > 0)
        @foreach($array_foto as $kaf => $aimg)
            <div class="row pl--15 pr--10 mb--15">
                @foreach($aimg as $ki => $img)
                    <div class="col-12 col-lg-3 pl--0 pr--5">
                        <div class="card h-100">
                            <div class="card-body">
                                <a href="#"
                                   class="d-block clearfix shadow-xs text-decoration-none position-relative bg-white transition-all-ease-250 rounded overflow-hidden show-hover-container">


                                    <a class="fancybox fancybox-secondary" data-fancybox="gallery"
                                       href="{{asset($img->getDato())}}">
                                        <img class="img-fluid " src="{{asset($img->getDato())}}"
                                             alt="{{$img->getNome()}}"
                                             width="299"/>
                                    </a>


                                </a>
                                <p class="card-text text-center pt--15">
                                    {{$img->getDescrizione()}}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    @else
        <div class="h--300 d-flex align-items-center">
            <h1 class="font-weight-100 text-justify text-center col">Questo Titolo non ha Foto</h1>
        </div>
    @endif
@else
    <div class="h--300 d-flex align-items-center">
        <h1 class="font-weight-100 text-justify text-center col">Questo Titolo non ha Foto</h1>
    </div>
@endif
