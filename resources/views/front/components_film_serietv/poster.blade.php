@if(count($posters[0]) > 0)
@foreach($posters as $kp => $poster)
<div class="row pl--15 mb--5">
    @foreach($poster as $ki => $img)
    <div class="col-12 col-lg-2 pl--0 pr--5">
        <div class="bg-white border rounded pl--15 pr--15">
            <a class="fancybox fancybox-secondary" data-fancybox="gallery"
               href="{{asset($img->getDato())}}">
                <img class="img-fluid pt--15 pb--15 h--275" src="{{asset($img->getDato())}}" alt="{{$img->getNome()}}" />
            </a>
        </div>
    </div>
    @endforeach
</div>
@endforeach
@else
    <div class="h--300 d-flex align-items-center">
        <h1 class="font-weight-100 text-justify text-center col">Questo Titolo non ha Poster</h1>
    </div>
@endif


