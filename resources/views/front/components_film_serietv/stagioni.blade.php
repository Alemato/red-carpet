<div class="row">
    @foreach($film->getStagioni() as $k=>$stagione)
    <div class="col-12 mt--10 mb--5">
        <div class="rounded bg-primary">
            <div class="row" data-toggle="collapse" href="#stagione{{$k}}" aria-expanded="true"
                 aria-controls="stagione1">
                <div class="col-12">
                    <h4 class="text-secondary-palette pt--10 pl--15">Stagione {{$stagione->getNumero()}} - {{count($stagione->getEpisodi())}} episodi

                        <div class="float-end mr--15">
                        <span class="group-icon">
			                <i class="fi fi-arrow-end-slim"></i>
			                <i class="fi fi-arrow-down-slim"></i>
		                </span>
                        </div>
                    </h4>
                </div>
            </div>
        </div>
    </div>


    <div class="collapse mt--10 pr--15 pl--15 show w-100" id="stagione{{$k}}">
        @foreach($stagione->getEpisodi() as $episodio)
        <div class="card card-body mb--10">
            <div class="row">
                <div class="col-auto d-flex align-items-center pr--0">
                    <span class="fs--50 font-weight-lighter font-raleway text-gray-700">{{$episodio->getNumero()}}</span>
                </div>
                <div class="col-auto">
                    <img src="{{asset($episodio->getImg()->getDato())}}" alt="{{$episodio->getImg()->getNome()}}">
                </div>
                <div class="col">
                    <h4 class="font-weight-bold text-gray-800">{{$episodio->getTitolo()}} <span class="float-end">{{$episodio->getDurata()}} min</span></h4>
                    <p class="fs--20">{{$episodio->getDescrizione()}}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    @endforeach
</div>

