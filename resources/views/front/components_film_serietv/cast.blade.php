<div class="row">
    <div class="col-12 col-lg-9">

        @foreach($cast as $row)
        <div class="row mb--15 pl--15">
            @foreach($row as $ruolo)
            <div class="col-12 col-lg-3 pl--0 pr--5">
                <div class="card h-100 ">
                    <div class="card-body">
                        <a href="{{asset('filmmaker/').$ruolo->getFilmMaker()->getIdFilmMaker()}}"
                           class="d-block clearfix p-2 shadow-xs text-decoration-none position-relative bg-white transition-all-ease-250 rounded overflow-hidden show-hover-container">

                            <div class="p-2 px-2 m-0 text-center bg-light-radial rounded position-relative z-index-1 overlay-dark-hover overlay-opacity-3">

                                <!-- image -->
                                <img src="{{asset($ruolo->getFilmMaker()->getImgAvatar()->getDato())}}" alt="{{$ruolo->getFilmMaker()->getImgAvatar()->getDescrizione()}}"
                                     class="img-fluid bg-suprime opacity--9">

                            </div>

                        </a>
                        <h5 class="card-title text-center"><a href="{{asset('filmmaker/').$ruolo->getFilmMaker()->getIdFilmMaker()}}">{{$ruolo->getFilmMaker()->getNome()}} {{$ruolo->getFilmMaker()->getCognome()}}</a></h5>
                        <p class="card-text text-center">
                            {{$ruolo->getRuoloInternoOpera()}}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach



        @if(count($filmmaker) > 0)
        <div class="row pl--15 pr--3 mb--25">
            <div class="col-12 bg-primary rounded">
                <h5 class="text-white mt--8">Filmmakers</h5>
            </div>
        </div>

        @foreach($filmmaker as $row)
        <div class="row mb--15 pl--15">
            @foreach($row as $ruolo)
            <div class="col-12 col-lg-3 pl--0 pr--5">
                <div class="card h-100 ">
                    <div class="card-body">
                        <a href="{{asset('filmmaker/').$ruolo->getFilmMaker()->getIdFilmMaker()}}"
                           class="d-block clearfix p-2 shadow-xs text-decoration-none position-relative bg-white transition-all-ease-250 rounded overflow-hidden show-hover-container">

                            <div class="p-2 px-2 m-0 text-center bg-light-radial rounded position-relative z-index-1 overlay-dark-hover overlay-opacity-3">

                                <!-- image -->
                                <img src="{{asset($ruolo->getFilmMaker()->getImgAvatar()->getDato())}}" alt="{{$ruolo->getFilmMaker()->getImgAvatar()->getDescrizione()}}"
                                     class="img-fluid bg-suprime opacity--9">

                            </div>

                        </a>
                        <h5 class="card-title text-center"><a href="{{asset('filmmaker/').$ruolo->getFilmMaker()->getIdFilmMaker()}}">{{$ruolo->getFilmMaker()->getNome()}} {{$ruolo->getFilmMaker()->getCognome()}}</a></h5>
                        <p class="card-text text-center">
                            {{$ruolo->getRuoloInternoOpera()}}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach

        @endif
    </div>

    <div class="col-12 col-lg-3 ">
        <div class="col-12 col-lg mb-4 pr--5 h-100 ">
            <div class="card">
                <div class="card-body mt--30">
                    <!-- Nested -->
                    <div class="m-auto" style="width: fit-content;">
                        <div class="easypie d-inline-block position-relative m-auto"
                             data-bar-color="#bb3132"
                             data-track-color="#eaeaea"
                             data-scale-color="#cccccc"
                             data-scale-length="0"
                             data-line-width="6"
                             data-line-cap="round"

                             data-size="160"
                             data-percent="{{$valutazione_critica}}"
                        >
                            <div class="absolute-full d-middle pt-0 pb-2">
                                <div class="flex-none text-center">
                                    <small class="d-block">{{$valutazione_critica_s}}/{{$valutazione_user_s}}</small>
                                </div>
                            </div>

                            <!-- Second -->
                            <div class="easypie d-inline-block position-absolute mt--15 ml--15 mr--15"
                                 data-bar-color="#bc827b"
                                 data-track-color="#eaeaea"
                                 data-scale-color="#cccccc"
                                 data-scale-length="0"
                                 data-line-width="6"
                                 data-line-cap="round"

                                 data-size="130"
                                 data-percent="{{$valutazione_user}}"></div>

                        </div>
                    </div>

                    <h6 class="card-title text-center text-uppercase mt--12 mb--12 fs--15">smartometro</h6>
                    <p class="card-text text-center m-auto">
                        Indice di gradimento medio del film tra pubblico e critica
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>
