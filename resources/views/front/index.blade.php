@extends('skeletons.front.app')

@section('content')
                    <!-- TEMPORARY SECTION -->
                    @if(session()->flash()->has('errorPermission'))
                        <div class="hide toast-on-load"
                             data-toast-type="danger"
                             data-toast-title="Informazione"
                             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-dislike float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorPermission'))[0] }}</span></div></div>"
                             data-toast-pos="top-center"
                             data-toast-delay="2000"
                             data-toast-fill="true"
                             data-
                        ></div>
                @endif
                <!-- COVER -->
                    <section class="pt--30 bg-light px-3 pb-3">
                        <div class="container min-h-33vh d-middle pt-4">
                            <div class="row text-center-xs">
                                <div class="col-12 col-md-7 order-2 order-md-1 pb-3" data-aos="fade-in"
                                     data-aos-delay="0">
                                    <div class="mb-3 mt-5">
                                        <h1 class="font-weight-light mb-3">
                                            Benvenuti in <span class="text-primary font-weight-medium">Smarty</span><br>
                                            <span class="font-weight-medium">Il cinema parte dal pubblico!</span>
                                        </h1>
                                        <p class="lead">
                                            Database di tutti i film, recensioni, critica, nuove uscite,
                                            cast completo, produzione, durata da oltre un secolo.<br>
                                            Tutto quello che vuoi sapere su <b>film</b> e <b>serie tv</b>!
                                        </p>
                                        <div class="mt-4">
                                            <a href="{{asset('lista_film/inuscita')}}"
                                               class="btn btn-primary transition-hover-top mb-3 d-block-xs">
                                                In uscita
                                            </a>
                                            <a href="{{asset('lista_ultime_recensioni')}}"
                                               class="btn btn-primary transition-hover-top btn-soft mb-3 d-block-xs">
                                                Ultime recensioni
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-5 order-1 order-md-2 pb-3 d-flex justify-content-center"
                                     data-aos="fade-in" data-aos-delay="200">

                                    <!-- svg image -->
                                    <img width="350" height="200" class="img-fluid lazy"
                                         src="{{ asset('assets/img/home/smarty-high-resolution-logo-color-on-transparent-background.png') }}" alt="...">
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /COVER -->

                    <!-- COUNTER -->
                    <section class="pt--50 bg-light pb--50">
                        <div class="bg-white">
                            <div class="container shadow-xs">
                                <div class="row col-border text-center text-muted">
                                    <div class="col-6 col-md-3 mt-3 mb-3 font-raleway">
                                        <div class="h1">
								<span data-toggle="count"
                                      data-count-from="0"
                                      data-count-to="{{$filmSerieTvTot}}"
                                      data-count-duration="3000"
                                      data-count-decimals="0">0</span><span class="font-weight-light">+</span>
                                        </div>
                                        <h3 class="h6 m-0">FILM E SERIE TV</h3>
                                    </div>
                                    <div class="col-6 col-md-3 mt-3 mb-3 font-raleway">
                                        <div class="h1">
								<span data-toggle="count"
                                      data-count-from="0"
                                      data-count-to="{{$totRec}}"
                                      data-count-duration="2800"
                                      data-count-decimals="0">0</span><span class="font-weight-light">+</span>
                                        </div>
                                        <h3 class="h6 m-0">RECENSIONI</h3>
                                    </div>
                                    <div class="col-6 col-md-3 mt-3 mb-3 b--0-xs font-raleway">
                                        <div class="h1">
								<span data-toggle="count"
                                      data-count-from="0"
                                      data-count-to="{{$totUtenti}}"
                                      data-count-duration="2300"
                                      data-count-decimals="0">0</span><span class="font-weight-light">+</span>
                                        </div>
                                        <h3 class="h6 m-0">UTENTI REGISTRATI</h3>
                                    </div>
                                    <div class="col-6 col-md-3 mt-3 mb-3 font-raleway">
                                        <div class="h1">
                            <span data-toggle="count"
                                  data-count-from="0"
                                  data-count-to="31"
                                  data-count-duration="1500"
                                  data-count-decimals="0">0</span><span class="font-weight-light">/30</span>
                                        </div>
                                        <h3 class="h6 m-0">RATED</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /COUNTER -->

                    <!-- CAROSELLO 1 FILM IN SALA-->
                    <section class="p-3 bg-c-6f3" data-aos="zoom-in-up" data-aos-once="false">
                        <div class="container bg-c-6f3 mt--50">
                            <div class="mt--25">
                                <div class="row">
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                    <div class="column col-8">
                                        <h1 class="fs&#45;&#45;30 text-center text-primary"> FILM IN SALA</h1>
                                    </div>
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                </div>
                                <div class="swiper-container p--30" data-swiper='{
		"slidesPerView": 4,
		"spaceBetween": 0,
		"slidesPerGroup": 4,
		"loop": false,
		"autoplay": false
	}'>

                                    <div class="swiper-wrapper hide">
                                        <!-- SLIDE 1 -->
                                        @foreach($filmInSala as $k=>$filmSala)
                                            <div class="swiper-slide">
                                                <div class="col-12">
                                                    <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                                        <div class="clearfix p--15">
                                                            <img width="217.5" height="321.891"
                                                                 src="{{ asset($filmSala->getImgCopertina()->getDato()) }}"
                                                                 alt="...">
                                                        </div>
                                                        <div class="card-body font-weight-light pt--0 pb--15">
                                                            <div class="d-table">
                                                                <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($filmSala->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">@if($filmSala->mediaVotoRedCarpet == 0)
                                                N.D @else {{$filmSala->mediaVotoRedCarpet}} @endif</span>
                                    </span>
                                                                    <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-decoration-none text-dark"
                                                                       href="{{asset('film/' . $filmSala->getIdFilm())}}">
                                                                        {{$filmSala->getTitolo()}}
                                                                    </a>
                                                                    <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                                        {{$filmSala->getDescrizione()}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="card-footer bg-transparent b-0 pt--4">
                                                            <hr class="mt--0 mb--15 bg-primary">
                                                            <p class="text-center mb--4 fs--20 text-dark">
                                                                {{date_format(date_create($filmSala->getDataUscita()), 'd/m/Y') }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div> <!-- QUA FINISCE LO SWIPER-WRAPPER -->
                                    <!-- FRECCIE -->
                                    <div class="swiper-button-next swiper-button-redcarper-dx"></div>
                                    <!-- BOTTONE DESTRO -->
                                    <div class="swiper-button-prev swiper-button-redcarper-sx"></div>
                                    <!-- BOTTONE SINISTRO -->
                                </div>
                            </div>

                        </div>
                    </section>

                    <!-- FILM MESE IN CORSO -->
                    <section class="bg-c-6f3" data-aos="zoom-in" data-aos-once="false">
                        <div class="shadow-xs pt-2 pb-2 bg-white">
                            <div class="container">
                                <div class="row max-h-250">
                                    <div class="col-12 col-md-4 d-flex justify-content-center">
                                        <!-- svg image -->
                                        <img width="180" height="180" class="lazy rounded-circle"
                                             src="{{asset('assets/img/home/nastro_pellicola.jpg')}}" alt="...">
                                    </div>
                                    <div class="col-12 col-md-4 d-flex align-items-center justify-content-center pl--0">
                                        <h2 class="font-weight-light font-raleway">
                                            Cosa guardare
                                        </h2>
                                    </div>
                                    <div class="col-12 col-md-4 d-flex align-items-center justify-content-end pl--0">
                                        <a href="{{asset('lista_film/mese')}}"
                                           class="btn transition-hover-top d-block-xs bg-primary text-white fs--20 pl--40 pr--40">
                                            Film mese in corso
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- CAROSELLO 2 SERIE TV IN CORSO-->
                    <section class="p-3 bg-c-6f3" data-aos="zoom-in-up" data-aos-once="false">
                        <div class="container bg-c-6f3">
                            <div class="mt--25">
                                <div class="row">
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                    <div class="column col-8">
                                        <h1 class="fs&#45;&#45;30 text-center text-primary"> SERIE TV IN CORSO</h1>
                                    </div>
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                </div>
                                <div class="swiper-container p--30" data-swiper='{
		"slidesPerView": 4,
		"spaceBetween": 0,
		"slidesPerGroup": 4,
		"loop": false,
		"autoplay": false
	}'>

                                    <div class="swiper-wrapper hide">
                                        <!-- SLIDE 1 -->
                                        @foreach($serieTvInCorso as $ke=>$stvCorso)
                                            <div class="swiper-slide">
                                                <div class="col-12">
                                                    <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                                        <div class="clearfix p--15">
                                                            <img width="217.5" height="321.891"
                                                                 src="{{ asset($stvCorso->getImgCopertina()->getDato()) }}"
                                                                 alt="...">
                                                        </div>
                                                        <div class="card-body font-weight-light pt--0 pb--15">
                                                            <div class="d-table">
                                                                <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($stvCorso->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">@if($stvCorso->mediaVotoRedCarpet == 0)
                                                N.D. @else {{$stvCorso->mediaVotoRedCarpet}} @endif</span>
                                    </span>
                                                                    <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-decoration-none text-dark"
                                                                       href="{{asset('serietv/'. $stvCorso->getIdSerieTv())}}">
                                                                        {{$stvCorso->getTitolo()}}
                                                                    </a>
                                                                    <h5 class="h5 mb--5 fs--17 text-gray-600"> {{$stvCorso->getStagioni()[0]->getNumero()}}
                                                                        ° stagione </h5>
                                                                    <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                                        {{$stvCorso->getDescrizione()}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="card-footer bg-transparent b-0 pt--4">
                                                            <hr class="mt--0 mb--15 bg-primary">
                                                            <p class="text-center mb--4 fs--20 text-dark">
                                                                {{date_format(date_create($stvCorso->getDataUscita()), 'd/m/Y') }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div> <!-- QUA FINISCE LO SWIPER-WRAPPER -->
                                    <!-- FRECCIE -->
                                    <div class="swiper-button-next swiper-button-redcarper-dx"></div>
                                    <!-- BOTTONE DESTRO -->
                                    <div class="swiper-button-prev swiper-button-redcarper-sx"></div>
                                    <!-- BOTTONE SINISTRO -->
                                </div>
                            </div>

                        </div>
                    </section>

                    <!-- PARALLAX -->
                    <div class="pt--100 pb--100">
                        <section
                                class="position-relative jarallax overlay-dark overlay-opacity-7 text-white p-0 bg-cover shadow-xs"
                                style="background-image:url({{asset('assets/img/home/stretta_di_mano_2.jpg')}})">
                            <div class="container pt--250 pb--250 text-center position-relative z-index-2">
                                @if(!auth())
                                    <h1 class="mb-5 font-raleway">
                                        Iscriviti e Recensisci
                                    </h1>
                                @else
                                    <h1 class="mb-5 font-raleway">
                                        Continua a Recensire
                                    </h1>
                                @endif
                                <h2 class="lead mb-5 font-raleway">
                                    i tuoi Film e Serie TV preferite
                                </h2>
                                @if(!auth())
                                    <a href="#"
                                       data-href="{{asset('components/accedi_modal')}}"
                                       data-ajax-modal-size="modal-lg"
                                       data-ajax-modal-centered="false"
                                       data-ajax-modal-callback-function=""
                                       data-ajax-modal-backdrop=""
                                       class="js-ajax-modal text-truncate font-weight-light btn transition-hover-top mb-3 d-block-xs bg-primary text-white fs--20">
                                        Accedi
                                    </a>
                                @endif
                            </div>
                            <!-- lines, looks like through a glass -->
                            <div class="absolute-full w-100 overflow-hidden opacity-5">
                                <img class="img-fluid" width="2000" height="2000"
                                     src="{{asset('assets/images/masks/shape-line-lense.svg')}}" alt="...">
                            </div>
                        </section>
                    </div>

                    <!-- CAROSELLO 3 FILM PIU' VOTATI-->
                    <section class="p-3"  data-aos="zoom-in-up" data-aos-once="false">
                        <div class="container">
                            <div class="mt--25">
                                <div class="row">
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                    <div class="column col-8">
                                        <h1 class="fs&#45;&#45;30 text-center text-primary"> FILM PIU' VOTATI</h1>
                                    </div>
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                </div>
                                <div class="swiper-container p--30" data-swiper='{
		"slidesPerView": 5,
		"spaceBetween": 0,
		"slidesPerGroup": 5,
		"loop": false,
		"autoplay": false
	}'>

                                    <div class="swiper-wrapper hide">
                                        <!-- SLIDE 1 -->
                                        @foreach($filmPiuVotati as $key=>$fl)
                                            <div class="swiper-slide">
                                                <div class="col-12">
                                                    <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                                        <div class="clearfix p--15 pl--10 pr--10">
                                                            <img width="172" height="254.547"
                                                                 src="{{ asset($fl->getImgCopertina()->getDato()) }}"
                                                                 alt="...">
                                                        </div>
                                                        <div class="card-body font-weight-light pt--0 pb--15 pl--15 pr--15">
                                                            <div class="d-table">
                                                                <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($fl->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">@if($fl->mediaVotoRedCarpet == 0)
                                                N.D. @else {{$fl->mediaVotoRedCarpet}} @endif</span>
                                    </span>
                                                                    <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-dark text-decoration-none"
                                                                       href="{{asset('film/' . $fl->getIdFilm())}}">
                                                                        {{$fl->getTitolo()}}
                                                                    </a>
                                                                    <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                                        {{$fl->getDescrizione()}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="card-footer bg-transparent b-0 pt--4 pl--10 pr--10">
                                                            <hr class="mt--0 mb--15 bg-primary">
                                                            <p class="text-center mb--4 fs--20 text-dark">
                                                                {{date_format(date_create($fl->getDataUscita()), 'd/m/Y') }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div> <!-- QUA FINISCE LO SWIPER-WRAPPER -->
                                    <!-- FRECCIE -->
                                    <div class="swiper-button-next swiper-button-redcarper-dx"></div>
                                    <!-- BOTTONE DESTRO -->
                                    <div class="swiper-button-prev swiper-button-redcarper-sx"></div>
                                    <!-- BOTTONE SINISTRO -->
                                </div>
                            </div>

                        </div>
                    </section>

                    <!-- SPAM PAGINE SOCIAL -->
                    <section class="" data-aos="zoom-in" data-aos-once="false">
                        <div class="shadow-xs bg-white">
                            <div class="container">
                                <div class="row max-h-250">
                                    <div class="col-12 col-md-3 d-flex justify-content-center pl--25">
                                        <img width="190" height="190" class="lazy"
                                             src="{{asset('assets/img/home/freccia_vettoriale1.jpg')}}" alt="...">
                                    </div>
                                    <div class="col-12 col-md-6 d-flex align-items-center pl--0">
                                        <h3 class="font-weight-light mr-auto font-raleway">
                                            Aiutaci a crescere mettendo il <span class="text-primary font-weight-bold">Mi Piace</span>
                                            alle nostre pagine Social
                                        </h3>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex align-items-center justify-content-center pl--10">
                                        <a href="https://www.facebook.com/"
                                           class="btn btn-facebook transition-hover-top mb-2 mr--10 rounded-circle"
                                           rel="noopener" aria-label="facebook page">
                                            <i class="fi fi-social-facebook"></i>
                                        </a>

                                        <a href="https://twitter.com/"
                                           class="btn btn-twitter transition-hover-top mb-2 mr--10 rounded-circle"
                                           rel="noopener" aria-label="twitter page">
                                            <i class="fi fi-social-twitter"></i>
                                        </a>

                                        <a href="https://www.youtube.com/"
                                           class="btn btn-youtube transition-hover-top mb-2 rounded-circle"
                                           rel="noopener" aria-label="youtube page">
                                            <i class="fi fi-social-youtube"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /SPAM PAGINE SOCIAL -->

                    <!-- CAROSELLO 4 SERIE TV PIU' VOTATE-->
                    <section class="p-3" data-aos="zoom-in-up" data-aos-once="false">
                        <div class="container mb--100">
                            <div class="mt--25">
                                <div class="row">
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                    <div class="column col-8">
                                        <h1 class="fs&#45;&#45;30 text-center text-primary"> SERIE TV PIU' VOTATE</h1>
                                    </div>
                                    <div class="column col-2">
                                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                                    </div>
                                </div>
                                <div class="swiper-container p--30" data-swiper='{
		"slidesPerView": 5,
		"spaceBetween": 0,
		"slidesPerGroup": 5,
		"loop": false,
		"autoplay": false
	}'>

                                    <div class="swiper-wrapper hide">
                                        <!-- SLIDE 1 -->
                                        @foreach($serieTVPiuVate as $keys=>$stv)
                                            <div class="swiper-slide">
                                                <div class="col-12">
                                                    <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                                        <div class="clearfix p--15 pl--10 pr--10">
                                                            <img width="172" height="254.547"
                                                                 src="{{ asset($stv->getImgCopertina()->getDato()) }}"
                                                                 alt="...">
                                                        </div>
                                                        <div class="card-body font-weight-light pt--0 pb--15 pl--15 pr--15">
                                                            <div class="d-table">
                                                                <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($stv->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">@if($stv->mediaVotoRedCarpet == 0)
                                                N.D. @else {{$stv->mediaVotoRedCarpet}} @endif</span>
                                    </span>
                                                                    <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-dark text-decoration-none"
                                                                       href="{{asset('serietv/'. $stv->getIdSerieTv())}}">
                                                                        {{$stv->getTitolo()}}
                                                                    </a>
                                                                    <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                                        {{$stv->getDescrizione()}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="card-footer bg-transparent b-0 pt--4 pl--10 pr--10">
                                                            <hr class="mt--0 mb--15 bg-primary">
                                                            <p class="text-center mb--4 fs--20 text-dark">
                                                                {{date_format(date_create($stv->getDataUscita()), 'd/m/Y') }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div> <!-- QUA FINISCE LO SWIPER-WRAPPER -->
                                    <!-- FRECCIE -->
                                    <div class="swiper-button-next swiper-button-redcarper-dx"></div>
                                    <!-- BOTTONE DESTRO -->
                                    <div class="swiper-button-prev swiper-button-redcarper-sx"></div>
                                    <!-- BOTTONE SINISTRO -->
                                </div>
                            </div>

                        </div>
                    </section>

                    <!-- /TEMPORARY SECTION -->
@endsection

