@if(count($listFoto[0]) > 0)
    @foreach($listFoto as $row)
<div class="row pl--15 pr--10 mb--15">
    @foreach($row as $img)
    <div class="col-12 col-md-3 pl--0 pr--5">
        <div class="card h-100">
            <div class="card-body">
                <a href="#"
                   class="d-block clearfix shadow-xs text-decoration-none position-relative bg-white transition-all-ease-250 rounded overflow-hidden show-hover-container">
                    <a class="fancybox fancybox-secondary" data-fancybox="gallery"
                       href="{{asset($img->getDato())}}">
                        <img width="299" class="img-fluid" src="{{asset($img->getDato())}}" alt="{{$img->getNome()}}" />
                    </a>
                </a>
                <p class="card-text text-center pt--15">
                    {{$img->getDescrizione()}}
                </p>
            </div>
        </div>
    </div>
    @endforeach
</div>
    @endforeach
@else
    <div class="h--300 d-flex align-items-center">
        <h1 class="font-weight-100 text-justify text-center col">Questo Filmmaker non ha foto</h1>
    </div>
@endif
