@if(count($listFilm) > 0 || count($listSerietv) > 0)
    <section class="pt-0 pb-0">
        <div class="container">
        @if(count($listFilm) > 0)
            @foreach($listFilm as $kf=>$film)
                <!-- Film -->
                    <div class="col border-bottom pb-3 @if($kf < 1)pt--20 pl--0 pr--0 @else pt--30 @endif">
                        <div class="row">
                            <div class=" col mt--10 media">

                                <!-- avatar : as background -->
                                <div class="w--100 text-center">
                                    <img class="w--70 h--100" src="{{asset($film->getImgCopertina()->getDato())}}"
                                         alt="{{$film->getImgCopertina()->getNome()}}">
                                </div>

                                <div class="media-body">
                                    <h5>{{$film->getTitolo()}}</h5>
                                    <div class="d-flex align-items-center pb-2">
                                        @if($film->valutazione > 0.0 && $film->valutazione <= 0.5)
                                            <i class="rating-0_5 text-warning"></i>
                                        @elseif($film->valutazione > 0.5 && $film->valutazione <= 1.0)
                                            <i class="rating-1_0 text-warning"></i>
                                        @elseif($film->valutazione > 1.0 && $film->valutazione <= 1.5)
                                            <i class="rating-1_5 text-warning"></i>
                                        @elseif($film->valutazione > 1.5 && $film->valutazione <= 2.0)
                                            <i class="rating-2_0 text-warning"></i>
                                        @elseif($film->valutazione > 2.0 && $film->valutazione <= 2.5)
                                            <i class="rating-2_5 text-warning"></i>
                                        @elseif($film->valutazione > 2.5 && $film->valutazione <= 3.0)
                                            <i class="rating-3_0 text-warning"></i>
                                        @elseif($film->valutazione > 3.0 && $film->valutazione <= 3.5)
                                            <i class="rating-3_5 text-warning"></i>
                                        @elseif($film->valutazione > 3.5 && $film->valutazione <= 4.0)
                                            <i class="rating-4_0 text-warning"></i>
                                        @elseif($film->valutazione > 4.0 && $film->valutazione <= 4.5)
                                            <i class="rating-4_5 text-warning"></i>
                                        @elseif($film->valutazione > 4.5 && $film->valutazione <= 5.0)
                                            <i class="rating-5_0 text-warning"></i>
                                        @else
                                            <i class="rating-0_0 text-warning"></i>
                                        @endif

                                        <span class="fs--11 pt--2">(SMARTOMETRO {{$film->valutazione}})</span>
                                    </div>

                                    <p class="fs--16 mb-0">
                                        Un film di @foreach($film->regista as $regista)<a
                                                href="{{asset('filmmaker/'.$regista->getFilmMaker()->getIdFilmMaker())}}"
                                                class="text-decoration-none">{{$regista->getFilmMaker()->getNome()}} {{$regista->getFilmMaker()->getCognome()}}</a>@endforeach
                                        . Con @foreach($film->cast as $kc=>$cast)<a
                                                href="{{asset('filmmaker/'.$cast->getFilmMaker()->getIdFilmMaker())}}"
                                                class="text-decoration-none">{{$cast->getFilmMaker()->getNome()}} {{$cast->getFilmMaker()->getCognome()}}</a>
                                        @if(count($film->cast)-1 > $kc),@else.@endif @endforeach
                                        <strong>Uscito {{italian_date_str($film->getDataUscita())}}</strong>.
                                        Distribuzione
                                        @foreach($film->getDistributori() as $kd => $distributore)<a href="#"
                                                                                                     class="text-decoration-none">{{$distributore->getNome()}}</a>@endforeach
                                        .
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="fs--16 pl--15 pr--15 mb-2 mt-2">
                                    {{$film->getTramaBreve()}}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 d-flex d-flex justify-content-end mt-2">
                                <a href="{{asset('film/'.$film->getIdFilm())}}" class="text-decoration-none">
                                    Scheda Film >>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /Film -->
            @endforeach
            @foreach($listSerietv as $ks=>$serieTv)
                <!-- SerieTv -->
                    <div class="col border-bottom pb-3 pt--30">
                        <div class="row">
                            <div class=" col mt--10 media">

                                <!-- avatar : as background -->
                                <div class="w--100 text-center">
                                    <img class="w--70 h--100" src="{{asset($serieTv->getImgCopertina()->getDato())}}"
                                         alt="{{$serieTv->getImgCopertina()->getNome()}}">
                                </div>

                                <div class="media-body">
                                    <h5>{{$serieTv->getTitolo()}}</h5>
                                    <div class="d-flex align-items-center pb-2">
                                        @if($serieTv->valutazione > 0.0 && $serieTv->valutazione <= 0.5)
                                            <i class="rating-0_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 0.5 && $serieTv->valutazione <= 1.0)
                                            <i class="rating-1_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 1.0 && $serieTv->valutazione <= 1.5)
                                            <i class="rating-1_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 1.5 && $serieTv->valutazione <= 2.0)
                                            <i class="rating-2_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 2.0 && $serieTv->valutazione <= 2.5)
                                            <i class="rating-2_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 2.5 && $serieTv->valutazione <= 3.0)
                                            <i class="rating-3_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 3.0 && $serieTv->valutazione <= 3.5)
                                            <i class="rating-3_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 3.5 && $serieTv->valutazione <= 4.0)
                                            <i class="rating-4_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 4.0 && $serieTv->valutazione <= 4.5)
                                            <i class="rating-4_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 4.5 && $serieTv->valutazione <= 5.0)
                                            <i class="rating-5_0 text-warning"></i>
                                        @else
                                            <i class="rating-0_0 text-warning"></i>
                                        @endif

                                        <span class="fs--11 pt--2">(SMARTOMETRO {{$serieTv->valutazione}})</span>
                                    </div>

                                    <p class="fs--16 mb-0">
                                        Un film di @foreach($serieTv->regista as $regista)<a
                                                href="{{asset('filmmaker/'.$regista->getFilmMaker()->getIdFilmMaker())}}"
                                                class="text-decoration-none">{{$regista->getFilmMaker()->getNome()}} {{$regista->getFilmMaker()->getCognome()}}</a>@endforeach
                                        . Con @foreach($serieTv->cast as $kc=>$cast)<a
                                                href="{{asset('filmmaker/'.$cast->getFilmMaker()->getIdFilmMaker())}}"
                                                class="text-decoration-none">{{$cast->getFilmMaker()->getNome()}} {{$cast->getFilmMaker()->getCognome()}}</a>
                                        @if(count($serieTv->cast)-1 > $kc),@else.@endif @endforeach
                                        <strong>Uscito {{italian_date_str($film->getDataUscita())}}</strong>.
                                        Distribuzione
                                        @foreach($serieTv->getDistributori() as $kd => $distributore)<a href="#"
                                                                                                        class="text-decoration-none">{{$distributore->getNome()}}</a>@endforeach
                                        .
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="fs--16 pl--15 pr--15 mb-2 mt-2">
                                    {{$serieTv->getTramaBreve()}}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 d-flex d-flex justify-content-end mt-2">
                                <a href="{{asset('serietv/'.$serieTv->getIdSerieTv())}}" class="text-decoration-none">
                                    Scheda SerieTv >>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /SerieTv -->
            @endforeach
        @else
            @foreach($listSerietv as $ks=>$serieTv)
                <!-- Film -->
                    <div class="col border-bottom pb-3 @if($ks < 1)pt--20 pl--0 pr--0 @else pt--30 @endif">
                        <div class="row">
                            <div class=" col mt--10 media">

                                <!-- avatar : as background -->
                                <div class="w--100 text-center">
                                    <img class="w--70 h--100" src="{{asset($serieTv->getImgCopertina()->getDato())}}"
                                         alt="{{$serieTv->getImgCopertina()->getNome()}}">
                                </div>

                                <div class="media-body">
                                    <h5>{{$serieTv->getTitolo()}}</h5>
                                    <div class="d-flex align-items-center pb-2">
                                        @if($serieTv->valutazione > 0.0 && $serieTv->valutazione <= 0.5)
                                            <i class="rating-0_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 0.5 && $serieTv->valutazione <= 1.0)
                                            <i class="rating-1_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 1.0 && $serieTv->valutazione <= 1.5)
                                            <i class="rating-1_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 1.5 && $serieTv->valutazione <= 2.0)
                                            <i class="rating-2_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 2.0 && $serieTv->valutazione <= 2.5)
                                            <i class="rating-2_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 2.5 && $serieTv->valutazione <= 3.0)
                                            <i class="rating-3_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 3.0 && $serieTv->valutazione <= 3.5)
                                            <i class="rating-3_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 3.5 && $serieTv->valutazione <= 4.0)
                                            <i class="rating-4_0 text-warning"></i>
                                        @elseif($serieTv->valutazione > 4.0 && $serieTv->valutazione <= 4.5)
                                            <i class="rating-4_5 text-warning"></i>
                                        @elseif($serieTv->valutazione > 4.5 && $serieTv->valutazione <= 5.0)
                                            <i class="rating-5_0 text-warning"></i>
                                        @else
                                            <i class="rating-0_0 text-warning"></i>
                                        @endif

                                        <span class="fs--11 pt--2">(SMARTOMETRO {{$serieTv->valutazione}})</span>
                                    </div>

                                    <p class="fs--16 mb-0">
                                        Un film di @foreach($serieTv->regista as $regista)<a
                                                href="{{asset('filmmaker/'.$regista->getFilmMaker()->getIdFilmMaker())}}"
                                                class="text-decoration-none">{{$regista->getFilmMaker()->getNome()}} {{$regista->getFilmMaker()->getCognome()}}</a>@endforeach
                                        . Con @foreach($serieTv->cast as $kc=>$cast)<a
                                                href="{{asset('filmmaker/'.$cast->getFilmMaker()->getIdFilmMaker())}}"
                                                class="text-decoration-none">{{$cast->getFilmMaker()->getNome()}} {{$cast->getFilmMaker()->getCognome()}}</a>
                                        @if(count($serieTv->cast)-1 > $kc),@else.@endif @endforeach
                                        <strong>Uscito {{italian_date_str($film->getDataUscita())}}</strong>.
                                        Distribuzione
                                        @foreach($serieTv->getDistributori() as $kd => $distributore)<a href="#"
                                                                                                        class="text-decoration-none">{{$distributore->getNome()}}</a>@endforeach
                                        .
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="fs--16 pl--15 pr--15 mb-2 mt-2">
                                    {{$serieTv->getTramaBreve()}}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 d-flex d-flex justify-content-end mt-2">
                                <a href="{{asset('serietv/'.$serieTv->getIdSerieTv())}}" class="text-decoration-none">
                                    Scheda SerieTv >>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /Film -->
            @endforeach
        @endif
        </div>
    </section>
@else
    <div class="h--300 d-flex align-items-center">
        <h1 class="font-weight-100 text-justify text-center col">Questo Filmmaker non ha una filmografia</h1>
    </div>
@endif