@extends('skeletons.front.app')

@section('content')
    <!-- PAGE TITLE -->
    <section class="bg-light pt--50 pb--40">
        <div class="container pt--5 pb--8">

            <h1 class="h3 text-gray-700 text-uppercase font-weight-light">
                Impostazioni Account
            </h1>

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb fs--14">
                    <li class="breadcrumb-item "><a href="{{asset('')}}">Home</a></li>
                    <li class="breadcrumb-item  active" aria-current="page">Impostazioni Account</li>
                </ol>
            </nav>

        </div>
    </section>
    <!-- /PAGE TITLE -->

    <section class="bg-theme-color-light pt--20 pb--60">
        <div class="container p--0">
            <div class="border border-primary-soft  bg-white rounded">
                <h1 class="text-center font-raleway font-weight-light text-gray-800 p--15">Aggiorna i tuoi dati</h1>
                <hr class="border-primary-soft mb--25">
                <div class="font-raleway border-bottom border-top border-primary-soft p--15 shadow-primary-xs mb--25">
                    <i class="fi fi-users"></i> INFORMAZIONI PERSONALI
                </div>
                @if(session()->flash()->has('errors'))
                    @php
                        $errors = session()->flash()->get('errors');
                        $hasErrors = true;
                    @endphp
                @endif
                @if(session()->flash()->has('updateResult'))
                    @php
                        session()->flash()->get('updateResult');
                    @endphp
                    <div class="hide toast-on-load"
                         data-toast-type="success"
                         data-toast-title="Informazione"
                         data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>Account Aggionanto Con Successo !!!</span></div></div>"
                         data-toast-pos="top-center"
                         {{--     data-toast-delay="4000"--}}
                         data-toast-fill="true"
                         data-
                    ></div>
                @endif
                @if(session()->flash()->has('DeleteStatus'))
                    @php
                        $deleteStatus = session()->flash()->get('DeleteStatus');
                    @endphp
                    @if($deleteStatus === 'fail')
                    <div class="hide toast-on-load"
                         data-toast-type="danger"
                         data-toast-title="Attenzione!!!"
                         data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>Non sono riuscito a cancellare l&apos;immagine :( , prego riprovare l&apos;operazione</span></div></div>"
                         data-toast-pos="top-center"
                         {{--     data-toast-delay="4000"--}}
                         data-toast-fill="true"
                         data-
                    ></div>
                    @else
                        <div class="hide toast-on-load"
                             data-toast-type="success"
                             data-toast-title="Informazione"
                             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>Account Aggionanto Con Successo !!!</span></div></div>"
                             data-toast-pos="top-center"
                             {{--     data-toast-delay="4000"--}}
                             data-toast-fill="true"
                             data-
                        ></div>
                    @endif
                @endif





                @if($user->getTipologiaUtenza() == 0)
                    <form class="pt--15" enctype="multipart/form-data" method="POST" action="{{asset('impostazioni_account/user_normal')}}">
                @elseif($user->getTipologiaUtenza() == 1)
                    <form class="pt--15" enctype="multipart/form-data" method="POST" action="{{asset('impostazioni_account/user_critica')}}">
                @elseif($user->getTipologiaUtenza() == 2)
                    <form class="pt--15" enctype="multipart/form-data" method="POST" action="{{asset('impostazioni_account/user_admin')}}">
                @endif
                        @csrf
                                        @isset(session('user')['img'])
                                            <div class="d-flex justify-content-center pt--15 pb--15">

                                                <div>
                                                    <!-- PREADDED : CIRCLE -->
                                                    <label class="rounded-circle text-center position-relative d-inline-block cursor-pointer border border-dashed w--200 h--200 bg-primary-soft border-primary shadow-primary-xs">



                                    <span class="z-index-2 js-file-input-avatar-ajax-circle-container-preadded d-block absolute-full z-index-1">
													<span data-id="0"
                                                          data-file-name="pi.jpg"
                                                          style="background-image:url({{asset(session('user')['img'])}})"
                                                          class="js-file-input-item d-inline-block position-relative overflow-hidden text-center rounded-circle m-0 p-0 animate-bouncein bg-cover w-100 h-100">
													</span>
												</span>

                                                        <!-- NOTE: data-file-preview-img-height="118 and <label> has .h--12 (120px). This is because we have a border - so we cut 2px (1px for each side) -->
                                                        <input name="avatar_file"
                                                               type="file"

                                                               data-file-ext="jpg, png, gif"
                                                               data-file-max-size-kb-per-file="11500"
                                                               data-file-ext-err-msg="Allowed:"
                                                               data-file-size-err-item-msg="File too large!"
                                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                                               data-file-toast-position="bottom-center"
                                                               data-file-preview-container=".js-file-input-avatar-ajax-circle-container-preadded"
                                                               data-file-preview-show-info="false"
                                                               data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                                               data-file-preview-img-height="198"
                                                               data-file-preview-img-cover="true"

                                                               class="custom-file-input absolute-full">

                                                        <svg class="rounded-circle m-4 z-index-0 fill-gray-300 m--50 fill-primary"
                                                             viewBox="0 0 60 60">
                                                            <path d="M41.014,45.389l-9.553-4.776C30.56,40.162,30,39.256,30,38.248v-3.381c0.229-0.28,0.47-0.599,0.719-0.951c1.239-1.75,2.232-3.698,2.954-5.799C35.084,27.47,36,26.075,36,24.5v-4c0-0.963-0.36-1.896-1-2.625v-5.319c0.056-0.55,0.276-3.824-2.092-6.525C30.854,3.688,27.521,2.5,23,2.5s-7.854,1.188-9.908,3.53c-2.368,2.701-2.148,5.976-2.092,6.525v5.319c-0.64,0.729-1,1.662-1,2.625v4c0,1.217,0.553,2.352,1.497,3.109c0.916,3.627,2.833,6.36,3.503,7.237v3.309c0,0.968-0.528,1.856-1.377,2.32l-8.921,4.866C1.801,46.924,0,49.958,0,53.262V57.5h46v-4.043C46,50.018,44.089,46.927,41.014,45.389z"/>
                                                            <path d="M55.467,46.526l-9.723-4.21c-0.23-0.115-0.485-0.396-0.704-0.771l6.525-0.005c0,0,0.377,0.037,0.962,0.037c1.073,0,2.638-0.122,4-0.707c0.817-0.352,1.425-1.047,1.669-1.907c0.246-0.868,0.09-1.787-0.426-2.523c-1.865-2.654-6.218-9.589-6.354-16.623c-0.003-0.121-0.397-12.083-12.21-12.18c-1.187,0.01-2.309,0.156-3.372,0.413c0.792,2.094,0.719,3.968,0.665,4.576v4.733c0.648,0.922,1,2.017,1,3.141v4c0,1.907-1.004,3.672-2.607,4.662c-0.748,2.022-1.738,3.911-2.949,5.621c-0.15,0.213-0.298,0.414-0.443,0.604v2.86c0,0.442,0.236,0.825,0.631,1.022l9.553,4.776c3.587,1.794,5.815,5.399,5.815,9.41V57.5H60v-3.697C60,50.711,58.282,47.933,55.467,46.526z"/>
                                                        </svg>

                                                    </label>
                                                    <h4 class="font-weight-normal fs--17 text-center">Immagine
                                                        profilo</h4>
                                                </div>
                                            </div>

                                            <div class="d-flex justify-content-center pb--15">
                                                {{-- <a href="{{asset('impostazioni_account?action=delete')}}"
                                                   class="btn btn-primary">Cancella
                                                    Immagine profilo</a>--}}
                                                <a	 href="#"
                                                                               data-href="{{asset('api/impostazioni_account')}}"
                                                                               class="js-ajax-confirm btn btn-primary"

                                                                               data-ajax-confirm-mode="ajax"
                                                                               data-ajax-confirm-method="DELETE"

                                                                               data-ajax-confirm-size="modal-md"
                                                                               data-ajax-confirm-centered="false"

                                                                               data-ajax-confirm-title="Per favore conferma"
                                                                               data-ajax-confirm-body="Sei sicuro di voler Cancellare la tua immagine di profilo?"

                                                                               data-ajax-confirm-btn-yes-class="btn-sm btn-danger left-0 ml--15 position-absolute"
                                                                               data-ajax-confirm-btn-yes-text="Conferma"
                                                                               data-ajax-confirm-btn-yes-icon="fi fi-check"

                                                                               data-ajax-confirm-btn-no-class="btn-sm btn-light border"
                                                                               data-ajax-confirm-btn-no-text="Annulla"
                                                                               data-ajax-confirm-btn-no-icon="fi fi-close"

                                                                               data-ajax-confirm-callback-function="reloadImgFun">
                                                    <i class="fi fi-thrash"></i>
                                                    Cancella Immagine profilo
                                                </a>
                                            </div>


                                        @else
                                            <div class="d-flex justify-content-center pt--15 pb--15 ">
                                                <div>
                                                    <!--

                                                                                    INNER : STATIC : CIRCLE
                                                                                    Width and Height are the same (because we use data-file-preview-img-cover="true")

                                                                                -->
                                                    <label class="rounded-circle text-center position-relative d-inline-block cursor-pointer border border-dashed w--200 h--200 bg-primary-soft border-primary shadow-primary-xs">

                                                        <!-- remove button -->
                                                        <a href="#"
                                                           class="js-file-upload-avatar-circle-remove hide position-absolute absolute-top w-100 z-index-3">
                                                             <span class="d-inline-block btn btn-sm btn-pill bg-secondary text-white pt--4 pb--4 pl--10 pr--10 m--1 mt--n15"
                                                                   title="remove avatar" data-tooltip="tooltip">
                                                                 <i class="fi fi-close m-0"></i>
                                                             </span>
                                                        </a>

                                                        <span class="z-index-2 js-file-input-avatar-circle-container d-block absolute-full z-index-1 hide-empty"> <!-- avatar container --></span>

                                                        <!-- hidden input (out of viewport, or safari will ignore it) -->
                                                        <!-- NOTE: data-file-preview-img-height="118 and <label> has .h--12 (120px). This is because we have a border - so we cut 2px (1px for each side) -->
                                                        <input name="avatar_file"
                                                               type="file"

                                                               data-file-ext="jpg, png"
                                                               data-file-max-size-kb-per-file="11500"
                                                               data-file-ext-err-msg="Allowed:"
                                                               data-file-size-err-item-msg="File too large!"
                                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                                               data-file-toast-position="bottom-center"
                                                               data-file-preview-container=".js-file-input-avatar-circle-container"
                                                               data-file-preview-show-info="false"
                                                               data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                                               data-file-preview-img-height="198"
                                                               data-file-btn-clear="a.js-file-upload-avatar-circle-remove"
                                                               data-file-preview-img-cover="true"

                                                               class="custom-file-input absolute-full">

                                                        <svg class="rounded-circle m-4 z-index-0 fill-gray-300 m--50 fill-primary"
                                                             viewBox="0 0 60 60">
                                                            <path d="M41.014,45.389l-9.553-4.776C30.56,40.162,30,39.256,30,38.248v-3.381c0.229-0.28,0.47-0.599,0.719-0.951c1.239-1.75,2.232-3.698,2.954-5.799C35.084,27.47,36,26.075,36,24.5v-4c0-0.963-0.36-1.896-1-2.625v-5.319c0.056-0.55,0.276-3.824-2.092-6.525C30.854,3.688,27.521,2.5,23,2.5s-7.854,1.188-9.908,3.53c-2.368,2.701-2.148,5.976-2.092,6.525v5.319c-0.64,0.729-1,1.662-1,2.625v4c0,1.217,0.553,2.352,1.497,3.109c0.916,3.627,2.833,6.36,3.503,7.237v3.309c0,0.968-0.528,1.856-1.377,2.32l-8.921,4.866C1.801,46.924,0,49.958,0,53.262V57.5h46v-4.043C46,50.018,44.089,46.927,41.014,45.389z"/>
                                                            <path d="M55.467,46.526l-9.723-4.21c-0.23-0.115-0.485-0.396-0.704-0.771l6.525-0.005c0,0,0.377,0.037,0.962,0.037c1.073,0,2.638-0.122,4-0.707c0.817-0.352,1.425-1.047,1.669-1.907c0.246-0.868,0.09-1.787-0.426-2.523c-1.865-2.654-6.218-9.589-6.354-16.623c-0.003-0.121-0.397-12.083-12.21-12.18c-1.187,0.01-2.309,0.156-3.372,0.413c0.792,2.094,0.719,3.968,0.665,4.576v4.733c0.648,0.922,1,2.017,1,3.141v4c0,1.907-1.004,3.672-2.607,4.662c-0.748,2.022-1.738,3.911-2.949,5.621c-0.15,0.213-0.298,0.414-0.443,0.604v2.86c0,0.442,0.236,0.825,0.631,1.022l9.553,4.776c3.587,1.794,5.815,5.399,5.815,9.41V57.5H60v-3.697C60,50.711,58.282,47.933,55.467,46.526z"/>
                                                        </svg>
                                                    </label>
                                                    <h4 class="font-weight-normal fs--17 text-center">Immagine
                                                        profilo</h4>
                                                </div>
                                            </div>
                                        @endisset


                                        <div class="form-group pr--15 pl--15 pt--15">
                                            <label for="name">Nome:</label>
                                            @isset($hasErrors)
                                                @isset($errors['nome'])
                                                    <input type="text" class="form-control invalidato" id="name" name="nome"
                                                           placeholder="Inserisci nome"
                                                           value="">
                                                @else
                                                <input type="text" class="form-control" id="name" name="nome"
                                                       placeholder="Inserisci nome"
                                                       value="{{old('nome')}}">
                                                @endisset
                                            @else
                                            <input type="text" class="form-control" id="name" name="nome"
                                                   placeholder="Inserisci nome"
                                                   value="{{$user->getNome()}}">
                                            @endisset
                                        </div>

                        @isset($errors['nome'])
                            <div class="pr--15 pl--15">
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['nome'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            </div>
                        @endisset

                                        <div class="form-group pr--15 pl--15 mb--20">
                                            <label for="surname">Cognome:</label>
                                            @isset($hasErrors)
                                                @isset($errors['cognome'])
                                                    <input type="text" class="form-control invalidato" id="surname" name="cognome"
                                                           placeholder="Inserisci Cognome" value="">
                                                @else
                                                <input type="text" class="form-control" id="surname" name="cognome"
                                                       placeholder="Inserisci Cognome" value="{{old('cognome')}}">
                                                @endisset
                                            @else
                                            <input type="text" class="form-control" id="surname" name="cognome"
                                                   placeholder="Inserisci Cognome" value="{{$user->getCognome()}}">
                                            @endisset
                                        </div>

                        @isset($errors['cognome'])
                            <div class="pr--15 pl--15">
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['cognome'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            </div>
                        @endisset


                                        <div class="form-group pr--15 pl--15 mb--20">
                                            <label for="birthday">Data di Nascita:</label>
                                            @isset($hasErrors)
                                                @isset($errors['birthday'])
                                                    <input type="date" class="form-control invalidato" id="birthday"
                                                           name="birthday"
                                                           value="">
                                                @else
                                                <input type="date" class="form-control" id="birthday"
                                                       name="birthday"
                                                       value="{{old('birthday')}}">
                                                @endisset
                                            @else
                                            <input type="date" class="form-control" id="birthday"
                                                   name="birthday"
                                                   value="{{$user->getDataNascita()}}">
                                            @endisset
                                        </div>

                        @isset($errors['birthday'])
                            <div class="pr--15 pl--15">
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['birthday'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            </div>
                        @endisset

                                        @if($user->getTipologiaUtenza() === 1)
                                            <div class="form-group pr--15 pl--15 mb--20">
                                                <label for="codice_albo">Codice Albo</label>
                                                @isset($hasErrors)
                                                    @isset($errors['codice_identificativo_albo'])
                                                        <input type="number" class="form-control invalidato" id="codice_albo"
                                                               name="codice_identificativo_albo"
                                                               max="99999999999" value="">
                                                    @else
                                                    <input type="number" class="form-control" id="codice_albo"
                                                           name="codice_identificativo_albo"
                                                           max="99999999999" value="{{old('codice_identificativo_albo')}}">
                                                    @endisset
                                                @else
                                                <input type="number" class="form-control" id="codice_albo"
                                                       name="codice_identificativo_albo"
                                                       max="99999999999" value="{{$user->getCodiceAlbo()}}">
                                                @endisset
                                            </div>
                                        @endif

                        @isset($errors['codice_albo'])
                            <div class="pr--15 pl--15">
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['codice_albo'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            </div>
                        @endisset

                        @if($user->getTipologiaUtenza() === 1)
                            <div class="form-group pr--15 pl--15 mb--20">
                                <label for="azienda">Nome Azienda:</label>
                                @isset($hasErrors)
                                    @isset($errors['azienda'])
                                        <input type="text" class="form-control invalidato" id="azienda"
                                               name="azienda"
                                               max="99999999999" value="">
                                    @else
                                        <input type="text" class="form-control" id="azienda"
                                               name="azienda"
                                               max="99999999999" value="{{old('azienda')}}">
                                    @endisset
                                @else
                                    <input type="text" class="form-control" id="azienda"
                                           name="azienda"
                                           max="99999999999" value="{{$user->getNomeAzienda()}}">
                                @endisset
                            </div>
                        @endif

                        @isset($errors['azienda'])
                            <div class="pr--15 pl--15">
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['azienda'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            </div>
                        @endisset

                                        @if($user->getTipologiaUtenza() != 2)
                                            <div class="form-label-group mb--90 pl--15 pr--15">
                                                @isset($hasErrors)
                                                    @isset($errors['biografia'])
                                                        <textarea placeholder="Textarea" id="biografia" name="biografia" class="form-control invalidato"
                                                                  rows="3"></textarea>
                                                    @else
                                                    <textarea placeholder="Textarea" id="biografia" name="biografia" class="form-control"
                                                              rows="3">{{old('biografia')}}</textarea>
                                                    @endisset
                                                @else
                            <textarea placeholder="Textarea" id="biografia" name="biografia" class="form-control"
                                      rows="3">@if($user->getBiografia() !== null){{$user->getBiografia()}}@endif</textarea>
                                                @endisset
                                                <label for="biografia" class="pl--25">Biografia</label>
                                            </div>
                                        @endif

                        @isset($errors['biografia'])
                            <div class="pr--15 pl--15">
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['biografia'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            </div>
                        @endisset


                                        <div class="font-raleway border-bottom border-top border-primary-soft p--15 shadow-primary-xs mb--40">
                                            <i
                                                    class="fi fi-locked"></i> INFORMAZIONI ACCESSO
                                        </div>
                                        <div class="form-group pr--15 pl--15 mb-4">
                                            <label for="email">Email:</label>
                                            @isset($hasErrors)
                                                @isset($errors['email'])
                                                    <input type="email" class="form-control invalidato" id="email" name="email"
                                                           aria-describedby="emailHelp"
                                                           placeholder="Inserisci Email" value="">
                                                @else
                                                <input type="email" class="form-control" id="email" name="email"
                                                       aria-describedby="emailHelp"
                                                       placeholder="Inserisci Email" value="{{old('email')}}">
                                                @endisset
                                            @else
                                            <input type="email" class="form-control" id="email" name="email"
                                                   aria-describedby="emailHelp"
                                                   placeholder="Inserisci Email" value="{{$user->getEmail()}}">
                                            @endisset
                                            <small id="emailHelp" class="form-text text-muted">Non condivideremo mai la
                                                tua
                                                email con
                                                nessuno
                                                altro.</small>
                                        </div>
                        @isset($errors['email'])
                        <div class="pr--15 pl--15">
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fi fi-close" aria-hidden="true"></span>
                                </button>
                                @foreach($errors['email'] as $ernome)
                                    {{$ernome}}<br>
                                @endforeach
                            </div>
                        </div>
                        @endisset



                        <div class="pr--15 pl--15 mb-3 mt-3">
                            <div class="input-group-over">
                                <div class="mb-2">Password Vecchia:</div>
                                <div class="form-label-group">
                                    @if(isset($errors['oldPassword']) || isset($errors['account_password']) || isset($errors['newPassword2']))
                                        <input id="oldPassword" name="oldPassword" type="password" class="form-control invalidato" placeholder="Inserisci Vecchia Password">
                                    @else
                                        <input id="oldPassword" name="oldPassword" type="password" class="form-control" placeholder="Inserisci Vecchia Password">
                                    @endif
                                    <label for="oldPassword">Inserisci Vecchia Password</label>
                                </div>

                                <!-- `SOW : Form Advanced` plugin used -->
                                <a href="#" class="btn fs--12 btn-password-type-toggle mt-3" data-target="#oldPassword">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                </a>

                            </div>
                        </div>

                        @if(isset($errors['oldPassword']) || isset($errors['account_password']) || isset($errors['newPassword2']))
                            <div class="pr--15 pl--15">
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @isset($errors['oldPassword'])
                                        @foreach($errors['oldPassword'] as $ernome)
                                            {{$ernome}}<br>
                                        @endforeach
                                    @else
                                        @isset($errors['account_password'])
                                            @foreach($errors['account_password'] as $ernome)
                                                {{$ernome}}<br>
                                            @endforeach
                                        @else
                                            @isset($errors['newPassword2'])
                                                @foreach($errors['newPassword2'] as $ernome)
                                                    {{$ernome}}<br>
                                                @endforeach
                                            @endisset
                                        @endisset
                                    @endisset
                                </div>
                            </div>
                        @endif

                        <div class="pr--15 pl--15 mb-3">
                            <div class="mb-2">Nuova Password:</div>
                            <div class="input-group-over">
                                <div class="form-label-group">
                                    @if(isset($errors['oldPassword']) || isset($errors['account_password']) || isset($errors['newPassword2']))
                                        <input id="account_password" name="account_password" type="password" class="form-control invalidato" placeholder="Inserici Nuova Password">
                                    @else
                                        <input id="account_password" name="account_password" type="password" class="form-control" placeholder="Inserici Nuova Password">
                                    @endif
                                    <label for="account_password">Inserici Nuova Password</label>
                                </div>

                                <!-- `SOW : Form Advanced` plugin used -->
                                <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#account_password">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                </a>

                            </div>
                        </div>

                        <div class="pr--15 pl--15 mb-5">
                            <div class="mb-2">Nuova Password:</div>
                            <div class="input-group-over">
                                <div class="form-label-group">
                                    @if(isset($errors['oldPassword']) || isset($errors['account_password']) || isset($errors['newPassword2']))
                                        <input id="newPassword2" name="newPassword2" type="password" class="form-control invalidato" placeholder="Inserisci di nuovo la nuova Password">
                                    @else
                                        <input id="newPassword2" name="newPassword2" type="password" class="form-control" placeholder="Inserisci di nuovo la nuova Password">
                                    @endif
                                    <label for="newPassword2">Inserisci di nuovo la nuova Password</label>
                                </div>

                                <!-- `SOW : Form Advanced` plugin used -->
                                <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#newPassword2">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                </a>

                            </div>
                        </div>

                                        <div class="d-flex d-flex justify-content-between">
                                            <button type="submit" class="btn btn-secondary m--15">Annulla</button>
                                            <button type="submit" class="btn btn-primary m--15">Salva</button>
                                        </div>



                                    </form>

            </div>
        </div>
    </section>
@endsection

@section('script')
    @parent
    @isset($hasErrors)
        <script>
            document.getElementById('{{array_key_last($errors)}}').scrollIntoView();
        </script>
    @endisset
        <script>
            function reloadImgFun() {
                window.location.reload();
            }
        </script>
@endsection