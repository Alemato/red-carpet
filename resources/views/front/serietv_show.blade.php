@extends('skeletons.front.app')

@section('content')
    <div class="jarallax h--500 bg-cover overlay-dark overlay-opacity-5 d-flex align-items-center justify-content-center text-white"
         data-speed="1.4"
         style="background-image:url({{asset($serietv->getVideos()[0]->getDato())}})"
         data-jarallax-video="mp4:{{asset($serietv->getVideos()[0]->getDato())}}">

        <!-- optional line lenses -->
        <div class="position-absolute absolute-full w-100 overflow-hidden">
            <img class="img-fluid" width="2000" height="2000"
                 src="{{asset('assets/images/masks/shape-line-lense.svg')}}" alt="...">
        </div>

        <div class="text-center pt--180 pb--180 ">

            <h1 class="m-0 font-weight-lighter fs--90">
                {{$serietv->getTiloloOriginale()}}
            </h1>

            <p class="lead mt-2">
                @isset($regista)
                    Regia di <a class="a-none-decoration-blold"
                                href="{{asset($regista['url'])}}"><b>{{$regista['nome']}}</b></a>.@endisset Una Serie
                Tv {{date("Y",strtotime($serietv->getDataSviluppoSerieTv()))}} con @foreach($attori as $k=>$filmmaker)
                    <a class="a-none-decoration-blold"
                       href="{{asset('filmmaker/').$filmmaker->getFilmMaker()->getIdFilmMaker()}}"><b>{{$filmmaker->getFilmMaker()->getNome()}} {{$filmmaker->getFilmMaker()->getCognome()}}</b></a>@if(count($attori)-1 > $k)
                        , @endif @endforeach <br>
                <a class="btn btn-sm btn-primary btn-pill mb-1 pt--0 pb--0 pr--8 pl--8 transition-hover-top"
                   href="{{asset('serietv/'.$serietv->getIdSerieTv()."?shwca=true")}}"><b>Cast
                        completo</b>
                </a>
                Titolo originale: <i>{{$serietv->getTiloloOriginale()}}</i> .
                Genere @foreach($serietv->getGeneri() as $kg=>$genere)<a class="a-none-decoration-blold"
                                                                         href="{{asset('lista_serietv').'?genere='.$genere->getIdGenere()}}">{{$genere->getNome()}}</a>@if(count($serietv->getGeneri())-1 > $kg)
                    ,@endif @endforeach -
                @foreach($serietv->getNazionalita() as $kn=>$idNazione){{$idNazione}} @if(count($serietv->getNazionalita())-1 > $kn)
                    ,@endif @endforeach {{date("Y",strtotime($serietv->getDataSviluppoSerieTv()))}}.
                <br> Uscita cinema {{italian_date_str($serietv->getDataUscita())}} distribuito
                da @foreach($serietv->getDistributori() as $kd=>$distributorte)<a class="a-none-decoration-blold"
                                                                                  href="{{asset('lista_serietv').'?distributore='.$distributorte->getIdDistributore()}}">{{$distributorte->getNome()}}</a>@if(count($serietv->getDistributori())-1 > $kd)
                    ,@endif @endforeach.
                @if(count($serietv->getRecensioni()) > 1) - SMARTOMETRO <b
                        class="text-primary font-weight-bold">{{$valutazione}}</b> su <b
                        class="text-primary font-weight-bold">5</b>
                recensioni tra critica, pubblico e dizionari. @endif
            </p>

            <p class="mt-5 mb-0">
                <a href="#localvideo" class="btn btn-lg btn-pill btn-primary transition-hover-top fancybox fs--18">
                    <b>Guarda il trailer</b> !
                </a>
                <video class="hide img-fluid" controls id="localvideo">
                    <source src="{{asset($serietv->getVideos()[0]->getDato())}}" type="video/mp4">
                    Your browser doesn't support HTML5 video tag.
                </video>
            </p>

        </div>

    </div>


    <!-- TEMPORARY SECTION -->
    <section class="pt--20">
        <div class="container p--0">
            <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link @if($sh) active @endif" id="pills-scheda-tab" data-toggle="pill"
                       href="#pills-scheda" role="tab"
                       aria-controls="pills-scheda" aria-selected="{{$sh}}">Scheda</a>
                </li>
                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link " id="pills-stagioni-tab" data-toggle="pill" href="#pills-stagioni" role="tab"
                       aria-controls="pills-poster" aria-selected="false">Stagioni</a>
                </li>
                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link @if($shwca) active @endif " id="pills-cast-tab" data-toggle="pill"
                       href="#pills-cast" role="tab"
                       aria-controls="pills-cast" aria-selected="{{$shwca}}">Cast</a>
                </li>

                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link @if($shwcr) active @endif" id="pills-critica-tab" data-toggle="pill"
                       href="#pills-critica" role="tab"
                       aria-controls="pills-critica" aria-selected="{{$shwcr}}">Critica</a>
                </li>

                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link" id="pills-premi-tab" data-toggle="pill" href="#pills-premi" role="tab"
                       aria-controls="pills-premi" aria-selected="false">Premi</a>
                </li>

                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link" id="pills-trailer-tab" data-toggle="pill" href="#pills-trailer" role="tab"
                       aria-controls="pills-trailer" aria-selected="false">Trailer</a>
                </li>


                <li class="nav-item col p--0">
                    <a class="nav-link" id="pills-foto-tab" data-toggle="pill" href="#pills-foto" role="tab"
                       aria-controls="pills-foto" aria-selected="false">Foto</a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">

                <div class="js-ajax tab-pane fade show @if($sh) active @endif" id="pills-scheda" role="tabpanel"
                     aria-labelledby="pills-scheda-tab"
                     data-ajax-url="{{asset('serietv/'.$serietv->getIdSerieTv().'/scheda')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show  js-ajax" id="pills-stagioni" role="tabpanel"
                     aria-labelledby="pills-poster-tab"
                     data-ajax-url="{{asset('serietv/'.$serietv->getIdSerieTv().'/stagioni')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show @if($shwca) active @endif js-ajax" id="pills-cast" role="tabpanel"
                     aria-labelledby="pills-cast-tab"
                     data-ajax-url="{{asset('serietv/'.$serietv->getIdSerieTv().'/cast')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show @if($shwcr) active @endif js-ajax " id="pills-critica" role="tabpanel"
                     aria-labelledby="pills-critica-tab"
                     data-ajax-url="{{asset('serietv/'.$serietv->getIdSerieTv().'/critica')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show js-ajax" id="pills-premi" role="tabpanel"
                     aria-labelledby="pills-premi-tab"
                     data-ajax-url="{{asset('serietv/'.$serietv->getIdSerieTv().'/premi')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show js-ajax" id="pills-trailer" role="tabpanel"
                     aria-labelledby="pills-trailer-tab"
                     data-ajax-url="{{asset('serietv/'.$serietv->getIdSerieTv().'/trailer')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show js-ajax" id="pills-foto" role="tabpanel" aria-labelledby="pills-foto-tab"
                     data-ajax-url="{{asset('serietv/'.$serietv->getIdSerieTv().'/foto')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

            </div>
        </div>

        @if(count($sliders)>0)
        <!-- CAROSELLO FILM COSIGLIATI-->
        <div class="container bg-c-6f3 pt--18 mt--25 rounded-xl"> <!-- style="color: #fff6f3" -->
            <div class="mt--25">
                <div class="row">
                    <div class="column col-2">
                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                        <!--  style="color:#f3eed9;background-color:#BB3132" -->
                    </div>
                    <div class="column col-8">
                        <h1 class="fs&#45;&#45;30 text-center text-primary"> SERIE TV CONSIGLIATE</h1>
                    </div>
                    <div class="column col-2">
                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                    </div>
                </div>
                <div class="swiper-container p--30" data-swiper='{
		"slidesPerView": 4,
		"spaceBetween": 0,
		"slidesPerGroup": 4,
		"loop": false,
		"autoplay": false
	}'>

                    <div class="swiper-wrapper hide">
                        @foreach($sliders as $k=>$slider)
                            @if($slider instanceof \App\Model\Film)
                                <div class="swiper-slide">
                                    <div class="col-12">
                                        <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                            <div class="clearfix p--15">
                                                <img width="217.5" height="321.891"
                                                     src="{{ asset($slider->getImgCopertina()->getDato()) }}" alt="...">
                                            </div>
                                            <div class="card-body font-weight-light pt--0 pb--15">
                                                <div class="d-table">
                                                    <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($slider->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">{{$slider->mediaVotoRedCarpet}}</span>
                                    </span>
                                                        <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-decoration-none text-dark"
                                                           href="{{asset('film/' . $slider->getIdFilm())}}">
                                                            {{$slider->getTitolo()}}
                                                        </a>
                                                        <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                            {{$slider->getDescrizione()}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-footer bg-transparent b-0 pt--4">
                                                <hr class="mt--0 mb--15 bg-primary">
                                                <p class="text-center mb--4 fs--20 text-dark">
                                                    {{date_format(date_create($slider->getDataUscita()), 'd/m/Y') }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @if($slider instanceof \App\Model\SerieTv)
                                    <div class="swiper-slide">
                                        <div class="col-12">
                                            <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                                <div class="clearfix p--15">
                                                    <img width="217.5" height="321.891"
                                                         src="{{ asset($slider->getImgCopertina()->getDato()) }}"
                                                         alt="...">
                                                </div>
                                                <div class="card-body font-weight-light pt--0 pb--15">
                                                    <div class="d-table">
                                                        <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($slider->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">{{$slider->mediaVotoRedCarpet}}</span>
                                    </span>
                                                            <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-decoration-none text-dark"
                                                               href="{{asset('serietv/' . $slider->getIdSerieTv())}}">
                                                                {{$slider->getTitolo()}}
                                                            </a>
                                                            <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                                {{$slider->getDescrizione()}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card-footer bg-transparent b-0 pt--4">
                                                    <hr class="mt--0 mb--15 bg-primary">
                                                    <p class="text-center mb--4 fs--20 text-dark">
                                                        {{date_format(date_create($slider->getDataSviluppoSerieTv()), 'd/m/Y') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    </div> <!-- QUA FINISCE LO SWIPER-WRAPPER -->
                    <!-- FRECCIE -->
                    <div class="swiper-button-next swiper-button-redcarper-dx"></div>   <!-- BOTTONE DESTRO -->
                    <div class="swiper-button-prev swiper-button-redcarper-sx"></div>   <!-- BOTTONE SINISTRO -->
                </div>
            </div>

        </div>
        @endif
    </section>
    <!-- /TEMPORARY SECTION -->
@endsection