@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!--
            PAGE TITLE
        -->
        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Dashboard
            </h1>

        </div>

        <!-- WIDGETS -->
        <div class="row gutters-sm">

            <!-- ALERT : UPGRADE -->
            <div class="col-12 mb-3">
                <div class="bg-white shadow-xs fs--16 p-2 rounded">

                    <div class="clearfix bg-light p-2 rounded ">
                        <div class="row col-border text-center text-muted">
                            <div class="col-6 col-md-4 mt-3 mb-3 font-raleway">
                                <div class="h1">
								<span data-toggle="count"
                                      data-count-from="0"
                                      data-count-to="{{$filmSerieTvTot}}"
                                      data-count-duration="3000"
                                      data-count-decimals="0">0</span><span class="font-weight-light">+</span>
                                </div>
                                <h3 class="h6 m-0">FILM E SERIE TV Publicate</h3>
                            </div>
                            <div class="col-6 col-md-4 mt-3 mb-3 font-raleway">
                                <div class="h1">
								<span data-toggle="count"
                                      data-count-from="0"
                                      data-count-to="{{$totRec}}"
                                      data-count-duration="2800"
                                      data-count-decimals="0">0</span><span class="font-weight-light">+</span>
                                </div>
                                <h3 class="h6 m-0">RECENSIONI publicate</h3>
                            </div>
                            <div class="col-6 col-md-4 mt-3 mb-3 b--0-xs font-raleway">
                                <div class="h1">
								<span data-toggle="count"
                                      data-count-from="0"
                                      data-count-to="{{$totUtenti}}"
                                      data-count-duration="2300"
                                      data-count-decimals="0">0</span><span class="font-weight-light">+</span>
                                </div>
                                <h3 class="h6 m-0">UTENTI REGISTRATI</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /ALERT : UPGDARE -->

            <!-- WIDGET : ultimi FILM -->
            <div class="col-12 col-xl-6 mb-3">

                <div class="portlet">

                    <div class="portlet-header">

                        <div class="float-end dropdown">

                            <a href="{{asset('admin/film')}}" class="btn btn-sm btn-light px-2 py-1 fs--15 mt--n3">
                                Visualizza Tutti
                            </a>

                        </div>


                        <span class="d-block text-muted text-truncate font-weight-medium">
										Ultimi Film publicati
									</span>

                    </div>


                    <!-- content loaded via ajax! -->
                    <div class="portlet-body max-h-500 scrollable-vertical scrollable-styled-dark">

                        @foreach($lastFilm as $film)
                        <!-- item -->
                        <div class="d-flex align-items-center p-3 border-bottom border-light">

                            <div class="flex-none w--40">
                                <img width="40" height="40" class="img-fluid lazy"
                                     src="{{asset($film->getImgCopertina()->getDato())}}"
                                     alt="{{$film->getImgCopertina()->getNome()}}">
                            </div>
                            <div class="flex-fill text-truncate px-3">
                                <span class="text-muted">{{$film->getTitolo()}} ({{date("Y", strtotime($film->getDataUscita()))}})</span>
                                <span class="fs--13 d-block text-gray-500">durata: {{$film->getDurata()}} min, @foreach($film->getDistributori() as $distributore) {{$distributore->getNome()}} @endforeach</span>
                            </div>

                            <div class="w--180 fs--14 text-gray-500 font-weight-light text-align-end">
                                <a href="{{asset('admin/modify_film/').$film->getIdFilm()}}">Visualizza</a>
                            </div>


                        </div>
                        <!-- /item -->
                        @endforeach
                    </div>

                    <div class="d-flex align-self-baseline w-100 py-2">
                        <a href="{{asset('admin/film')}}" class="btn btn-sm text-gray-500 b-0 fs--16 shadow-none font-weight-light">
                            <i class="fi fi-arrow-end-slim"></i>
                            <span>Visualizza Tutti</span>
                        </a>
                    </div>

                </div>

            </div>
            <!-- /WIDGET : ultimi FILM -->

            <!-- WIDGET : ultime SerieTv -->
            <div class="col-12 col-xl-6 mb-3">

                <div class="portlet">

                    <div class="portlet-header">

                        <div class="float-end dropdown">

                            <a href="{{asset('admin/serietv')}}" class="btn btn-sm btn-light px-2 py-1 fs--15 mt--n3">
                                Visualizza Tutte
                            </a>

                        </div>


                        <span class="d-block text-muted text-truncate font-weight-medium">
										Ultime Serie Tv publicate
									</span>

                    </div>


                    <!-- content loaded via ajax! -->
                    <div class="portlet-body max-h-500 scrollable-vertical scrollable-styled-dark">

                    @foreach($lastSerieTv as $serieTv)
                        <!-- item -->
                            <div class="d-flex align-items-center p-3 border-bottom border-light">

                                <div class="flex-none w--40">
                                    <img width="40" height="40" class="img-fluid lazy"
                                         src="{{asset($serieTv->getImgCopertina()->getDato())}}"
                                         alt="{{$serieTv->getImgCopertina()->getNome()}}">
                                </div>
                                <div class="flex-fill text-truncate px-3">
                                    <span class="text-muted">{{$serieTv->getTitolo()}} ({{date("Y", strtotime($serieTv->getDataUscita()))}})</span>
                                    <span class="fs--13 d-block text-gray-500">durata media: {{$serieTv->getStagioni()[0]->getEpisodi()[0]->getDurata()}} min, @foreach($serieTv->getDistributori() as $distributore) {{$distributore->getNome()}} @endforeach</span>
                                </div>

                                <div class="w--180 fs--14 text-gray-500 font-weight-light text-align-end">
                                    <a href="{{asset('admin/modify_serietv/').$serieTv->getIdSerieTv()}}">Visualizza</a>
                                </div>


                            </div>
                            <!-- /item -->
                    @endforeach

                    </div>

                    <div class="d-flex align-self-baseline w-100 py-2">
                        <a href="{{asset('admin/serietv')}}" class="btn btn-sm text-gray-500 b-0 fs--16 shadow-none font-weight-light">
                            <i class="fi fi-arrow-end-slim"></i>
                            <span>Visualizza Tutte</span>
                        </a>
                    </div>

                </div>

            </div>
            <!-- /WIDGET : ultime SerieTv -->


            <!-- ALERT : UPGRADE -->
            <div class="col-12 mb-3">
                <div class="bg-white shadow-xs fs--16 p-2 rounded">

                    <div class="clearfix bg-light p-2 rounded h--50">
                    </div>

                </div>
            </div>
            <!-- /ALERT : UPGDARE -->

            <!-- WIDGET : Recensioni -->
            <div class="col-12 col-xl-6 mb-3">

                <div class="portlet">

                    <div class="portlet-header">

                        <div class="float-end dropdown">

                            <!-- reload ajax content -->
                            <a href="#" id="summaryTicketListReloadBtn" aria-label="Reload Content"
                               class="btn btn-sm btn-light px-2 py-1 fs--15 mt--n3">
											<span class="group-icon">
												<i class="fi fi-circle-spin"></i>
												<i class="fi fi-circle-spin fi-spin"></i>
											</span>
                            </a>

                            <!-- dropdown -->
                            <button type="button"
                                    class="dropdown-toggle btn btn-sm btn-soft btn-primary px-2 py-1 fs--15 mt--n3"
                                    id="dropdownRecent" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                <i class="fi fi-dots-vertical m-0"></i>
                            </button>

                            <!--

                                Note: .dropdown-menu-click-update
                                    is the key class to set the link active on click
                                    and close dropdown (if .dropdown-click-ignore is not set)

                            -->
                            <div class="prefix-link-icon prefix-icon-dot dropdown-menu dropdown-menu-invert dropdown-menu-click-update end-0 mt-2"
                                 aria-labelledby="dropdownRecent">

                                <div class="dropdown-header">
                                    Filtra le recensioni
                                </div>

                                <!--

                                    Note: these are working ajax links!
                                    html_frontend/documentation/plugins-sow-ajax-navigation.html

                                    Both plugins are working together:
                                        SOW : Ajax Navigation
                                        SOW : Ajax Content

                                    data-href used here, instead of href to hide
                                    the link from "open in new tab" actions

                                -->
                                <a class="dropdown-item js-ajax"
                                   href="#"
                                   data-href="{{asset('admin/recent_reviews_normali')}}"
                                   data-ajax-target-container="#summaryTicketList"
                                   data-ajax-update-url="false"
                                   data-ajax-show-loading-icon="true"
                                   data-ajax-autoscroll-to-content="false">
                                    Utenti normali
                                </a>

                                <a class="dropdown-item js-ajax"
                                   href="#"
                                   data-href="{{asset('admin/recent_reviews_critici')}}"
                                   data-ajax-target-container="#summaryTicketList"
                                   data-ajax-update-url="false"
                                   data-ajax-show-loading-icon="true"
                                   data-ajax-autoscroll-to-content="false">
                                    Critici
                                </a>
                            </div>
                            <!-- /dropdown -->

                        </div>

                        <span class="d-block text-muted text-truncate font-weight-medium">
										Ultime recensioni
									</span>

                    </div>


                    <!-- content loaded via ajax! -->
                    <div id="summaryTicketList"
                         data-ajax-url="{{asset('admin/recent_reviews')}}"
                         data-ajax-btn-reload="#summaryTicketListReloadBtn"
                         class="js-ajax portlet-body max-h-500 scrollable-vertical scrollable-styled-dark">

                        <!-- ajax content pushed here -->
                        <div class="text-center p-5"><!-- loader indicator -->
                            <i class="fi fi-circle-spin fi-spin fs--30 text-gray-300"></i>
                        </div>

                    </div>

                    <div class="d-flex align-self-baseline w-100 py-2">
                        <a href="{{asset('admin/review_grouped_by_opere')}}" class="btn btn-sm text-gray-500 b-0 fs--16 shadow-none font-weight-light">
                            <i class="fi fi-arrow-end-slim"></i>
                            <span>Visualizza Tutti</span>
                        </a>
                    </div>

                </div>

            </div>
            <!-- /WIDGET : Recensioni -->


            <!-- WIDGET : utenti -->
            <div class="col-12 col-xl-6 mb-3">

                <div class="portlet">

                    <div class="portlet-header">

                        <div class="float-end dropdown">

                            <!-- reload ajax content -->
                            <a href="#" id="summaryNotificationListReloadBtn" aria-label="Reload Content"
                               class="btn btn-sm btn-light px-2 py-1 fs--15 mt--n3">
											<span class="group-icon">
												<i class="fi fi-circle-spin"></i>
												<i class="fi fi-circle-spin fi-spin"></i>
											</span>
                            </a>

                            <!-- dropdown -->
                            <button type="button"
                                    class="dropdown-toggle btn btn-sm btn-soft btn-primary px-2 py-1 fs--15 mt--n3"
                                    id="dropdownLog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="js-trigger-text">Tipologia</span>
                                <i class="fi fi-arrow-down"></i>
                            </button>

                            <!--

                                Note: .dropdown-menu-click-update
                                    is the key class to set the link active on click
                                    and close dropdown (if .dropdown-click-ignore is not set)

                            -->
                            <div class="prefix-link-icon prefix-icon-dot dropdown-menu dropdown-menu-click-update dropdown-menu-invert end-0 mt-2"
                                 aria-labelledby="dropdownLog">

                                <div class="dropdown-header">
                                    Filtra per tipologia
                                </div>

                                <!--

                                    Note: these are working ajax links!
                                    html_frontend/documentation/plugins-sow-ajax-navigation.html

                                    Both plugins are working together:
                                        SOW : Ajax Navigation
                                        SOW : Ajax Content

                                    data-href used here, instead of href to hide
                                    the link from "open in new tab" actions

                                    NOTE: .js-ajax-text-update
                                        Used by SOW : Dropdown to update the text
                                        on button trigger! The button must have the same class!

                                -->
                                <a class="dropdown-item js-ajax"
                                   href="#"
                                   data-href="{{asset('admin/recent_users_normali')}}"
                                   data-ajax-target-container="#summaryNotificationList"
                                   data-ajax-update-url="false"
                                   data-ajax-show-loading-icon="true"
                                   data-ajax-autoscroll-to-content="false">
                                    <span class="js-trigger-text">Utente Normale</span>
                                </a>

                                <a class="dropdown-item js-ajax"
                                   href="#"
                                   data-href="{{asset('admin/recent_users_critici')}}"
                                   data-ajax-target-container="#summaryNotificationList"
                                   data-ajax-update-url="false"
                                   data-ajax-show-loading-icon="true"
                                   data-ajax-autoscroll-to-content="false">
                                    <span class="js-trigger-text">Critico</span>
                                </a>

                                <a class="dropdown-item js-ajax"
                                   href="#"
                                   data-href="{{asset('admin/recent_users_admin')}}"
                                   data-ajax-target-container="#summaryNotificationList"
                                   data-ajax-update-url="false"
                                   data-ajax-show-loading-icon="true"
                                   data-ajax-autoscroll-to-content="false">
                                    <span class="js-trigger-text">Amministratore</span>
                                </a>
                                <a class="dropdown-item js-ajax"
                                   href="#"
                                   data-href="{{asset('admin/recent_users')}}"
                                   data-ajax-target-container="#summaryNotificationList"
                                   data-ajax-update-url="false"
                                   data-ajax-show-loading-icon="true"
                                   data-ajax-autoscroll-to-content="false">
                                    <span class="js-trigger-text">Tutti</span>
                                </a>
                            </div>
                            <!-- /dropdown -->

                        </div>


                        <span class="d-block text-muted text-truncate font-weight-medium">
										Ultimi Utenti registrati
									</span>

                    </div>


                    <!-- content loaded via ajax! -->
                    <div id="summaryNotificationList"
                         data-ajax-url="{{asset('admin/recent_users')}}"
                         data-ajax-btn-reload="#summaryNotificationListReloadBtn"
                         class="js-ajax portlet-body max-h-500 scrollable-vertical scrollable-styled-dark">

                        <!-- ajax content pushed here -->
                        <div class="text-center p-5"><!-- loader indicator -->
                            <i class="fi fi-circle-spin fi-spin fs--30 text-gray-300"></i>
                        </div>

                    </div>


                    <div class="d-flex align-self-baseline w-100 py-2">
                        <a href="{{asset('admin/user_list')}}" class="btn btn-sm text-gray-500 b-0 fs--16 shadow-none font-weight-light">
                            <i class="fi fi-arrow-end-slim"></i>
                            <span>Visualizza tutti</span>
                        </a>
                    </div>

                </div>

            </div>
            <!-- /WIDGET : utenti -->
        </div>
        <!-- /WIDGETS -->
    </div>
@endsection