@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!--
            PAGE TITLE
        -->
        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Inserisci un nuovo Filmmakers
            </h1>

        </div>

        <form class="" action="{{asset('admin/create_filmmaker')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <!-- Primary -->
            <section class="rounded ">

                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse1"
                   role="button" aria-expanded="true" aria-controls="collapse1">
                    <!-- section header -->
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Scheda Filmmakers
                                <small class="fs--11 text-muted d-block mt-1">Dati anagrafici, Copertina e
                                    Avatar</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <!-- /section header -->

                <div class="collapse mb-3 show" id="collapse1">
                    <div class="row">
                        <div class="col-12 col-md-auto">

                            <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed h--565 w--382">

                                <!-- remove button -->
                                <a href="#"
                                   class="js-file-input-showcase-remove hide position-absolute absolute-top text-align-start w-100 z-index-3">
		                            <span class="d-inline-block btn btn-sm bg-secondary text-white pt--4 pb--4 pl--10 pr--10 m--1"
                                          title="remove image" data-tooltip="tooltip">
			<i class="fi fi-close m-0"></i>
		</span>
                                </a>

                                <span class="z-index-2 js-file-input-showcase-container d-block absolute-full z-index-1 hide-empty"><!-- image container --></span>

                                <input name="immagine_profilo"
                                       type="file"
                                       data-file-ext="jpg, jpeg, png"
                                       data-file-max-size-kb-per-file="10240"
                                       data-file-ext-err-msg="Allowed:"
                                       data-file-size-err-item-msg="File too large!"
                                       data-file-size-err-total-msg="Total allowed size exceeded!"
                                       data-file-toast-position="top-center"
                                       data-file-preview-container=".js-file-input-showcase-container"
                                       data-file-preview-show-info="false"
                                       data-file-preview-class="m-0 p-0 rounded"
                                       data-file-preview-img-height="auto"
                                       data-file-btn-clear="a.js-file-input-showcase-remove"
                                       data-file-preview-img-cover="true"
                                       class="custom-file-input absolute-full">

                                <div class="absolute-full">
                                    <div class="d-table">
                                        <div class="d-table-cell align-middle text-center">

                                            <i class="fi fi-image fs--50 text-muted"></i>
                                            <small class="d-block text-muted">
                                                <b>IMMAGINE PROFILO</b>
                                                <span class="d-block mt-1">
						Preferibilmente carica una 450x563 px.
					</span>
                                            </small>

                                        </div>
                                    </div>
                                </div>

                                <!-- ratio maintained using a `blank` image -->
                                <img class="w-100"
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="...">

                            </label>

                        </div>
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Nome" id="nome"
                                               type="text"
                                               value=""
                                               class="form-control"
                                               name="nome">
                                        <label for="Nome">Nome</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md pl-md-2">
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Cognome" id="cognome" type="text" value=""
                                               class="form-control"
                                               name="cognome">
                                        <label for="cognome">Cognome</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label for="biografia" class="">Biografia:</label>
                                    <textarea id="biografia" class="summernote-editor w-100"
                                              data-placeholder="Biograzia dell'attore o filmmakers"
                                              name="biografia"
                                              data-min-height="415"
                                              data-max-height="415"
                                              data-lang="it-IT"
                                              data-disable-resize-rditor="true"
                                              data-ajax-url="_ajax/demo.summernote.php"
                                              data-ajax-params="['action','upload']['param2','value2']"

                                              data-toolbar='[
			["style", ["style"]],
			["font", ["bold", "italic", "underline", "clear"]],
			["color", ["color"]],
			["para", ["ul", "ol", "paragraph"]],
			["insert", ["link", "hr"]],
			["view", ["fullscreen", "codeview"]],
			["help", ["help"]]
		]'
                                    ></textarea>
                                </div>
                            </div>
                            <div class="row">

                            </div>

                        </div>
                    </div>
                    <div class="row mt--20">
                        <div class="col-12 col-md mb-3">
                            <div class="position-relative">
                                <label for="descrizione_breve">Breve descrizione dell'attore o filmmakers:</label>
                                <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		500 caratteri massimi
	</span>

                                <textarea id="descrizione_breve"
                                          class="js-form-advanced-char-count-down form-control"
                                          data-output-target=".js-form-advanced-char-left2"
                                          name="descrizione_breve"
                                          maxlength="500"></textarea>

                                <div class="fs--12 text-muted text-align-end mt--3">
                                    Caratteri rimanenti: <span
                                            class="js-form-advanced-char-left2">500</span>
                                </div>
                            </div>
                            <div class="row  pr--15 mt-3">
                                <div class="position-relative col-12 col-md mb-3 mt-2">
                                    <label for="frase_campione">Citazione da film o intervista:</label>
                                    <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		500 caratteri massimi
	</span>

                                    <textarea id="frase_campione"
                                              class="js-form-advanced-char-count-down form-control"
                                              data-output-target=".js-form-advanced-char-left2"
                                              name="frase_campione"
                                              maxlength="500"></textarea>

                                    <div class="fs--12 text-muted text-align-end mt--3">
                                        Caratteri rimanenti: <span
                                                class="js-form-advanced-char-left2">500</span>
                                    </div>
                                </div>
                                <div class="col-12 col-md-3 mt-2">
                                    <div class="mb--8">Tipologia Filmmakers:</div>
                                    <div class="form-label-group  mb-3 ">

                                        <select id="tipologia_filmmakers" class="form-control bs-select"
                                                name="tipologia_filmmakers[]" multiple>
                                            <option value="0" selected>Altro</option>
                                            <option value="1">Attore</option>
                                            <option value="2">Regista</option>
                                            <option value="3">Distributore</option>
                                            <option value="4">Produttore</option>
                                            <option value="5">Sceneggiatore</option>
                                            <option value="6">Fotografia</option>
                                            <option value="7">Montaggio</option>
                                            <option value="8">Musica</option>
                                            <option value="9">Scenografo</option>
                                            <option value="10">Costumi</option>
                                            <option value="11">Effetti</option>
                                            <option value="12">Art Director</option>
                                            <option value="13">Trucco</option>
                                        </select>
                                        <label for="tipologia_filmmakers" class="pl--2"> Tipologia
                                            Filmmakers:</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-auto mb-3">
                            <div class="position-relative text-center mb-2">Avatar:</div>
                            <label class="w--150 h--250  rounded text-center position-relative d-inline-block cursor-pointer border border-secondary border-dashed bg-white">
                                <!-- remove button -->
                                <a href="#"
                                   class="js-file-upload-avatar-squared-remove hide position-absolute absolute-top text-align-start w-100 z-index-3">
															<span class="d-inline-block btn btn-sm bg-secondary text-white pt--4 pb--4 pl--10 pr--10 m--1"
                                                                  title="remove avatar" data-tooltip="tooltip">
																<i class="fi fi-close m-0"></i>
															</span>
                                </a>

                                <span class="z-index-2 js-file-input-avatar-squared-container d-block absolute-full z-index-1 hide-empty"><!-- avatar container --></span>

                                <!-- hidden input (out of viewport, or safari will ignore it) -->
                                <!-- NOTE: data-file-preview-img-height="118 and <label> has .h--12 (120px). This is because we have a border - so we cut 2px (1px for each side) -->
                                <input name="name_avatar_static_squared"
                                       type="file"

                                       data-file-ext="jpg, png, gif"
                                       data-file-max-size-kb-per-file="11500"
                                       data-file-ext-err-msg="Allowed:"
                                       data-file-size-err-item-msg="File too large!"
                                       data-file-size-err-total-msg="Total allowed size exceeded!"
                                       data-file-toast-position="bottom-center"
                                       data-file-preview-container=".js-file-input-avatar-squared-container"
                                       data-file-preview-show-info="false"
                                       data-file-preview-class="m-0 p-0 rounded animate-bouncein w--147"
                                       data-file-preview-img-height="247"
                                       data-file-btn-clear="a.js-file-upload-avatar-squared-remove"
                                       data-file-preview-img-cover="true"

                                       class="custom-file-input absolute-full">

                                <svg class="fill-gray-600 m--25 z-index-0" viewBox="0 0 60 60">
                                    <path d="M41.014,45.389l-9.553-4.776C30.56,40.162,30,39.256,30,38.248v-3.381c0.229-0.28,0.47-0.599,0.719-0.951c1.239-1.75,2.232-3.698,2.954-5.799C35.084,27.47,36,26.075,36,24.5v-4c0-0.963-0.36-1.896-1-2.625v-5.319c0.056-0.55,0.276-3.824-2.092-6.525C30.854,3.688,27.521,2.5,23,2.5s-7.854,1.188-9.908,3.53c-2.368,2.701-2.148,5.976-2.092,6.525v5.319c-0.64,0.729-1,1.662-1,2.625v4c0,1.217,0.553,2.352,1.497,3.109c0.916,3.627,2.833,6.36,3.503,7.237v3.309c0,0.968-0.528,1.856-1.377,2.32l-8.921,4.866C1.801,46.924,0,49.958,0,53.262V57.5h46v-4.043C46,50.018,44.089,46.927,41.014,45.389z"/>
                                    <path d="M55.467,46.526l-9.723-4.21c-0.23-0.115-0.485-0.396-0.704-0.771l6.525-0.005c0,0,0.377,0.037,0.962,0.037c1.073,0,2.638-0.122,4-0.707c0.817-0.352,1.425-1.047,1.669-1.907c0.246-0.868,0.09-1.787-0.426-2.523c-1.865-2.654-6.218-9.589-6.354-16.623c-0.003-0.121-0.397-12.083-12.21-12.18c-1.187,0.01-2.309,0.156-3.372,0.413c0.792,2.094,0.719,3.968,0.665,4.576v4.733c0.648,0.922,1,2.017,1,3.141v4c0,1.907-1.004,3.672-2.607,4.662c-0.748,2.022-1.738,3.911-2.949,5.621c-0.15,0.213-0.298,0.414-0.443,0.604v2.86c0,0.442,0.236,0.825,0.631,1.022l9.553,4.776c3.587,1.794,5.815,5.399,5.815,9.41V57.5H60v-3.697C60,50.711,58.282,47.933,55.467,46.526z"/>
                                </svg>
                                Immagine Avatar<br>da usare <br>per il cast

                            </label>
                        </div>
                    </div>
                </div>

            </section>
            <!-- /Primary -->

            <!-- MEDIA -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse3"
                   role="button" aria-expanded="false" aria-controls="collapse3">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                MEDIA
                                <small class="fs--11 text-muted d-block mt-1">Foto, Poster</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapse3">
                    <!-- /section header -->
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">


                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th class="w--90">MEDIA</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- file -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="media[img][]"
                                               type="file"

                                               data-file-ext="jpg, png, gif, jpeg"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="m-0 p-0 rounded animate-bouncein"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione][]"></textarea>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>
                </div>
            </section>
            <!-- /MEDIA-->


            <button type="submit" class="btn btn-lg btn-success float-end"> Salva <i
                        class="fi fi-check mr--0 ml--15"></i>
            </button>
        </form>

    </div>
    <!-- /MIDDLE -->
@endsection