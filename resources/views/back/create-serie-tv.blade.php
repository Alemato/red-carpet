@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!--
            PAGE TITLE
        -->
        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Inserisci una nuova Serie TV
            </h1>

        </div>

        <form class="" action="{{asset('admin/create_serietv')}}" method="POST" enctype="multipart/form-data">
        @csrf

        <!-- Primary -->
            <section class="rounded ">


                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse1"
                   role="button" aria-expanded="true" aria-controls="collapse1">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Scheda Serire Tv
                                <small class="fs--11 text-muted d-block mt-1">Titolo, Trama e Copertina</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <!-- /section header -->

                <div class="collapse mb-3 show" id="collapse1">
                    <div class="row">
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <input id="titolo_originale_serie_tv" name="titolo_originale_serie_tv" type="text"
                                               placeholder="Titolo Originale Serie Tv"
                                               value=""
                                               class="form-control"
                                        >
                                        <label for="titolo_originale_serie_tv">Titolo Originale Serie Tv</label>
                                    </div>
                                </div>
                                <div class="d-none d-sm-block fs--35 font-weight-100">
                                    <div>/</div>
                                </div>
                                <div class="col-12 col-md pl-md-2">
                                    <div class="form-label-group mb-3">
                                        <input id="titolo_serie_tv" name="titolo_serie_tv" type="text" placeholder="Titolo Serie Tv"
                                               value=""
                                               class="form-control">
                                        <label for="titolo_serie_tv">Titolo Serie Tv</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label for="trama" class="">Trama:</label>
                                    <textarea id="trama" name="trama" class="summernote-editor w-100"
                                              data-placeholder="Trama Generale della Serie Tv"
                                              data-min-height="285"
                                              data-max-height="285"
                                              data-lang="it-IT"
                                              data-disable-resize-rditor="true"

                                              data-toolbar='[
			["style", ["style"]],
			["font", ["bold", "italic", "underline", "clear"]],
			["para", ["ul", "ol", "paragraph"]],
			["insert", ["link", "hr"]],
			["view", ["fullscreen", "codeview"]],
			["help", ["help"]]
		]'
                                    ></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3" style="">
                                    <div class="position-relative">
                                        <label for="descrizione_serie_Tv">Descrizione Serie TV: </label>
                                        <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		250 caratteri massimi
	</span>

                                        <textarea id="descrizione_serie_Tv"
                                                  name="descrizione_serie_Tv"
                                                  class="js-form-advanced-char-count-down form-control"
                                                  data-output-target=".js-form-advanced-char-left2"
                                                  maxlength="250"></textarea>

                                        <div class="fs--12 text-muted text-align-end mt--3">
                                            Caratteri rimanenti: <span
                                                    class="js-form-advanced-char-left2">250</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-auto">

                            <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed h--565 w--382">

                                <!-- remove button -->
                                <a href="#"
                                   class="js-file-input-showcase-remove hide position-absolute absolute-top text-align-start w-100 z-index-3">
		                            <span class="d-inline-block btn btn-sm bg-secondary text-white pt--4 pb--4 pl--10 pr--10 m--1"
                                          title="remove image" data-tooltip="tooltip">
			<i class="fi fi-close m-0"></i>
		</span>
                                </a>

                                <span class="z-index-2 js-file-input-showcase-container d-block absolute-full z-index-1 hide-empty"><!-- image container --></span>

                                <input name="immagine_copertina"
                                       type="file"
                                       data-file-ext="jpg, jpeg, png"
                                       data-file-max-size-kb-per-file="10240"
                                       data-file-ext-err-msg="Allowed:"
                                       data-file-size-err-item-msg="File too large!"
                                       data-file-size-err-total-msg="Total allowed size exceeded!"
                                       data-file-toast-position="top-center"
                                       data-file-preview-container=".js-file-input-showcase-container"
                                       data-file-preview-show-info="false"
                                       data-file-preview-class="m-0 p-0 rounded"
                                       data-file-preview-img-height="auto"
                                       data-file-btn-clear="a.js-file-input-showcase-remove"
                                       data-file-preview-img-cover="true"
                                       class="custom-file-input absolute-full">

                                <div class="absolute-full">
                                    <div class="d-table">
                                        <div class="d-table-cell align-middle text-center">

                                            <i class="fi fi-image fs--50 text-muted"></i>
                                            <small class="d-block text-muted">
                                                <b>IMMAGINE COPERTINA</b>
                                                <span class="d-block mt-1">
						Perfafore, 380x563 px preferibile.
					</span>
                                            </small>

                                        </div>
                                    </div>
                                </div>

                                <!-- ratio maintained using a `blank` image -->
                                <img class="w-100"
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="...">

                            </label>

                        </div>
                    </div>

                </div>

                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapseDati"
                   role="button" aria-expanded="false" aria-controls="collapseDati">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Dati della Serie Tv
                                <small class="fs--11 text-muted d-block mt-1">Descrizioni, Dati vari </small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>

                <div class="collapse mb-3" id="collapseDati">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="row">
                                <div class="col-12">
                                    <div class="position-relative">
                                        <label for="descrizione_breve">Trama generale in riassunto: </label>
                                        <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		500 caratteri massimi
	</span>

                                        <textarea id="descrizione_breve"
                                                  name="trama_breve"
                                                  class="js-form-advanced-char-count-down form-control"
                                                  data-output-target=".js-form-advanced-char-left2"
                                                  maxlength="500"></textarea>

                                        <div class="fs--12 text-muted text-align-end mt--3">
                                            Caratteri rimanenti: <span
                                                    class="js-form-advanced-char-left2">500</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <input id="data_sviluppo" type="date"
                                       name="data_sviluppo"
                                       value="{{date('Y-m-d')}}"
                                       class="form-control">
                                <label for="data_sviluppo">Data sviluppo Serie Tv</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <input id="data_uscita_italia" type="date"
                                       name="data_uscita_italia"
                                       value="{{date('Y-m-d')}}"
                                       class="form-control">
                                <label for="data_uscita_italia">Data di uscita Serie Tv</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <select id="eta_consigliata"
                                        name="eta_consigliata"
                                        class="form-control">
                                    <option value="0">0+ anni</option>
                                    <option value="5">5+ anni</option>
                                    <option value="13">13+ anni</option>
                                    <option value="15">15+ anni</option>
                                    <option value="18">18+ anni</option>
                                </select>
                                <label for="eta_consigliata">Età consigliata</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-2 pl--15">
                            <div class="form-label-group w-100">
                                <select id="serie_tv_in_corso"
                                        name="serie_tv_in_corso"
                                        class="form-control">
                                    <option value="1">Serie Tv in corso</option>
                                    <option value="0">Serie Tv non in corso</option>
                                </select>
                                <label for="serie_tv_in_corso">Serie Tv in corso</label>
                            </div>
                        </div>
                        <div class="col-12 col-md">
                            <div class="form-label-group mb-3">
                                <select id="nazione" class="form-control bs-select" name="nazione[]" multiple>
                                    @foreach($array_nazioni as $k => $nazione)
                                        <option value="{{$k}}" @if($k === 'IT') selected="selected" @endif >{{$nazione}}</option>
                                    @endforeach
                                </select>
                                <label for="nazione">Nazionalit&agrave;</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <select id="distributore" class="form-control bs-select"
                                                name="distributore[]" multiple>
                                            <option value="0"></option>
                                            @isset($distributori)
                                                @if(count($distributori)>0)
                                                    @foreach($distributori as $distributore)
                                                        <option value="{{$distributore->getIdDistributore()}}">{{$distributore->getNome()}}</option>
                                                    @endforeach
                                                @endif
                                            @endisset
                                        </select>
                                        <label for="distributore">Distributore Serie Tv</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-auto pl-md-2 mb-3">
                                    <a href="#" class="js-ajax-modal btn btn-primary"
                                       data-href="{{asset('admin/modal/create_distributore')}}"
                                       data-ajax-modal-size="modal-md"
                                       data-ajax-modal-centered="false"
                                       data-ajax-modal-callback-function=""
                                       data-ajax-modal-backdrop="">
                                        Aggiungi nuovo
                                        <i class="fi fi-plus ml--5 mr--0"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <select id="genere" class="form-control bs-select" name="genere[]" multiple>
                                            <option value="0"></option>
                                            @isset($generi)
                                                @if(count($generi)>0)
                                                    @foreach($generi as $genere)
                                                        <option value="{{$genere->getIdGenere()}}">{{$genere->getNome()}}</option>
                                                    @endforeach
                                                @endif
                                            @endisset
                                        </select>
                                        <label for="genere">Genere Serie TV</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-auto pl-md-2 mb-3">
                                    <a href="#"
                                       data-href="{{asset('admin/modal/create_genere')}}"
                                       data-ajax-modal-size="modal-lg"
                                       data-ajax-modal-centered="false"
                                       data-ajax-modal-callback-function=""
                                       data-ajax-modal-backdrop=""
                                       class="js-ajax-modal btn btn-primary">
                                        Aggiungi Nuovo
                                        <i class="fi fi-plus ml--5 mr--0"></i>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <!-- /Primary -->

            <!-- CAST -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class=" text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse2"
                   role="button" aria-expanded="false" aria-controls="collapse2">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Cast
                                <small class="fs--11 text-muted d-block mt-1">Cast & Filmmakers</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <!-- /section header -->
                <div class="collapse mb-3" id="collapse2">
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-row-limit="50"
                         data-table-row-method="append">


                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th>Filmmakers</th>
                                <th>Tipologia</th>
                                <th>Ruolo</th>
                                <th class="w--80"></th>
                            </tr>
                            </thead>

                            <tbody>

                            <tr>
                                <!-- SKU -->
                                <td>
                                    <div class="col pr--0 pl--0">
                                        <div class="form-label-group">
                                            <select class="form-control select2-plugin ajax-method"
                                                    name="cast[select_filmmakers][]"
                                                    data-input-min-length="1" data-ajax-method="GET"
                                                    data-ajax-url="{{asset('api/cast')}}"
                                                    data-ajax-cache="true"
                                                    data-input-delay="250">
                                            </select>
                                            <label
                                                    class="label-select2">Filmmakers</label>
                                        </div>
                                    </div>
                                </td>

                                <!-- Barcode -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="cast[tipologia][]">
                                            <option value="0">Altro</option>
                                            <option value="1">Attore</option>
                                            <option value="2">Regista</option>
                                            <option value="4">Produttore</option>
                                            <option value="5">Sceneggiatore</option>
                                            <option value="6">Fotografia</option>
                                            <option value="7">Montaggio</option>
                                            <option value="8">Musica</option>
                                            <option value="9">Scenografo</option>
                                            <option value="10">Costumista</option>
                                            <option value="11">Effetti</option>
                                            <option value="12">Art Director</option>
                                            <option value="13">Trucco</option>
                                        </select>
                                        <label>Tipologia</label>
                                    </div>
                                </td>

                                <!-- Price -->
                                <td>
                                    <div class="form-label-group">
                                        <input placeholder="ruolo" type="text" class="form-control"
                                               name="cast[ruolo][]"
                                               value="">
                                        <label>Ruolo</label>
                                    </div>
                                </td>

                                <!-- Option -->
                                <td class="text-center">

                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
						<span class="group-icon">
							<i class="fi fi-plus"></i>
							<i class="fi fi-thrash"></i>
						</span>
                                    </a>

                                </td>

                            </tr>

                            </tbody>
                        </table>
                        <div class="d-flex justify-content-start">
                            <div class="col-auto pl--0">
                                Se non trovi il filmmaker aggiungilo tramite questo bottone:
                                <a href="#" class="js-ajax-modal btn btn-sm btn-primary ml--15"
                                   data-href="{{asset('admin/modal/create_filmmakers')}}"
                                   data-ajax-modal-size="modal-xl"
                                   data-ajax-modal-centered="false"
                                   data-ajax-modal-callback-function=""
                                   data-ajax-modal-backdrop="">
                                    Aggiungi Filmmaker
                                    <i class="fi fi-plus ml--5 mr--0"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- /CAST-->

            <!-- STAGIONI -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse5"
                   role="button" aria-expanded="false" aria-controls="collapse5">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                STAGIONI
                                <small class="fs--11 text-muted d-block mt-1">stagioni & puntate</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapse5">
                    <!-- /section header -->
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">


                        <!--

                            ADD COLUMN

                                The name of this input (name="option" in this demo)
                                is used to generate input fields with names like this:
                                    name="option[color]"

                                if no input name used, field name is generated like this:
                                    name="color"

                        -->
                        <!--
                        <div class="js-form-advanced-table-column-add input-group mb-3 max-w-300">
-->
                        <!--
                            Optional Plugin used:
                            SOW : Search Inline
                        -->
                        <!--
                            <input type="text" name="variant" class="form-control form-control-sm input-suggest"
                                   placeholder="Ex: color, size"
                                   aria-label="Table Cloner"
                                   aria-describedby="button-table-cloner"
                                   data-input-suggest-type="text"
                                   data-input-suggest-name="variant"
                                   data-input-suggest-ajax-url="_ajax/input_suggest_variants.json"
                                   data-input-suggest-ajax-method="GET"
                                   data-input-suggest-ajax-limit="100">

                            <div class="input-group-append">
                                <button class="btn btn-sm btn-outline-secondary" type="button" id="button-table-cloner">Add</button>
                            </div>
                        </div>  -->


                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th class="w--150">STAGIONE</th>
                                <th class="w--400">TITOLO EPISODIO</th>
                                <th class="w--100">COPERTINA</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--150">DURATA PUNTATA</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Stagione -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="number" class="form-control"
                                               name="stagioni_episodi[numero_stagione][]" min="1">
                                        <label>Numero stagione</label>
                                    </div>
                                </td>
                                <!-- Titolo Episodio -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="text" class="form-control" name="stagioni_episodi[titolo][]">
                                        <label>Titolo Episodio</label>
                                    </div>
                                </td>

                                <!-- COPERTINA -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="stagioni_episodi[copertina][]"
                                               type="file"

                                               data-file-ext="jpg, png, gif, webp"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="m-0 p-0 rounded"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="stagioni_episodi[descrizione][]"></textarea>
                                    </div>
                                </td>

                                <!-- Durata puntata -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="number" class="form-control"
                                               name="stagioni_episodi[dutata_puntata][]" min="1">
                                        <label>Durata Puntata</label>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
{{--
                            <!-- preadded -->
                            <tr>
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Stagione -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="number" class="form-control"
                                               name="stagioni_episodi[numero_stagione]" min="0" value="1">
                                        <label>Numero stagione</label>
                                    </div>
                                </td>


                                <!-- Titolo Episodio -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="text" class="form-control" name="stagioni_episodi[titolo]"
                                               value="Puntata 1">
                                        <label>Titolo Episodio</label>
                                    </div>
                                </td>


                                <!-- Copertina -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty">
							<span data-id="0"
                                  data-file-name="mutzii-fmDCrqPQKog-unsplash-min.jpg"
                                  style="background-image:url('../assets/images/dashelily/puntata-1-dash.webp')"
                                  class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
							</span>
						</span>

                                        <!-- hidden file input -->
                                        <input name="variant[image]"
                                               type="file"

                                               data-file-ext="jpg, png, gif, mp4"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               data-file-ajax-upload-enable="true"
                                               data-file-ajax-upload-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-upload-params="['action','upload']['param2','value2']"

                                               data-file-ajax-delete-enable="true"
                                               data-file-ajax-delete-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-delete-params="['action','delete_file']"

                                               data-file-ajax-toast-success-txt="Successfully Uploaded!"
                                               data-file-ajax-toast-error-txt="One or more files not uploaded!"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione]"> la descrizione puntata 1</textarea>
                                    </div>
                                </td>

                                <!-- Durata puntata -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="number" class="form-control"
                                               name="stagioni_episodi[dutata_puntata]" min="1" value="23">
                                        <label>Durata Puntata</label>
                                    </div>
                                </td>

                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>

                            <!-- preadded -->
                            <tr>
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Stagione -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="number" class="form-control"
                                               name="stagioni_episodi[numero_stagione]" min="0" value="1">
                                        <label>Numero stagione</label>
                                    </div>
                                </td>


                                <!-- Titolo Episodio -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="text" class="form-control" name="stagioni_episodi[titolo]"
                                               value="Puntata 2">
                                        <label>Titolo Episodio</label>
                                    </div>
                                </td>

                                <!-- Copertina -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-2 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-2 z-index-2 d-block absolute-full z-index-1 hide-empty">
							<span data-id="0"
                                  data-file-name="nikita-kachanovsky-ad_0wMHtvlU-unsplash-min.jpg"
                                  style="background-image:url('../assets/images/dashelily/puntata-2-lily.webp')"
                                  class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
							</span>
						</span>

                                        <!-- hidden file input -->
                                        <input name="name_avatar_ajax_circle"
                                               type="file"

                                               data-file-ext="jpg, png, gif, mp4"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-2"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-2"
                                               data-file-preview-img-cover="true"

                                               data-file-ajax-upload-enable="true"
                                               data-file-ajax-upload-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-upload-params="['action','upload']['param2','value2']"

                                               data-file-ajax-delete-enable="true"
                                               data-file-ajax-delete-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-delete-params="['action','delete_file']"

                                               data-file-ajax-toast-success-txt="Successfully Uploaded!"
                                               data-file-ajax-toast-error-txt="One or more files not uploaded!"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>

                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione]"> la descrizione puntata 2</textarea>
                                    </div>
                                </td>

                                <!-- Durata puntata -->
                                <td>
                                    <div class="form-label-group">
                                        <input type="number" class="form-control"
                                               name="stagioni_episodi[dutata_puntata]" min="1" value="23">
                                        <label>Durata Puntata</label>
                                    </div>
                                </td>

                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
--}}
                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>
                </div>
            </section>
            <!-- /STAGIONI-->

            <!-- MEDIA -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class=" text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse3"
                   role="button" aria-expanded="false" aria-controls="collapse3">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                MEDIA
                                <small class="fs--11 text-muted d-block mt-1">Foto, Poster & Trailer</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapse3">
                    <!-- /section header -->
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th>SCHEDA</th>
                                <th class="w--90">MEDIA</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Scheda -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="immagini[scheda][]">
                                            <option value="6">Poster</option>
                                            <option value="5">Foto</option>
                                        </select>
                                        <label>Visualizzabile in</label>
                                    </div>
                                </td>

                                <!-- file -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="immagini[file][]"
                                               type="file"
                                               data-file-ext="jpg, png, gif, mp4"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="m-0 p-0 rounded"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="immagini[descrizione][]"></textarea>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>

                            {{--

                            <!-- preadded -->
                            <tr>
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- scheda -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="media[scheda][]">
                                            <option value="0"></option>
                                            <option value="1">Trailer</option>
                                            <option value="2">Poster</option>
                                            <option value="3" selected>Foto</option>
                                        </select>
                                        <label>Visualizzabile in</label>
                                    </div>
                                </td>
                                <!-- Image -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty">
							<span data-id="0"
                                  data-file-name="mutzii-fmDCrqPQKog-unsplash-min.jpg"
                                  style="background-image:url({{asset('assets/images/mutzii-fmDCrqPQKog-unsplash-min.jpg')}})"
                                  class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
							</span>
						</span>

                                        <!-- hidden file input -->
                                        <input name="media[file][]"
                                               type="file"

                                               data-file-ext="jpg, png, gif, mp4"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               data-file-ajax-upload-enable="true"
                                               data-file-ajax-upload-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-upload-params="['action','upload']['param2','value2']"

                                               data-file-ajax-delete-enable="true"
                                               data-file-ajax-delete-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-delete-params="['action','delete_file']"

                                               data-file-ajax-toast-success-txt="Successfully Uploaded!"
                                               data-file-ajax-toast-error-txt="One or more files not uploaded!"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>

                                <!-- tipologia file -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="media[tipologia_file][]">
                                            <option value="0"></option>
                                            <option value="1" selected>Jpeg</option>
                                            <option value="2">Png</option>
                                            <option value="3">Gif</option>
                                            <option value="4">Mp4</option>
                                        </select>
                                        <label>Tipologia file</label>
                                    </div>
                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione][]"> la descrizione 1</textarea>
                                    </div>
                                </td>

                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
                        --}}

                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>

                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="append">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th>VIDEO</th>
                                <th class="w--80">COVER</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>


                                <!-- video -->
                                <td>
                                    <div class="input-group">
                                        <input type="file" name="media[video][]">

                                    </div>
                                </td>


                                <!-- file -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="media[cover][]"
                                               type="file"
                                               data-file-ext="jpg, png, gif, jpeg"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="m-0 p-0 rounded"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>



                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione][]"></textarea>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>
                </div>
            </section>
            <!-- /MEDIA-->

            <!-- PREMI -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse4"
                   role="button" aria-expanded="false" aria-controls="collapse4">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                PREMI
                                <small class="fs--11 text-muted d-block mt-1">Premi & Nomination</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapse4">
                    <!-- /section header -->
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th>GENERE</th>
                                <th>TIPOLOGIA</th>
                                <th>STATO</th>
                                <th>ASSEGNATO A</th>
                                <th>DESCRIZIONE</th>
                                <th>DATA</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Genere -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[genere][]">
                                            @foreach($premi as $k => $premio)
                                                <option value="{{$premio->getIdPremio()}}" >{{$premio->getNome()}}</option>
                                            @endforeach
                                        </select>
                                        <label>Premio</label>
                                    </div>
                                </td>

                                <!-- Tipologia -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[tipologia][]">
                                            @foreach($tipologiaPremi as $k => $tipologiaPremio)
                                                <option value="{{$tipologiaPremio->getidTipologiaPremio()}}" >{{$tipologiaPremio->getNome()}}</option>
                                            @endforeach
                                        </select>
                                        <label>Premio per</label>
                                    </div>
                                </td>

                                <!-- Stato -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[stato][]">
                                            <option value="0">Nomination</option>
                                            <option value="1">Vittoria</option>
                                        </select>
                                        <label>Stato:</label>
                                    </div>
                                </td>

                                <!-- Assegnato -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control select2-plugin ajax-method"
                                                name="premio[select_filmmakers][]"
                                                data-input-min-length="1" data-ajax-method="GET"
                                                data-ajax-url="{{asset('api/cast')}}"
                                                data-input-delay="250">
                                        </select>
                                        <label
                                                class="label-select2">Assegnato a</label>
                                    </div>
                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="premio[descrizione][]"></textarea>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Data premiazione" type="date" value="{{date('Y-m-d')}}"
                                               class="form-control" name="premio[data_premiazione][]">
                                        <label>Data Premiazione</label>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>

                            {{--

                            <!-- preadded -->
                            <tr>
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Genere -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[genere]">
                                            <option value="0"></option>
                                            <option value="1" selected>Oscar</option>
                                            <option value="2">BAFTA - British Academy of Film and Television
                                            </option>
                                            <option value="3">Critics Choice Award</option>
                                        </select>
                                        <label>Premio</label>
                                    </div>
                                </td>

                                <!-- Tipologia -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[tipologia]">
                                            <option value="0"></option>
                                            <option value="1" selected>Fotografia</option>
                                            <option value="2">Regia</option>
                                            <option value="3">Miglior Interpretazione</option>
                                        </select>
                                        <label>Premio per</label>
                                    </div>
                                </td>

                                <!-- Assegnato -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control select2-plugin ajax-method"
                                                name="cast[select_filmmakers]"
                                                data-input-min-length="1" data-ajax-method="GET"
                                                data-ajax-url="http://localhost:6969/backend/_ajax/cast.json"
                                                data-input-delay="250">
                                            <option value="1">Mario Rossi</option>
                                        </select>
                                        <label
                                                class="label-select2">Assegnato a</label>
                                    </div>
                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="premio[descrizione]">Premio assegnato a Mario Rossi</textarea>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Data premiazione" type="date" value="2020-01-09"
                                               class="form-control" name="premio[data_premiazione]">
                                        <label>Data Premiazione</label>
                                    </div>
                                </td>

                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>

                            <!-- preadded -->
                            <tr>
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Genere -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[genere]">
                                            <option value="0"></option>
                                            <option value="1">Oscar</option>
                                            <option value="2" selected>BAFTA - British Academy of Film and
                                                Television
                                            </option>
                                            <option value="3">Critics Choice Award</option>
                                        </select>
                                        <label>Premio</label>
                                    </div>
                                </td>

                                <!-- Tipologia -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[tipologia]">
                                            <option value="0"></option>
                                            <option value="1">Fotografia</option>
                                            <option value="2" selected>Regia</option>
                                            <option value="3">Miglior Interpretazione</option>
                                        </select>
                                        <label>Premio per</label>
                                    </div>
                                </td>

                                <!-- Assegnato -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control select2-plugin ajax-method"
                                                name="cast[select_filmmakers]"
                                                data-input-min-length="1" data-ajax-method="GET"
                                                data-ajax-url="http://localhost:6969/backend/_ajax/cast.json"
                                                data-input-delay="250">
                                            <option value="1">Marco Bianchi</option>
                                        </select>
                                        <label
                                                class="label-select2">Assegnato a</label>
                                    </div>
                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="premio[descrizione]">Premio assegnato a Marco Bianchi</textarea>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Data premiazione" type="date" value="2020-02-09"
                                               class="form-control" name="premio[data_premiazione]">
                                        <label>Data Premiazione</label>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>

                            --}}

                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>
                </div>
            </section>
            <!-- /PREMI-->

            <button type="submit" class="btn btn-lg btn-success float-end"> Salva <i
                        class="fi fi-check mr--0 ml--15"></i>
            </button>
        </form>

    </div>
    <!-- /MIDDLE -->
@endsection