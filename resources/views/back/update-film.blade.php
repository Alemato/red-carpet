@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!--
            PAGE TITLE
        -->
        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Inserisci un nuovo film
            </h1>

        </div>

        <form class="" action="{{asset('admin/modify_film/'.$filmObj->getIdFilm())}}" method="POST"
              enctype="multipart/form-data">


            @csrf

            <!-- Primary -->
            <section class="rounded ">


                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse1"
                   role="button" aria-expanded="true" aria-controls="collapse1">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Scheda Film
                                <small class="fs--11 text-muted d-block mt-1">Titolo, Trama e Copertina</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <!-- /section header -->

                <div class="collapse mb-3 show" id="collapse1">
                    <div class="row">
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <input id="titolo_originale_film" name="titolo_originale_film" type="text"
                                               placeholder="Titolo Originale"
                                               value="{{$filmObj->getTitoloOriginale()}}"
                                               class="form-control"
                                        >
                                        <label for="titolo_originale_film">Titolo Originale Film</label>
                                    </div>
                                </div>
                                <div class="d-none d-sm-block fs--35 font-weight-100">
                                    <div>/</div>
                                </div>
                                <div class="col-12 col-md pl-md-2">
                                    <div class="form-label-group mb-3">
                                        <input id="titolo_film" name="titolo_film" type="text" placeholder="Titolo"
                                               value="{{$filmObj->getTitolo()}}"
                                               class="form-control">
                                        <label for="titolo_film">Titolo Film</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label for="description" class="">Trama:</label>
                                    <textarea id="description" name="trama" class="summernote-editor w-100"
                                              data-placeholder="Trama"
                                              data-min-height="285"
                                              data-max-height="285"
                                              data-lang="it-IT"
                                              data-disable-resize-rditor="true"
                                              data-ajax-url="_ajax/demo.summernote.php"
                                              data-ajax-params="['action','upload']['param2','value2']"

                                              data-toolbar='[
			["style", ["style"]],
			["font", ["bold", "italic", "underline", "clear"]],
			["para", ["ul", "ol", "paragraph"]],
			["insert", ["link", "hr"]],
			["view", ["fullscreen", "codeview"]],
			["help", ["help"]]
		]'
                                    >{{$filmObj->getTrama()}}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <div class="position-relative">
                                        <label for="descrizione_breve">Trama in riassunto: </label>
                                        <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		500 caratteri massimi
	</span>

                                        <textarea id="descrizione_breve"
                                                  name="trama_breve"
                                                  class="js-form-advanced-char-count-down form-control"
                                                  data-output-target=".js-form-advanced-char-left2"
                                                  maxlength="500">{{$filmObj->getTramaBreve()}}</textarea>

                                        <div class="fs--12 text-muted text-align-end mt--3">
                                            Caratteri rimanenti: <span
                                                    class="js-form-advanced-char-left2">{{500 - strlen($filmObj->getTramaBreve())}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-auto">

                            <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed h--565 w--382">


                                <span class="z-index-2 js-file-input-showcase-container d-block absolute-full z-index-1 hide-empty">
                                    <span data-id="{{$filmObj->getImgCopertina()->getIdImmagione()}}"
                                          data-file-name="{{$filmObj->getImgCopertina()->getNome()}}"
                                          style="background-image:url('{{asset($filmObj->getImgCopertina()->getDato())}}')"
                                          class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
																</span>
                                </span>

                                <input name="immagine_copertina"
                                       type="file"
                                       data-file-ext="jpg, jpeg, png"
                                       data-file-max-size-kb-per-file="10240"
                                       data-file-ext-err-msg="Allowed:"
                                       data-file-size-err-item-msg="File too large!"
                                       data-file-size-err-total-msg="Total allowed size exceeded!"
                                       data-file-toast-position="top-center"
                                       data-file-preview-container=".js-file-input-showcase-container"
                                       data-file-preview-show-info="false"
                                       data-file-preview-class="m-0 p-0 rounded"
                                       data-file-preview-img-height="auto"
                                       data-file-btn-clear="a.js-file-input-showcase-remove"
                                       data-file-preview-img-cover="true"
                                       class="custom-file-input absolute-full">

                                <div class="absolute-full">
                                    <div class="d-table">
                                        <div class="d-table-cell align-middle text-center">

                                            <i class="fi fi-image fs--50 text-muted"></i>
                                            <small class="d-block text-muted">
                                                <b>IMMAGINE COPERTINA</b>
                                                <span class="d-block mt-1">
						Perfafore, 380x563 px preferibile.
					</span>
                                            </small>

                                        </div>
                                    </div>
                                </div>

                                <!-- ratio maintained using a `blank` image -->
                                <img class="w-100"
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="...">

                            </label>

                        </div>

                    </div>

                </div>

                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapseDati"
                   role="button" aria-expanded="false" aria-controls="collapseDati">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Dati del Film
                                <small class="fs--11 text-muted d-block mt-1">Descrizioni, Dati vari </small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapseDati">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="row">
                                <div class="col-12 col-md-8" style="padding-right: 26px;">
                                    <div class="position-relative">
                                        <label for="descrizione_film">Descrizione film: </label>
                                        <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		250 caratteri massimi
	</span>

                                        <textarea id="descrizione_film"
                                                  name="descrizione_film"
                                                  class="js-form-advanced-char-count-down form-control"
                                                  data-output-target=".js-form-advanced-char-left2"
                                                  maxlength="250">{{$filmObj->getDescrizione()}}</textarea>

                                        <div class="fs--12 text-muted text-align-end mt--3">
                                            Caratteri rimanenti: <span
                                                    class="js-form-advanced-char-left2">{{250 - strlen($filmObj->getDescrizione())}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4 pl--5" style="padding-top: 33px;">
                                    <div class="form-label-group w-100">
                                        <select id="film_in_sala"
                                                name="film_in_sala"
                                                class="form-control">
                                            <option value="0" @if($filmObj->getInSala() === 0)
                                                selected="selected"
                                                    @endif >Film non disponibile nei cinema
                                            </option>
                                            <option value="1" @if($filmObj->getInSala() === 1)
                                                selected="selected"
                                                    @endif>Film nei cinema
                                            </option>
                                        </select>
                                        <label for="film_in_sala">Film in sala</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <input id="data_sviluppo" type="date"
                                       name="data_sviluppo"
                                       value="{{$filmObj->getDataSviluppoFilm()}}"
                                       class="form-control">
                                <label for="data_sviluppo">Data sviluppo film</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <input id="data_uscita_italia" type="date"
                                       name="data_uscita_italia"
                                       value="{{$filmObj->getDataUscita()}}"
                                       class="form-control">
                                <label for="data_uscita_italia">Data di uscita</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <select id="eta_consigliata"
                                        name="eta_consigliata"
                                        class="form-control">
                                    <option value="0" @if($filmObj->getAgeRating() === 0)
                                        selected="selected"
                                            @endif>0+ anni
                                    </option>
                                    <option value="5" @if($filmObj->getAgeRating() === 5)
                                        selected="selected"
                                            @endif>5+ anni
                                    </option>
                                    <option value="13" @if($filmObj->getAgeRating() === 13)
                                        selected="selected"
                                            @endif>13+ anni
                                    </option>
                                    <option value="15" @if($filmObj->getAgeRating() === 15)
                                        selected="selected"
                                            @endif>15+ anni
                                    </option>
                                    <option value="18" @if($filmObj->getAgeRating() === 18)
                                        selected="selected"
                                            @endif>18+ anni
                                    </option>
                                </select>
                                <label for="eta_consigliata">Età consigliata</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto">
                            <div class="form-label-group mb-3">
                                <input placeholder="Titolo Originale" id="durata_film" type="number"
                                       name="durata_film"
                                       value="{{$filmObj->getDurata()}}"
                                       class="form-control">
                                <label for="durata_film">Durata Film</label>
                            </div>
                        </div>
                        <div class="col-12 col-md">
                            <div class="form-label-group mb-3">
                                <select id="nazione" class="form-control bs-select" name="nazione[]" multiple>
                                    @foreach($array_nazioni as $k => $nazione)
                                        <option value="{{$k}}"
                                                @foreach($filmObj->getNazionalita() as $k => $nazione_scelta)
                                                    @if($nazione_scelta === $nazione)
                                                        selected="selected"
                                                @endif
                                                @endforeach
                                        >{{$nazione}}</option>
                                    @endforeach
                                </select>
                                <label for="nazione">Nazionalit&agrave;</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <select id="distributore" class="form-control bs-select"
                                                name="distributore[]" multiple>
                                            <option value="0"></option>
                                            @isset($distributori)
                                                @if(count($distributori)>0)
                                                    @foreach($distributori as $distributore)
                                                        <option value="{{$distributore->getIdDistributore()}}"
                                                                @foreach($filmObj->getDistributori() as $k => $distributori_scelti)
                                                                    @if($distributori_scelti->getIdDistributore() === $distributore->getIdDistributore())
                                                                        selected="selected"
                                                                @endif
                                                                @endforeach>{{$distributore->getNome()}}</option>
                                                    @endforeach
                                                @endif
                                            @endisset
                                        </select>
                                        <label for="distributore">Distributore film</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-auto pl-md-2 mb-3">
                                    <a href="#" class="js-ajax-modal btn btn-primary"
                                       data-href="{{asset('admin/modal/create_distributore')}}"
                                       data-ajax-modal-size="modal-md"
                                       data-ajax-modal-centered="false"
                                       data-ajax-modal-callback-function=""
                                       data-ajax-modal-backdrop="">
                                        Aggiungi nuovo
                                        <i class="fi fi-plus ml--5 mr--0"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <select id="genere" class="form-control bs-select" name="genere[]" multiple>
                                            <option value="0"></option>
                                            @isset($generi)
                                                @if(count($generi)>0)
                                                    @foreach($generi as $genere)
                                                        <option value="{{$genere->getIdGenere()}}"
                                                                @foreach($filmObj->getGeneri() as $k => $generi_scelti)
                                                                    @if($generi_scelti->getIdGenere() === $genere->getIdGenere())
                                                                        selected="selected"
                                                                @endif
                                                                @endforeach>{{$genere->getNome()}}</option>
                                                    @endforeach
                                                @endif
                                            @endisset
                                        </select>
                                        <label for="genere">Genere film</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-auto pl-md-2 mb-3">
                                    <a href="#"
                                       data-href="{{asset('admin/modal/create_genere')}}"
                                       data-ajax-modal-size="modal-lg"
                                       data-ajax-modal-centered="false"
                                       data-ajax-modal-callback-function=""
                                       data-ajax-modal-backdrop=""
                                       class="js-ajax-modal btn btn-primary">
                                        Aggiungi Nuovo
                                        <i class="fi fi-plus ml--5 mr--0"></i>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <!-- /Primary -->

            <!-- CAST -->
            <section class="rounded shadow-3d" id="s_cast">
                <!-- section header -->
                <a class=" text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse2"
                   role="button" aria-expanded="false" aria-controls="collapse2">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Cast
                                <small class="fs--11 text-muted d-block mt-1">Cast & Filmmakers</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <!-- /section header -->
                @if($showCast)
                    <div class="collapse mb-3 show" id="collapse2">
                        @else
                            <div class="collapse mb-3" id="collapse2">
                                @endif
                                <!--
     default
         data-table-clone-method="append|prepend"
 -->
                                <div class="js-form-advanced-table mb-6"
                                     data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                                     data-table-row-limit="50"
                                     data-table-row-method="append">


                                    <table class="table table-bordered table-align-middle table-sm">
                                        <thead>
                                        <tr>
                                            <th>Filmmakers</th>
                                            <th>Tipologia</th>
                                            <th>Ruolo</th>
                                            <th class="w--80 text-center">
                                                <a href="#"
                                                   class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                                    <i class="fi fi-plus"></i>
                                                </a>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        <tr class="js-ignore hide">
                                            <!-- SKU -->
                                            <td>
                                                <input type="hidden" name="cast[id_cast][]" value="">
                                                <div class="col pr--0 pl--0">
                                                    <div class="form-label-group">
                                                        <select class="form-control select2-plugin ajax-method"
                                                                name="cast[select_filmmakers][]"
                                                                data-input-min-length="1" data-ajax-method="GET"
                                                                data-ajax-url="{{asset('api/cast')}}"
                                                                data-ajax-cache="true"
                                                                data-input-delay="250">
                                                        </select>
                                                        <label
                                                                class="label-select2">Filmmakers</label>
                                                    </div>
                                                </div>
                                            </td>

                                            <!-- Barcode -->
                                            <td>
                                                <div class="form-label-group">
                                                    <select class="form-control bs-select" name="cast[tipologia][]">
                                                        <option value="0">Altro</option>
                                                        <option value="1">Attore</option>
                                                        <option value="2">Regista</option>
                                                        <option value="4">Produttore</option>
                                                        <option value="5">Sceneggiatore</option>
                                                        <option value="6">Fotografia</option>
                                                        <option value="7">Montaggio</option>
                                                        <option value="8">Musica</option>
                                                        <option value="9">Scenografo</option>
                                                        <option value="3">Costumista</option>
                                                        <option value="11">Effetti</option>
                                                        <option value="12">Art Director</option>
                                                        <option value="13">Trucco</option>
                                                    </select>
                                                    <label>Tipologia</label>
                                                </div>
                                            </td>

                                            <!-- Price -->
                                            <td>
                                                <div class="form-label-group">
                                                    <input placeholder="ruolo" type="text" class="form-control"
                                                           name="cast[ruolo][]"
                                                           value="">
                                                    <label>Ruolo</label>
                                                </div>
                                            </td>

                                            <!-- Option -->
                                            <td class="text-center">

                                                <a href="#"
                                                   class="btn btn-sm btn-primary btn-table-clone rounded-circle">
						<span class="group-icon">
							<i class="fi fi-plus"></i>
							<i class="fi fi-thrash"></i>
						</span>
                                                </a>

                                            </td>

                                        </tr>


                                        @foreach($filmObj->getRuoli() as $k => $ruolo)
                                            <!-- preadded -->
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="cast[id_cast][]"
                                                           value="{{$ruolo->getIdRuolo()}}">
                                                    <div class="col pr--0 pl--0">
                                                        <div class="form-label-group">
                                                            <select class="form-control select2-plugin ajax-method"
                                                                    name="cast[select_filmmakers][]"
                                                                    data-input-min-length="1" data-ajax-method="GET"
                                                                    data-ajax-url="{{asset('api/cast')}}"
                                                                    data-ajax-cache="true"
                                                                    data-input-delay="250">
                                                                <option value="{{$ruolo->getFilmMaker()->getIdFilmMaker()}}"
                                                                        selected="selected">{{$ruolo->getFilmMaker()->getNome()}} {{$ruolo->getFilmMaker()->getCognome()}}</option>
                                                            </select>

                                                            <label
                                                                    class="label-select2">Filmmakers</label>
                                                        </div>
                                                    </div>
                                                </td>

                                                <!-- Barcode -->
                                                <td>
                                                    <div class="form-label-group">
                                                        <select class="form-control bs-select" name="cast[tipologia][]">
                                                            <option value="0" @if($ruolo->getTipologia() === '0')
                                                                selected="selected"
                                                                    @endif>Altro
                                                            </option>
                                                            <option value="1" @if($ruolo->getTipologia() === '1')
                                                                selected="selected"
                                                                    @endif>Attore
                                                            </option>
                                                            <option value="2" @if($ruolo->getTipologia() === '2')
                                                                selected="selected"
                                                                    @endif>Regista
                                                            </option>
                                                            <option value="4" @if($ruolo->getTipologia() === '4')
                                                                selected="selected"
                                                                    @endif>Produttore
                                                            </option>
                                                            <option value="5" @if($ruolo->getTipologia() === '5')
                                                                selected="selected"
                                                                    @endif>Sceneggiatore
                                                            </option>
                                                            <option value="6" @if($ruolo->getTipologia() === '6')
                                                                selected="selected"
                                                                    @endif>Fotografia
                                                            </option>
                                                            <option value="7" @if($ruolo->getTipologia() === '7')
                                                                selected="selected"
                                                                    @endif>Montaggio
                                                            </option>
                                                            <option value="8" @if($ruolo->getTipologia() === '8')
                                                                selected="selected"
                                                                    @endif>Musica
                                                            </option>
                                                            <option value="9" @if($ruolo->getTipologia() === '9')
                                                                selected="selected"
                                                                    @endif>Scenografo
                                                            </option>
                                                            <option value="10" @if($ruolo->getTipologia() === '10')
                                                                selected="selected"
                                                                    @endif>Costumista
                                                            </option>
                                                            <option value="11" @if($ruolo->getTipologia() === '11')
                                                                selected="selected"
                                                                    @endif>Effetti
                                                            </option>
                                                            <option value="12" @if($ruolo->getTipologia() === '12')
                                                                selected="selected"
                                                                    @endif>Art Director
                                                            </option>
                                                            <option value="13" @if($ruolo->getTipologia() === '13')
                                                                selected="selected"
                                                                    @endif>Trucco
                                                            </option>
                                                        </select>
                                                        <label>Tipologia</label>
                                                    </div>
                                                </td>

                                                <!-- Price -->
                                                <td>
                                                    <div class="form-label-group">
                                                        <input placeholder="ruolo" type="text" class="form-control"
                                                               name="cast[ruolo][]"
                                                               value="{{$ruolo->getRuoloInternoOpera()}}">
                                                        <label>Ruolo</label>
                                                    </div>
                                                </td>

                                                <!-- Option -->
                                                <td class="position-relative text-center">

                                                    <!-- direct delete -->
                                                    <!--
                                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                                        <i class="fi fi-thrash"></i>
                                                    </a>
                                                    -->

                                                    <!-- remove button (confirm trigger) -->
                                                    <a href="#"
                                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                                        <i class="fi fi-thrash"></i>
                                                    </a>

                                                    <!-- confirm -->
                                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                                        <a href="#!"
                                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                                        <a href="#!"
                                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                                    </div>

                                                </td>

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-start">
                                        <div class="col-auto pl--0">
                                            Se non trovi il filmmaker aggiungilo tramite questo bottone:
                                            <a href="#" class="js-ajax-modal btn btn-sm btn-primary ml--15"
                                               data-href="{{asset('admin/modal/create_filmmakers')}}"
                                               data-ajax-modal-size="modal-xl"
                                               data-ajax-modal-centered="false"
                                               data-ajax-modal-callback-function=""
                                               data-ajax-modal-backdrop="">
                                                Aggiungi Filmmaker
                                                <i class="fi fi-plus ml--5 mr--0"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
            </section>
            <!-- /CAST-->

            <!-- MEDIA -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class=" text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse3"
                   role="button" aria-expanded="false" aria-controls="collapse3">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                MEDIA
                                <small class="fs--11 text-muted d-block mt-1">Foto, Poster & Trailer</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapse3">
                    <!-- /section header -->
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th>SCHEDA</th>
                                <th class="w--90">MEDIA</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Scheda -->
                                <td>
                                    <input type="hidden" name="immagini[Id_immagini][]" value="">
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="immagini[scheda][]">
                                            <option value="6">Poster</option>
                                            <option value="5">Foto</option>
                                        </select>
                                        <label>Visualizzabile in</label>
                                    </div>
                                </td>

                                <!-- file -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="immagini[file][]"
                                               type="file"
                                               data-file-ext="jpg, png, gif, mp4"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="m-0 p-0 rounded"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="immagini[descrizione][]"></textarea>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>


                            @foreach($filmObj->getImgs() as $k => $immagine)
                                <!-- preadded -->
                                <tr>
                                    <!-- sortable handler -->
                                    <td class="px-0 text-center">
                                        <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                    </td>
                                    <!-- scheda -->
                                    <td>
                                        <input type="hidden" name="immagini[Id_immagini][]"
                                               value="{{$immagine->getIdImmagione()}}">
                                        <div class="form-label-group">
                                            <select class="form-control bs-select" name="immagini[scheda][]">

                                                <option value="6" @if($immagine->getTipo() === 6)
                                                    selected="selected"
                                                        @endif>Poster
                                                </option>
                                                <option value="5" @if($immagine->getTipo() === 5)
                                                    selected="selected"
                                                        @endif>Foto
                                                </option>
                                            </select>
                                            <label>Visualizzabile in</label>
                                        </div>
                                    </td>
                                    <!-- Image -->
                                    <td>

                                        <!--

                                            AJAX IMAGE UPLOAD

                                        -->
                                        <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">


                                            <!-- image container -->
                                            <span class="js-table-file-preview-{{$immagine->getIdImmagione()}} z-index-2 d-block absolute-full z-index-1 hide-empty">
							<span data-id="{{$immagine->getIdImmagione()}}"
                                  data-file-name="{{$immagine->getNome()}}"
                                  style="background-image:url({{asset($immagine->getDato())}})"
                                  class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
							</span>
						</span>

                                            <!-- hidden file input -->
                                            <input name="immagini[file][]"
                                                   type="file"

                                                   data-file-ext="jpg, png, gif"
                                                   data-file-max-size-kb-per-file="50000"
                                                   data-file-ext-err-msg="Allowed:"
                                                   data-file-size-err-item-msg="File too large!"
                                                   data-file-size-err-total-msg="Total allowed size exceeded!"
                                                   data-file-toast-position="bottom-center"
                                                   data-file-preview-container=".js-table-file-preview-{{$immagine->getIdImmagione()}}"
                                                   data-file-preview-show-info="false"
                                                   data-file-preview-class="shadow-md mb-2 rounded"
                                                   data-file-preview-img-height="auto"

                                                   data-file-preview-img-cover="true"
                                                   class="custom-file-input absolute-full">

                                            <!-- icon -->
                                            <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                        </label>

                                    </td>


                                    <!-- Descrizione -->
                                    <td>
                                        <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="immagini[descrizione][]">{{$immagine->getDescrizione()}}</textarea>
                                        </div>
                                    </td>

                                    <!-- Option -->
                                    <td class="position-relative text-center">

                                        <!-- direct delete -->
                                        <!--
                                        <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                            <i class="fi fi-thrash"></i>
                                        </a>
                                        -->

                                        <!-- remove button (confirm trigger) -->
                                        <a href="#"
                                           class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                            <i class="fi fi-thrash"></i>
                                        </a>

                                        <!-- confirm -->
                                        <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                            <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                            <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                            <a href="#!"
                                               class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                            <a href="#!"
                                               class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                        </div>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>

                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th class="w--800">VIDEO</th>
                                <th class="w--80">COVER</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>


                                <!-- video -->
                                <td>
                                    <input type="hidden" name="media[id_video][]" value="">
                                    <div class="input-group">
                                        <input type="file" name="media[video][]">

                                    </div>
                                </td>


                                <!-- file -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="media[cover][]"
                                               type="file"
                                               data-file-ext="jpg, png, gif, jpeg"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="shadow-md mb-2 rounded"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione][]"></textarea>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>

                            @foreach($filmObj->getVideos() as $k => $video)
                                <!-- preadded -->
                                <tr>
                                    <!-- sortable handler -->
                                    <td class="px-0 text-center">
                                        <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                    </td>

                                    <!-- video -->
                                    <td>
                                        <input type="hidden" name="media[id_video][]" value="{{$video->getIdVideo()}}">
                                        <div class="input-group align-items-center">
                                            <div class="d-block pr--15">
                                                <h4 class="mb--5">Video caricato: <b>{{$video->getNome()}}</b></h4>
                                                <h6>se vuoi modificarlo ricarica il video</h6>
                                            </div>
                                            <input type="file" name="media[video][]" class="ml--15">

                                        </div>
                                    </td>

                                    <!-- cover -->
                                    <td>

                                        <!--

                                            AJAX IMAGE UPLOAD

                                        -->
                                        <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                            <!-- image container -->
                                            <span class="js-table-file-preview-{{$video->getImmagine()->getIdImmagione()}} z-index-2 d-block absolute-full z-index-1 hide-empty">
							<span data-id="{{$video->getImmagine()->getIdImmagione()}}"
                                  data-file-name="{{$video->getImmagine()->getNome()}}"
                                  style="background-image:url({{asset($video->getImmagine()->getDato())}})"
                                  class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
							</span>
						</span>
                                            <!-- hidden file input -->
                                            <input name="media[cover][]"
                                                   type="file"
                                                   data-file-ext="jpg, png, gif, jpeg"
                                                   data-file-max-size-kb-per-file="50000"
                                                   data-file-ext-err-msg="Allowed:"
                                                   data-file-size-err-item-msg="File too large!"
                                                   data-file-size-err-total-msg="Total allowed size exceeded!"
                                                   data-file-toast-position="bottom-center"
                                                   data-file-preview-container=".js-table-file-preview-{{$video->getImmagine()->getIdImmagione()}}"
                                                   data-file-preview-show-info="false"
                                                   data-file-preview-class="shadow-md mb-2 rounded"
                                                   data-file-preview-img-height="auto"
                                                   data-file-preview-img-cover="true"

                                                   data-file-btn-clear=".js-table-file-remove-1"

                                                   data-file-ajax-upload-enable="false"
                                                   data-file-ajax-upload-url="../demo.files/php/demo.ajax_file_upload.php"
                                                   data-file-ajax-upload-params="['action','upload']['param2','value2']"

                                                   data-file-ajax-delete-enable="false"
                                                   data-file-ajax-delete-url="../demo.files/php/demo.ajax_file_upload.php"
                                                   data-file-ajax-delete-params="['action','delete_file']"

                                                   data-file-ajax-toast-success-txt="Successfully Uploaded!"
                                                   data-file-ajax-toast-error-txt="One or more files not uploaded!"
                                                   class="custom-file-input absolute-full">

                                            <!-- icon -->
                                            <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                        </label>

                                    </td>


                                    <!-- Descrizione -->
                                    <td>
                                        <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione][]">{{$video->getDescrizione()}}</textarea>
                                        </div>
                                    </td>

                                    <!-- Option -->
                                    <td class="position-relative text-center">

                                        <!-- direct delete -->
                                        <!--
                                        <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                            <i class="fi fi-thrash"></i>
                                        </a>
                                        -->

                                        <!-- remove button (confirm trigger) -->
                                        <a href="#"
                                           class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                            <i class="fi fi-thrash"></i>
                                        </a>

                                        <!-- confirm -->
                                        <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                            <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                            <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                            <a href="#!"
                                               class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                            <a href="#!"
                                               class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                        </div>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>
                </div>
            </section>
            <!-- /MEDIA-->

            <!-- PREMI -->
            <section class="rounded shadow-3d" id="s_premi">
                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse4"
                   role="button" aria-expanded="false" aria-controls="collapse4">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                PREMI
                                <small class="fs--11 text-muted d-block mt-1">Premi & Nomination</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                @if($showNomination)
                    <div class="collapse mb-3 show" id="collapse4">
                        @else
                            <div class="collapse mb-3" id="collapse4">
                                @endif
                                <!-- /section header -->
                                <!--
                 default
                     data-table-clone-method="append|prepend"
             -->
                                <div class="js-form-advanced-table mb-6"
                                     data-table-column-insert-before=".js-clone-before"
                                     data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                                     data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                                     data-table-column-limit="4"
                                     data-table-row-limit="50"
                                     data-table-row-method="prepend">

                                    <table class="table table-bordered table-align-middle table-sm">
                                        <thead>
                                        <tr>
                                            <th class="w--30">&nbsp;</th>
                                            <th>GENERE</th>
                                            <th>TIPOLOGIA</th>
                                            <th>STATO</th>
                                            <th>ASSEGNATO A</th>
                                            <th>DESCRIZIONE</th>
                                            <th>DATA</th>
                                            <th class="w--80 text-center">
                                                <a href="#"
                                                   class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                                    <i class="fi fi-plus"></i>
                                                </a>
                                            </th>
                                        </tr>
                                        </thead>

                                        <!--
                                            See Sortable doumentation if ajax reorder is needed
                                        -->
                                        <tbody class="sortable">

                                        <!--
                                            OPTIONAL
                                            ACTING AS A TEMPLATE TO CLONE
                                            IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                            Else, the first TR is used by default!
                                            .js-ignore = optional, used by sortable to igore from drag/drop reorder
                                        -->
                                        <tr class="js-ignore hide">
                                            <!-- sortable handler -->
                                            <td class="px-0 text-center">
                                                <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                            </td>
                                            <!-- Genere -->
                                            <td>
                                                <input type="hidden" name="premio[Id_premio][]" value="">
                                                <div class="form-label-group">
                                                    <select class="form-control bs-select" name="premio[genere][]">
                                                        @foreach($premi as $k => $premio)
                                                            <option value="{{$premio->getIdPremio()}}">{{$premio->getNome()}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label>Premio</label>
                                                </div>
                                            </td>

                                            <!-- Tipologia -->
                                            <td>
                                                <div class="form-label-group">
                                                    <select class="form-control bs-select" name="premio[tipologia][]">
                                                        @foreach($tipologiaPremi as $k => $tipologiaPremio)
                                                            <option value="{{$tipologiaPremio->getidTipologiaPremio()}}">{{$tipologiaPremio->getNome()}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label>Premio per</label>
                                                </div>
                                            </td>

                                            <!-- Stato -->
                                            <td>
                                                <div class="form-label-group">
                                                    <select class="form-control bs-select" name="premio[stato][]">
                                                        <option value="0">Nomination</option>
                                                        <option value="1">Vittoria</option>
                                                    </select>
                                                    <label>Stato:</label>
                                                </div>
                                            </td>

                                            <!-- Assegnato -->
                                            <td>
                                                <div class="form-label-group">
                                                    <select class="form-control select2-plugin ajax-method"
                                                            name="premio[select_filmmakers][]"
                                                            data-input-min-length="1" data-ajax-method="GET"
                                                            data-ajax-url="{{asset('api/cast')}}"
                                                            data-input-delay="250">
                                                    </select>
                                                    <label
                                                            class="label-select2">Assegnato a</label>
                                                </div>
                                            </td>


                                            <!-- Descrizione -->
                                            <td>
                                                <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="premio[descrizione][]"></textarea>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-label-group mb-3">
                                                    <input placeholder="Data premiazione" type="date"
                                                           value="{{date("Y-m-d")}}"
                                                           class="form-control" name="premio[data_premiazione][]">
                                                    <label>Data Premiazione</label>
                                                </div>
                                            </td>


                                            <!-- Option -->
                                            <td class="position-relative text-center">

                                                <!-- direct delete -->
                                                <!--
                                                <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                                    <i class="fi fi-thrash"></i>
                                                </a>
                                                -->

                                                <!-- remove button (confirm trigger) -->
                                                <a href="#"
                                                   class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                                    <i class="fi fi-thrash"></i>
                                                </a>

                                                <!-- confirm -->
                                                <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                                    <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                                    <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                                    <a href="#!"
                                                       class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                                    <a href="#!"
                                                       class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                                </div>

                                            </td>

                                        </tr>

                                        @foreach($filmObj->getNominations() as $nomination)
                                            <!-- preadded -->
                                            <tr>
                                                <!-- sortable handler -->
                                                <td class="px-0 text-center">
                                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                                </td>
                                                <!-- Genere -->
                                                <td>
                                                    <input type="hidden" name="premio[Id_premio][]"
                                                           value="{{$nomination->getIdNomination()}}">
                                                    <div class="form-label-group">
                                                        <select class="form-control bs-select" name="premio[genere][]">
                                                            @foreach($premi as $k => $premio)
                                                                <option value="{{$premio->getIdPremio()}}"
                                                                        @if($nomination->getCategoriaPremio()->getPremio()->getIdPremio() == $premio->getIdPremio())
                                                                            selected="selected"
                                                                        @endif
                                                                >{{$premio->getNome()}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label>Premio</label>
                                                    </div>
                                                </td>

                                                <!-- Tipologia -->
                                                <td>
                                                    <div class="form-label-group">
                                                        <select class="form-control bs-select"
                                                                name="premio[tipologia][]">
                                                            @foreach($tipologiaPremi as $k => $tipologiaPremio)
                                                                <option value="{{$tipologiaPremio->getidTipologiaPremio()}}"
                                                                        @if($nomination->getCategoriaPremio()->getTipologia()->getidTipologiaPremio() == $tipologiaPremio->getidTipologiaPremio())
                                                                            selected="selected"
                                                                        @endif
                                                                >{{$tipologiaPremio->getNome()}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label>Premio per</label>
                                                    </div>
                                                </td>

                                                <!-- Stato -->
                                                <td>
                                                    <div class="form-label-group">
                                                        <select class="form-control bs-select" name="premio[stato][]">
                                                            <option value="0" @if($nomination->getStato() === 0)
                                                                selected="selected"
                                                                    @endif>Nomination
                                                            </option>
                                                            <option value="1" @if($nomination->getStato() === 1)
                                                                selected="selected"
                                                                    @endif>Vittoria
                                                            </option>
                                                        </select>
                                                        <label>Stato:</label>
                                                    </div>
                                                </td>

                                                <!-- Assegnato -->
                                                <td>
                                                    <div class="form-label-group">
                                                        <select class="form-control select2-plugin ajax-method"
                                                                name="premio[select_filmmakers][]"
                                                                data-input-min-length="1" data-ajax-method="GET"
                                                                data-ajax-url="{{asset('api/cast')}}"
                                                                data-input-delay="250">
                                                            <option value="{{$nomination->getFilmMaker()->getIdFilmMaker()}}">{{$nomination->getFilmMaker()->getNome()}} {{$nomination->getFilmMaker()->getCognome()}}</option>
                                                        </select>
                                                        <label
                                                                class="label-select2">Assegnato a</label>
                                                    </div>
                                                </td>


                                                <!-- Descrizione -->
                                                <td>
                                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="premio[descrizione][]">{{$nomination->getDescrizione()}}</textarea>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="form-label-group mb-3">
                                                        <input placeholder="Data premiazione" type="date"
                                                               value="{{$nomination->getDataPremiazione()}}"
                                                               class="form-control" name="premio[data_premiazione][]">
                                                        <label>Data Premiazione</label>
                                                    </div>
                                                </td>


                                                <!-- Option -->
                                                <td class="position-relative text-center">

                                                    <!-- direct delete -->
                                                    <!--
                                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                                        <i class="fi fi-thrash"></i>
                                                    </a>
                                                    -->

                                                    <!-- remove button (confirm trigger) -->
                                                    <a href="#"
                                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                                        <i class="fi fi-thrash"></i>
                                                    </a>

                                                    <!-- confirm -->
                                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                                        <a href="#!"
                                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                                        <a href="#!"
                                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                                    </div>

                                                </td>

                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                    <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe
                                        per
                                        riordinarle</small>

                                </div>
                            </div>
            </section>
            <!-- /PREMI-->
            <button type="submit" class="btn btn-lg btn-success float-end"> Salva <i
                        class="fi fi-check mr--0 ml--15"></i>
            </button>
        </form>
    </div>
    <!-- /MIDDLE -->
@endsection
@section('script')
    @if($showCast)
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    document.getElementById('s_cast').scrollIntoView();
                }, 500);
            });
        </script>
    @elseif($showNomination)
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    document.getElementById('s_premi').scrollIntoView();
                }, 500);
            });
        </script>
    @endif
@endsection