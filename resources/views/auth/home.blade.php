@extends('layouts.app')

@section('content')
    <h3>
       Dati DB:
    </h3>
    <table style="font-size: 20px;">
        @json(session('errors'))
        @json(session('user'))
        @foreach ($result[0] as $key => $value)
            <tr>
                <td style="text-transform: capitalize;"> {{ $key }}:</td>
                <td style="width: 50px"></td>
                <td> {{ $value }}</td>
            </tr>
        @endforeach
    </table>
@endsection
