@extends('skeletons.front.app')

@section('content')
    <section class="pt--10 pb--60 px-3 pb-3 container min-h-75vh">
        <div class="d-flex justify-content-center">
            <div>
                <h1 class="display-1 font-raleway text-center" style="font-size: 150px;">{{$code}}</h1>
                <h1 class="display-1 font-raleway text-center">{{$message}}</h1>
            </div>
        </div>
    </section>
@endsection
