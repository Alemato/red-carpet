@section('siderbar')
    <!-- SIDEBAR -->
    <aside id="aside-main"
           class="aside-start bg-gradient-primary aside-hide-xs d-flex align-items-stretch justify-content-lg-between align-items-start flex-column">

        <!--
            LOGO
            visibility : desktop only
        -->
        <div class="align-self-baseline w-100">
            <div class="clearfix d-flex justify-content-between">

                <!-- Logo : height: 60px max -->
                <a class="w-100 align-self-center navbar-brand px-3 py-4 " href="{{asset('')}}">
                    <img class="mx-auto d-block" src="{{asset('assets/img/home/smarty-text.png')}}" alt="...">
                </a>

            </div>
        </div>
        <!-- /LOGO -->


        <div class="aside-wrapper scrollable-vertical scrollable-styled-light align-self-baseline h-100 w-100">

            <!--

                All parent open navs are closed on click!
                To ignore this feature, add .js-ignore to .nav-deep

                Links height (paddings):
                    .nav-deep-xs
                    .nav-deep-sm
                    .nav-deep-md  	(default, ununsed class)

                .nav-deep-hover 	hover background slightly different
                .nav-deep-bordered	bordered links


                ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                IMPORTANT NOTE:
                    Curently using ajax navigation!
                    remove .js-ajax class to have regular links!
                ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            -->
            <nav class="nav-deep nav-deep-dark nav-deep-hover">
                <ul class="nav flex-column">

                    <li @if(is_active_link('admin', true))class="nav-item active" @else class="nav-item" @endif>
                        <a class="nav-link js-ajax" href="{{asset('admin')}}">
                            <i class="fi fi-menu-dots"></i>
                            <b>Dashboard</b>
                        </a>
                    </li>

                    @if(can('Creazione e Modifica Film') || can('Lista Film'))
                        <li @if(is_active_link('admin/film') || is_active_link('admin/create_film') || is_active_link('admin/modify_film'))class="nav-item active"
                            @else class="nav-item" @endif>
                            <a class="nav-link" href="#">
										<span class="group-icon float-end">
											<i class="fi fi-arrow-end-slim"></i>
											<i class="fi fi-arrow-down-slim"></i>
										</span>
                                <i class="fi fi-movie"></i>
                                Film
                            </a>

                            <ul class="nav flex-column fs--15">
                                @if(can('Creazione e Modifica Film') || can('Lista Film'))
                                    <li @if(is_active_link('admin/film') || is_active_link('admin/modify_film'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/film')}}">
                                            Lista Film
                                        </a>
                                    </li>
                                @endif
                                @if(can('Creazione e Modifica Film'))
                                    <li @if(is_active_link('admin/create_film'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/create_film')}}">
                                            Aggiungi Film
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(can('Creazione e Modifica Serie Tv') || can('Lista Serie Tv'))
                        <li @if(is_active_link('admin/serietv') || is_active_link('admin/create_serietv') || is_active_link('admin/modify_serietv'))class="nav-item active"
                            @else class="nav-item" @endif>
                            <a class="nav-link" href="#">
                    <span class="group-icon float-end">
                        <i class="fi fi-arrow-end-slim"></i>
                        <i class="fi fi-arrow-down-slim"></i>
                    </span>
                                <i class="fi fi-shape-3dots"></i>
                                SerieTv
                            </a>
                            <ul class="nav flex-column fs--15">
                                @if(can('Creazione e Modifica Serie Tv') || can('Lista Serie Tv'))
                                    <li @if(is_active_link('admin/serietv') || is_active_link('admin/modify_serietv'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/serietv')}}">
                                            Lista SerieTv
                                        </a>
                                    </li>
                                @endif
                                @if(can('Creazione e Modifica Serie Tv'))
                                    <li @if(is_active_link('admin/create_serietv'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/create_serietv')}}">
                                            Aggiungi SerieTV
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(can('Lista Film') && can('Lista Serie Tv'))
                        <li @if(is_active_link('admin/generi'))class="nav-item active" @else class="nav-item" @endif>

                            <a class="nav-link" href="{{asset('admin/generi')}}">
                                <i class="nav-icon fi fi-shape-star"><!-- main icon --></i>
                                Generi
                            </a>

                        </li>
                    @endif


                    @if((can('Lista Film') && can('Lista Serie Tv')) || can('Creazione e Modifica Filmmaker'))
                        <li @if(is_active_link('admin/create_filmmaker') || is_active_link('admin/listfilmmaker') || is_active_link('admin/modify__filmmaker'))class="nav-item active"
                            @else class="nav-item" @endif>
                            <a class="nav-link" href="#">
                    <span class="group-icon float-end">
                        <i class="fi fi-arrow-end-slim"></i>
                        <i class="fi fi-arrow-down-slim"></i>
                    </span>
                                <i class="fi fi-mollecules"></i>
                                Attori & Filmmakers
                            </a>

                            <ul class="nav flex-column fs--15">
                                @if((can('Lista Film') && can('Lista Serie Tv')) || can('Creazione e Modifica Filmmaker'))
                                    <li @if(is_active_link('admin/create_filmmaker'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/create_filmmaker')}}">
                                            Aggiungi Attore/Filmmaker
                                        </a>
                                    </li>
                                @endif
                                @if(can('Lista Film') && can('Lista Serie Tv'))
                                    <li @if(is_active_link('admin/listfilmmaker') || is_active_link('admin/modify__filmmaker'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/filmmaker')}}">
                                            Lista Filmmaker
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(can('Lista Utenza')||can('Creazione e Modifica User') || can('Lista Gruppi'))
                        <li @if(is_active_link('admin/user_list') || is_active_link('admin/groups_services'))class="nav-item active"
                            @else class="nav-item" @endif>
                            <a class="nav-link" href="#">
                    <span class="group-icon float-end">
                        <i class="fi fi-arrow-end-slim"></i>
                        <i class="fi fi-arrow-down-slim"></i>
                    </span>
                                <i class="fi fi-layers"></i>
                                Utenza
                            </a>

                            <ul class="nav flex-column fs--15">
                                @if(can('Lista Utenza')||can('Creazione e Modifica User'))
                                    <li @if(is_active_link('admin/user_list'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/user_list')}}">
                                            Gestione Utenti
                                        </a>
                                    </li>
                                @endif
                                @if(can('Lista Gruppi'))
                                    <li @if(is_active_link('admin/groups_services'))class="nav-item active"
                                        @else class="nav-item" @endif>
                                        <a class="nav-link js-ajax" href="{{asset('admin/groups_services')}}">
                                            Gruppi e Servizi
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(can('Gestione Recensioni'))
                    <li @if(is_active_link('admin/review_grouped_by_opere'))class="nav-item active"
                        @else class="nav-item" @endif>
                        <a class="nav-link js-ajax" href="{{asset('admin/review_grouped_by_opere')}}">
                            <i class="fi fi-like"></i>
                            <b>Recensioni</b>
                        </a>
                    </li>
                    @endif


                    <li @if(is_active_link('admin/dggddg') || is_active_link('admin/dggddg'))class="nav-item active"
                        @else class="nav-item" @endif>
                        <a class="nav-link" href="#">
                    <span class="group-icon float-end">
                        <i class="fi fi-arrow-end-slim"></i>
                        <i class="fi fi-arrow-down-slim"></i>
                    </span>
                            <i class="fi fi-eq-vertical"></i>
                            Impostazioni
                        </a>

                        <ul class="nav flex-column fs--15">
                            {{--
                                <li @if(is_active_link('admin/dggddg'))class="nav-item active"
                                    @else class="nav-item" @endif>
                                    <a class="nav-link" href="#">
                                        Homepage
                                    </a>
                                </li>
                            --}}

                            <li @if(is_active_link('admin/dggddg'))class="nav-item active"
                                @else class="nav-item" @endif>
                                <a class="nav-link" href="{{ asset('admin/impostazioni') }}">
                                    Slider Pubblicitri
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </nav>

        </div>

    </aside>
    <!-- /SIDEBAR -->
@endsection