@include('skeletons.back.header')
@include('skeletons.back.siderbar')

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <meta name="description" content="...">

    <meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1, user-scalable=0">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- up to 10% speed up for external res -->
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com/">
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <!-- preloading icon font is helping to speed up a little bit -->
    <link rel="preload" href="{{asset('assets/fonts/flaticon/Flaticon.woff2')}}" as="font" type="font/woff2" crossorigin>

    <!-- non block rendering : page speed : js = polyfill for old browsers missing `preload` -->
    <link rel="stylesheet" href="{{asset('assets/css/core.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor_bundle.min.css')}}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700%7CRoboto:100,300,400,500,700&amp;display=swap">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('assets/logo/icon_512x512.png')}}">

    <link rel="manifest" href="{{asset('assets/images/manifest/manifest.json')}}">
    <meta name="theme-color" content="#bb3132">
</head>
<body class="layout-admin aside-sticky layout-padded bg-theme-color-light">
<div id="wrapper" class="d-flex align-items-stretch flex-column">

    @yield('header')
    <div id="wrapper_content" class="d-flex flex-fill">

        @yield('siderbar')

        @if(session()->flash()->has('errorPermission'))
            <div class="hide toast-on-load"
                 data-toast-type="danger"
                 data-toast-title="Informazione"
                 data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-dislike float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorPermission'))[0] }}</span></div></div>"
                 data-toast-pos="top-center"
                 data-toast-delay="2000"
                 data-toast-fill="true"
                 data-
            ></div>
        @endif

        @yield('content')
    </div>

</div>

<script src="{{asset('assets/js/core.min.js')}}"></script>
<div id="page_js_files">@yield('script')</div>
</body>
</html>