@include('skeletons.front.header')
@include('skeletons.front.footer')

<!doctype html>
<html lang="it" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>{{$titlepage}}</title>
    <meta name="description" content="...">

    <meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1, user-scalable=0">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- up to 10% speed up for external res -->
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com/">
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <!-- preloading icon font is helping to speed up a little bit -->
    <link rel="preload" href="{{ asset('assets/fonts/flaticon/Flaticon.woff2" as="font" type="font/woff2') }}" crossorigin>

    <link rel="stylesheet" href="{{ asset('assets/css/core.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor_bundle.min.css') }}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700%7CRoboto:100,300,400,500,700&amp;display=swap">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/logo/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('assets/logo/icon_512x512.png') }}">

    <link rel="manifest" href="{{ asset('assets/images/manifest/manifest.json') }}">
    <meta name="theme-color" content="#bb3132">

</head>
<body class="header-sticky">
<div id="wrapper">
@yield('header')

@yield('content')

@yield('footer')
</div>
<script src="{{ asset('assets/js/core.min.js') }}"></script>
<div id="page_js_files">@yield('script')</div>
</body>
</html>
