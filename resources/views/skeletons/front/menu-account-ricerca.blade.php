@section('menu-account-ricerca')


        @if(auth())

            <!-- OPTIONS -->
            <ul class="list-inline list-unstyled mb--0 d-flex align-items-end">
            <li class="list-inline-item ml--6 mr--6 dropdown float-start">

                        <a href="#" id="dropdownAccountOptions"
                           class="btn btn-sm rounded-circle btn-primary btn-soft-static" data-toggle="dropdown"
                           aria-expanded="false" aria-haspopup="true">
									<span class="group-icon">
										<i class="fi fi-user-male"></i>
										<i class="fi fi-close"></i>
									</span>
                </a>

                <div aria-labelledby="dropdownAccountOptions"
                     class="prefix-link-icon prefix-icon-dot dropdown-menu dropdown-menu-clean dropdown-menu-invert dropdown-click-ignore p--0 mt--18 fs--15">
                    <div class="dropdown-header">
                        {{session('user')['nome']}} {{session('user')['cognome']}}
                    </div>
                    <div class="dropdown-divider"></div>
                    @if(can("Creazione Recensione"))
                    <a href="{{asset('lista_recensioni_personali')}}" class="dropdown-item text-truncate font-weight-light">
                        Le Tue Recensioni
                    </a>
                    @endif
                    <a href="{{asset('impostazioni_account')}}"
                       class="dropdown-item text-truncate font-weight-light">
                        Impostazioni Account
                    </a>
                    @if(can("Impostazioni Sito"))
                        <a href="{{asset('admin')}}"
                           class="dropdown-item text-truncate font-weight-light">
                            Back-End
                        </a>
                    @endif
                    <div class="dropdown-divider mb--0"></div>
                    <a href="{{asset('logout')}}" class="prefix-icon-ignore dropdown-footer dropdown-custom-ignore">
                        <i class="fi fi-power float-start"></i>
                        Log Out
                    </a>
                </div>

            <li class="list-inline-item ml--6 mr--6 dropdown float-start">

                <a href="#"
                   class="btn-sow-search-toggler btn btn-sm rounded-circle btn-primary btn-soft-static">
                    <i class="fi fi-search"></i>
                </a>

            </li>

                <li class="list-inline-item ml--6 mr--6 dropdown float-start">

                    <!-- TRIGGER -->
                </li>
            </ul>
            <!-- /OPTIONS -->

        @else


            <!-- OPTIONS -->
                <ul class="list-unstyled mb--0 d-flex align-items-center">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#"
                       data-href="{{asset('components/accedi_modal')}}"
                       data-ajax-modal-size="modal-lg"
                       data-ajax-modal-centered="false"
                       data-ajax-modal-callback-function=""
                       data-ajax-modal-backdrop=""
                       class="js-ajax-modal text-truncate font-weight-light nav-link">
                        Accedi
                    </a>
                </li>
            </ul>
            <li class="ml--6 mr--6 dropdown float-start">

                <a href="#"
                   class="btn-sow-search-toggler btn btn-sm rounded-circle btn-primary btn-soft-static">
                    <i class="fi fi-search"></i>
                </a>

            </li>

                    <li class=" ml--6 mr--6 dropdown float-start">

                        <!-- TRIGGER -->
                    </li>
                </ul>
                <!-- /OPTIONS -->
        @endif





@endsection