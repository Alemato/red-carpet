
@section('footer')
<!-- Footer -->
<footer id="footer" class="footer-dark text-white">
    <div class="container">

        <div class="row">

            <div class="bg-distinct col-12 col-md-6 col-lg-4 text-center p-0 py-5">

                <div class="footer-svg-shape position-absolute absolute-top z-index-2 mt--n70 w-100 overflow-hidden pt--5">
                    <svg viewBox="0 0 1440 28" fill="none" xmlns="http://www.w3.org/2000/svg"
                         preserveAspectRatio="none">
                        <path d="M0 28H1440C1440 28 1154 3.21961e-10 720 1.30487e-09C286 2.28777e-09 0 28 0 28Z"></path>
                    </svg>
                </div>

                <form method="post" action="#subscribe_url" class="px-4">

                    <input type="hidden" name="action" value="subscribe" tabindex="-1">

                    <p id="btnAriaFooterFormTitle" class="text-white">
                        Rimani informato, iscriviti alla nostra newletter!
                    </p>


                    <div class="input-group-over input-group-pill mb-4">

                        <input required class="form-control b-0" name="subscriber_email" type="email" value=""
                               placeholder="Indirizzo email" aria-label="indirizzo email">

                        <button type="submit" class="btn bg-transparent shadow-none" aria-label="subscribe"
                                aria-labelledby="btnAriaFooterFormTitle">
                            <i class="fi fi-send fs--18"></i>
                        </button>

                    </div>


                </form>

                <div class="mt--12">

                    <a href="#" class="btn btn-sm btn-facebook transition-hover-top mb-2 rounded-circle"
                       rel="noopener" aria-label="facebook page">
                        <i class="fi fi-social-facebook"></i>
                    </a>

                    <a href="#" class="btn btn-sm btn-twitter transition-hover-top mb-2 rounded-circle"
                       rel="noopener" aria-label="twitter page">
                        <i class="fi fi-social-twitter"></i>
                    </a>

                    <a href="#!" class="btn btn-sm btn-youtube transition-hover-top mb-2 rounded-circle"
                       rel="noopener" aria-label="youtube page">
                        <i class="fi fi-social-youtube"></i>
                    </a>

                </div>

            </div>


            <div class="pl--30 col-12 col-md-6 col-lg-8 py-5 text-center-xs">


                <ul class="mb-0 breadcrumb bg-transparent p-0 block-xs font-weight-bold fs--18">
                    <li class="breadcrumb-item"><a href="#">Contact</a></li>
                    <li class="breadcrumb-item"><a href="#">About Us</a></li>
                    <li class="breadcrumb-item"><a href="#">Terms &amp; Conditions</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">GDPR &amp; Cookies</a></li>
                </ul>

                <div class="text-muted mt--30">
                    <p>
                        Sul sito Smarty trovi tutto sul mondo del cinema e delle serieTv:
                        i trailer dei film in uscita, le recensioni delle migliori testate gionalistiche , recensioni di altri utenti appassionati e tutte le informazioni su attori e cast e tanto altro ancora!!!
                    </p>
                </div>

            </div>

        </div>

    </div>

    <div class="bg-distinct py-3 clearfix">

        <div class="container clearfix font-weight-light text-center-xs">

            <div class="fs--14 py-2 float-start float-none-xs m-0-xs">
                &copy; Smarty.
            </div>


        </div>
    </div>

</footer>
<!-- /Footer -->
@endsection