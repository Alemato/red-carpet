@include('skeletons.front.menu')
@include('skeletons.front.menu-account-ricerca')
@include('components.search')

@section('header')
<!-- HEADER -->
<header id="header" class="shadow-xs bg-gradient-primary">
    <!-- NAVBAR -->
    <div class="container position-relative">

        @yield('search-component')


        <nav class="navbar navbar-expand-lg navbar-dark justify-content-lg-between justify-content-md-inherit">

            <div class="align-items-start">

                <!-- mobile menu button : show -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMainNav"
                        aria-controls="navbarMainNav" aria-expanded="false" aria-label="Toggle navigation">
                    <svg width="25" viewBox="0 0 20 20">
                        <path d="M 19.9876 1.998 L -0.0108 1.998 L -0.0108 -0.0019 L 19.9876 -0.0019 L 19.9876 1.998 Z"></path>
                        <path d="M 19.9876 7.9979 L -0.0108 7.9979 L -0.0108 5.9979 L 19.9876 5.9979 L 19.9876 7.9979 Z"></path>
                        <path d="M 19.9876 13.9977 L -0.0108 13.9977 L -0.0108 11.9978 L 19.9876 11.9978 L 19.9876 13.9977 Z"></path>
                        <path d="M 19.9876 19.9976 L -0.0108 19.9976 L -0.0108 17.9976 L 19.9876 17.9976 L 19.9876 19.9976 Z"></path>
                    </svg>
                </button>

                <!--
                    Logo : height: 70px max
                -->
                <a class="navbar-brand" href="/">
                    <img src="{{asset("assets/img/home/smarty-text.png")}}" alt="...">
                </a>

            </div>

            @yield('menu')

            @yield('menu-account-ricerca')

        </nav>

    </div>
    <!-- /NAVBAR -->
</header>
<!-- SOTTOMENU' FILM -->
@php $menuArray = menu(); @endphp
<div class="collapse c-c w-100 position-fixed z-index-10" id="collapseExample1">
    <div class="d-flex justify-content-center bg-white">
        <div class="col-auto">
            <ul class="list-inline mb-0 overflow-x-auto p-3">
                @foreach($menuArray[0] as $gen)
                    <li class="inline-menu">
                        <a href="{{asset('lista_film?genere=' . $gen->getIdGenere())}}"
                           class="dropdown-link text-decoration-none">{{$gen->getNome()}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

<!-- SOTTOMENU' SERIE TV -->
<div class="collapse c-c w-100 position-fixed z-index-10" id="collapseExample2">
    <div class="d-flex justify-content-center bg-white">
        <div class="col-auto">
            <ul class="list-inline mb-0 overflow-x-auto p-3">
                @foreach($menuArray[0] as $gen)
                    <li class="inline-menu">
                        <a href="{{asset('lista_serietv?genere=' . $gen->getIdGenere())}}"
                           class="dropdown-link text-decoration-none">{{$gen->getNome()}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

<!-- SOTTOMENU' FILMMAKER -->
<div class="collapse c-c w-100 position-fixed z-index-10" id="collapseExample3">
    <div class="d-flex justify-content-center bg-white">
        <div class="col-auto">
            <ul class="list-inline mb-0 overflow-x-auto p-3">
                @foreach($menuArray[1] as $tpr)
                    <li class="inline-menu">
                        <a href="{{asset('filmmaker_list/'.$tpr->getTipologia())}}"
                           class="dropdown-link text-decoration-none">{{decodifica_array_filmmakers_to_string($tpr->getTipologia())}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!-- /HEADER-->
@endsection
@section('script')
    <script>
        // mouseenter con click opzionale
        $('.c-link').on("click mouseenter", function (){
            //console.log($(this).data("target"));
            //console.log($(".c-c"));
            $(".c-c").each(function (){
                //console.log($(this));
                $(this).collapse("hide");
            });
            $(".c-c").mouseleave(function (){
                $(".c-c").each(function (){
                    //console.log($(this));
                    $(this).collapse("hide");
                });
            });
            $("*:not(.c-c)").click(function (){
                $(".c-c").each(function (){
                    //console.log($(this));
                    $(this).collapse("hide");
                });
            });
            $(".b-b").mouseenter(function (){
                $(".c-c").each(function (){
                    //console.log($(this));
                    $(this).collapse("hide");
                });
            });
            //console.log($(target));
            var target = $(this).data("target");
            $(target).collapse("toggle");
        });
    </script>
@endsection