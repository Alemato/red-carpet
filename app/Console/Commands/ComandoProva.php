<?php

namespace App\Console\Commands;

class ComandoProva extends Command{
    protected $name = 'prova:comando';
    protected $help = 'fa ls -lart';
    protected $description = 'non fa niente di che: ls -lart';

    public function handler(){
        $this->info("=============");
        $this->info("Fa ls -lart");
        $this->info("=============");

        $command = 'ls -lart';

        $this->info(shell_exec($command));
        $this->info("++++Comando Completato++++");
        $this->info("=============");
    }
}
