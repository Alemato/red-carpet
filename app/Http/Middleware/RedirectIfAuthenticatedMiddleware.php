<?php

namespace App\Http\Middleware;

use App\Services\UserService as UserService;
use DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as Handle;

class RedirectIfAuthenticatedMiddleware
{
    public function __invoke(Request $request, Handle $handler)
    {
        $userService = new UserService();
        if ($userService->check()) {
            return redirect('/');
        }

        return $handler->handle($request);
    }
}