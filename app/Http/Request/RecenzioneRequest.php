<?php


namespace App\Http\Request;


use App\Support\FormRequest;

class RecenzioneRequest extends FormRequest
{
    protected function afterValidationFails()
    {
        $this->forget('csrf_value');
        $this->forget('csrf_name');
    }

    public function messages()
    {
        return [
            'titolo.required' => 'Il Titolo è richiesto',
            'titolo.string' => 'Il Titolo deve essere un testo',
            'contenuto.required' => 'La recenzione è richiesta',
            'contenuto.string' => 'La recenzione deve essere un testo',
            'valutazione.required' => 'La valutazione è richiesta'
        ];
    }

    public function rules()
    {
        return [
            'titolo'=>'required|string',
            'contenuto'=>'required|string',
            'valutazione'=>'required'
        ];
    }

}