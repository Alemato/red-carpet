<?php


namespace App\Http\Request;


use App\Http\Request\Rules\EmailExistRules;
use App\Http\Request\Rules\EmailRules;
use App\Http\Request\Rules\PasswordRules;
use App\Support\FormRequest;

class StoreLoginRequest extends FormRequest
{
    protected function afterValidationPasses()
    {
        $this->forget('csrf_value');
        $this->forget('csrf_name');
        $this->password = sha1($this->password);
    }

    public function messages()
    {
        return [
            'email.email' => 'L\'email deve essere un email valida',
            'email.required' => 'L\'email è richiesto',
            'password.string' => 'La password deve essere un testo avente: una lettera maiuscola, una minuscola, un numero e un carattere speciale',
            'password.required' => 'La password è richiesta',
        ];
    }

    public function rules(){
        // regole di validazione
        return [
            'email' => ['required', 'email', new EmailExistRules()],
            'password' => ['required','string', new PasswordRules()]
        ];
    }

}