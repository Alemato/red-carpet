<?php


namespace App\Http\Request\Rules;


use DateTime;
use Illuminate\Contracts\Validation\Rule;

class BirthdayRules implements Rule
{
    /**
     * BirthdayRules constructor.
     */
    public function __construct()
    {
    }


    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {
        $magiore  = date(DateTime::ISO8601, mktime(23, 59, 59, date("m")  , date("d"), date("Y") -18));
        return $value <= $magiore;
    }

    /**
     * @inheritDoc
     */
    public function message()
    {
        return 'Non sei maggiorenne, non puoi iscriverti';
    }
}