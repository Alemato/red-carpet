<?php


namespace App\Http\Request\Rules;


use Illuminate\Contracts\Validation\Rule;

class CustomRules implements Rule
{
    /**
     * CustomRules constructor.
     */
    public function __construct()
    {
    }

    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {
        //condizione di valutazione
        return true;
    }

    /**
     * @inheritDoc
     */
    public function message()
    {

        return 'l\'attributo :attribute è sbagliato.';
    }
}