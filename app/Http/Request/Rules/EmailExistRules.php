<?php


namespace App\Http\Request\Rules;


use Illuminate\Contracts\Validation\Rule;
use DB;

class EmailExistRules implements Rule
{

    /**
     * EmailUniqueRules constructor.
     */
    public function __construct(string $email='')
    {
    }

    public function passes($attribute, $value)
    {
        $result = DB::select('select email from users where email = ?', [$value]);
        return !empty($result);
    }

    public function message()
    {
        return "l'email che hai inserito non esiste nel sistema!";
    }


}