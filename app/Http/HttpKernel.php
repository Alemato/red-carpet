<?php

namespace App\Http;

use Boot\Foundation\HttpKernel as Kernel;

class HttpKernel extends Kernel
{
    /**
     * Injectable Request Input Form Request Validators
     * @var array
     */
    public array $requests = [
        Request\RecenzioneRequest::class,
        Request\StoreImpostazioniAccountUserAdmin::class,
        Request\StoreImpostazioniAccountUserCritica::class,
        Request\StoreImpostazioniAccountUserNormal::class,
        Request\StoreLoginRequest::class,
        Request\StoreUserNormalRequest::class,
        Request\StoreUserCritica::class,
        Request\StoreUserAdmin::class,
        Request\ModifyUserNormalRequest::class,
        Request\ModifyUserCriticoRequest::class,
        Request\ModifyUserAdminRequest::class,
        Request\StoreGroup::class
    ];


    /**
     * Global Middleware
     *
     * @var array
     */
    public array $middleware = [
        Middleware\RouteContextMiddleware::class
//        Middleware\ExampleAfterMiddleware::class,
//        Middleware\ExampleBeforeMiddleware::class
    ];

    /**
     * Route Group Middleware
     */
    public array $middlewareGroups = [
        'api' => [
            Middleware\RedirectIfGuestMiddleware::class
        ],
        'admin' => [
            'csrf',
            Middleware\RedirectIfGuestMiddleware::class,
            Middleware\AmminisatrazioneMiddleware::class
        ],
        'web' => [
            'csrf'
        ]
    ];
}