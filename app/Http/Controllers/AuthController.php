<?php


namespace App\Http\Controllers;

use App\Http\Request\StoreLoginRequest;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class AuthController
{
    public function store(StoreLoginRequest $input, View $view)
    {
        if ($input->failed()) return $view('components.accedi_modal_errors', ['redirect'=>false]);
        $userService = new UserService();
        $successful = $userService->validLogin($input->email, $input->password);



        if (!$successful) {
            $err = 'Credenziali non valide';
            return $view('components.accedi_modal_errors', ['err'=>$err, 'redirect'=>false]);
        }
        $url_referer = $input->getRequest()->getServerParams()["HTTP_REFERER"];
        return $view('components.accedi_modal_errors', ['redirect'=>true, "url_referer"=> $url_referer]);
    }

    public function logout(RequestInput $requestInput)
    {
        $userService = new UserService();
        if ($userService->logout()) {
            return redirect(asset(''));
        } else return redirect($requestInput->getRequest()->getServerParams()["HTTP_REFERER"]);

    }
}