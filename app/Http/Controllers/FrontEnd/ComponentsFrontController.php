<?php


namespace App\Http\Controllers\FrontEnd;


use App\Model\Recensione;
use App\Model\User;
use App\Services\RecensioneService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;
use DB;

class ComponentsFrontController
{
    public function show(View $view, RequestInput $requestInput)
    {
        //dd($requestInput->getArguments()['componet']);
        //$requestInput->getArguments()
        switch ($requestInput->getArguments()['componet']) {
            case "accedi_modal":
                return $view('components.accedi_modal');
            case "recenzione_modal":
            {
                $recenzioneObj = new Recensione();
                if (isset($requestInput->getArguments()['idSpecifico'])) {
                    $idRecenzione = intval($requestInput->getArguments()['idSpecifico']);
                    $recenzioneService = new RecensioneService();
                    $recenzioneObj = $recenzioneService->getRecensioneById($idRecenzione);
                }
                $user = new User();
                if (auth()) {
                    $userService = new UserService();
                    $user = $userService->getUser();
                }
                //dd($requestInput->getRequest()->getServerParams()["HTTP_REFERER"]);
                $path = parse_url($requestInput->getRequest()->getServerParams()["HTTP_REFERER"])["path"];
                return $view('components.recenzione_modal', ["user" => $user, "recenzioneObj" => $recenzioneObj, "path" => $path]);
            }
            default:
                break;
        }

        //return $view('front.index');
    }
}