<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\ImmagineService;
use App\Services\SerieTvService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class SerieTvShowController
{
    public function show(RequestInput $requestInput, View $view)
    {
        $serietvService = new SerieTvService();
        $args = $requestInput->getArguments();
        $serietv = $serietvService->getSerieTvById($args['idSerieTv']);
        $regista = array();
        foreach ($serietv->getRuoli() as $ruolo) {
            if ($ruolo->getTipologia() == '2') {
                $regista['url'] = 'filmmaker/' . $ruolo->getFilmMaker()->getIdFilmMaker();
                $regista['nome'] = $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome();
                break;
            }
        }
        $attori = array();
        foreach ($serietv->getRuoli() as $ruolo) {
            if (count($attori) > 5) {
                break;
            }
            if ($ruolo->getTipologia() == '1') {
                array_push($attori, $ruolo);
            }

        }
        $valutazione = 0;
        $qr = $requestInput->getQueryString();
        $sh = true;
        $shwcr = false;
        $shwca = false;
        foreach ($qr as $qrk => $argument) {
            if ($qrk === 'shwcr') {
                $shwcr = true;
                $sh = false;
            } else if ($qrk === 'shwca') {
                $shwca = true;
                $sh = false;
            }
        }
        try{
            $sliders = $serietvService->getUltimeSerieTvCarosello();
        }catch (\Exception $exception){
            $sliders = array();
        }

        return $view('front.serietv_show', ['titlepage' => $serietv->getTitolo(), "serietv" => $serietv, "regista" => $regista, "attori" => $attori, "valutazione" => $valutazione, "sh" => $sh, "shwcr" => $shwcr, "shwca" => $shwca, "sliders" => $sliders]);
    }

    public function showComponent(RequestInput $requestInput, View $view)
    {
        $userService = new UserService();
        $serietvServce = new SerieTvService();
        $args = $requestInput->getArguments();
        //dd($args);
        $film = $serietvServce->getSerieTvById($args['idSerieTv']);
        switch ($args['component']) {
            case "scheda":
                return $view('front.components_film_serietv.scheda', ["film" => $film]);
            case "cast":
            {
                $cast = array();
                $filmmaker = array();
                $row = array();
                $i = 0;
                foreach ($film->getRuoli() as $ruolo) {
                    if ($i === 4) {
                        $i = 0;
                        array_push($cast, $row);
                        $row = array();
                    }
                    if ($ruolo->getTipologia() == '2') {
                        array_push($row, $ruolo);
                        $i++;
                    }
                }
                foreach ($film->getRuoli() as $ruolo) {
                    if ($i === 4) {
                        $i = 0;
                        array_push($cast, $row);
                        $row = array();
                    }
                    if ($ruolo->getTipologia() == '1') {
                        array_push($row, $ruolo);
                        $i++;
                    }
                }
                if ($i <= 4) {
                    $i = 0;
                    array_push($cast, $row);
                    $row = array();
                }

                foreach ($film->getRuoli() as $ruolo) {
                    if ($i === 4) {
                        $i = 0;
                        array_push($filmmaker, $row);
                        $row = array();
                    }
                    if ($ruolo->getTipologia() != '1') {
                        array_push($row, $ruolo);
                        $i++;
                    }
                }
                if ($i <= 4) {
                    array_push($filmmaker, $row);
                }
                $valutazione_user_normalizzata = $serietvServce->mediaVotiUtenti($film);
                $valutazione_critica_normalizata = $serietvServce->MediaVotiCritici($film);
                if ($valutazione_critica_normalizata >= 1) {
                    $valutazione_critica_p = (100 * $valutazione_critica_normalizata) / 5;
                    $valutazione_critica = (string)$valutazione_critica_p;
                } else {
                    $valutazione_critica_p = 100;
                    $valutazione_critica = "ND";
                }
                if ($valutazione_user_normalizzata >= 1) {
                    $valutazione_user_p = (100 * $valutazione_user_normalizzata) / 5;
                    $valutazione_user = (string)$valutazione_user_p;
                } else {
                    $valutazione_user_p = 100;
                    $valutazione_user = "ND";
                }
                return $view('front.components_film_serietv.cast', ["film" => $film, "valutazione_user" => $valutazione_user_p, "valutazione_critica" => $valutazione_critica_p, "valutazione_user_s" => $valutazione_user, "valutazione_critica_s" => $valutazione_critica, "cast" => $cast, "filmmaker" => $filmmaker]);
            }
            case "stagioni":
            {
                return $view('front.components_film_serietv.stagioni', ["film" => $film]);
            }
            case "critica":
            {
                $auth = auth();
                if ($auth) {
                    $user = $userService->getUser();
                }
                $valutazione_critica = 4.0;
                $valutazione_user = 3.5;
                $recenzioniUtente = array();
                $recenzioniCritico = array();
                $harecensito = false;
                $idRecenzioneSua = 0;
                foreach ($film->getRecensioni() as $recenzione) {
                    if ($auth) {
                        if ($recenzione->getUtente()->getIdUser() === $user->getIdUser()) {
                            $harecensito = true;
                            $idRecenzioneSua = $recenzione->getIdRecensione();
                        }
                    }
                    if ($recenzione->getUtente()->getTipologiaUtenza() == 0) {
                        if (count($recenzioniUtente) < 5) {
                            array_push($recenzioniUtente, $recenzione);
                        }
                    } elseif ($recenzione->getUtente()->getTipologiaUtenza() == 1) {
                        if (count($recenzioniCritico) < 5) {
                            array_push($recenzioniCritico, $recenzione);
                        }
                    }
                    if ($auth) {
                        if (count($recenzioniUtente) > 4 && count($recenzioniCritico) > 4 && $harecensito) {
                            break;
                        }
                    } else {
                        if (count($recenzioniUtente) > 4 && count($recenzioniCritico) > 4) {
                            break;
                        }
                    }
                }
                return $view('front.components_film_serietv.critica', ["film" => $film, "recenzioniUtente" => $recenzioniUtente, "recenzioniCritico" => $recenzioniCritico, "valutazione_critica" => $valutazione_critica, "valutazione_user" => $valutazione_user, "harecensito" => $harecensito, "idRecenzioneSua" => $idRecenzioneSua]);
            }
            case "premi":
            {
                /*$array_by_genere = array();
                foreach ($film->getNominations() as $kn => $nomination) {
                    if (key_exists($nomination->getCategoriaPremio()->getPremio()->getIdPremio(), $array_by_genere)) {
                        array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()], $nomination);
                    } else {
                        $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()] = array();
                        array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()], $nomination);
                    }
                }*/
                $array_by_genere = array();
                foreach ($film->getNominations() as $kn => $nomination) {
                    if (key_exists($nomination->getCategoriaPremio()->getPremio()->getIdPremio(), $array_by_genere)) {
                        if (key_exists(date("Y", strtotime($nomination->getDataPremiazione())), $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()])) {
                            array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))], $nomination);
                        } else {
                            $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))] = array();
                            array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))], $nomination);
                        }
                    } else {
                        $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()] = array();
                        $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))] = array();
                        array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))], $nomination);
                    }
                }

                return $view('front.components_film_serietv.premi', ["film" => $film, "array_nomination" => $array_by_genere]);
            }
            case "trailer":
            {
                return $view('front.components_film_serietv.trailer', ["film" => $film]);
            }
            case "foto":
            {
                $imgService = new ImmagineService();
                $media5 = $imgService->getImgByEntitaAndIdEntitaAndTipologia('serietv', $film->getIdSerieTv(), 5);
                $media6 = $imgService->getImgByEntitaAndIdEntitaAndTipologia('serietv', $film->getIdSerieTv(), 6);
                $array_foto = array();
                $row = array();
                foreach ($media5 as $km => $img) {
                    if (count($row) < 3) {
                        array_push($row, $img);
                    } else {
                        array_push($row, $img);
                        array_push($array_foto, $row);
                        $row = array();
                    }
                }
                foreach ($media6 as $km => $img) {
                    if (count($row) < 3) {
                        array_push($row, $img);
                    } else {
                        array_push($row, $img);
                        array_push($array_foto, $row);
                        $row = array();
                    }
                }
                if (count($row) <= 3) {
                    array_push($array_foto, $row);
                }
                return $view('front.components_film_serietv.foto', ["array_foto" => $array_foto]);
            }
            default:
                break;
        }
        return $view('front.components_film_serietv.scheda', ["film" => $film]);
    }
}