<?php


namespace App\Http\Controllers\Api;


use App\Model\Genere;
use App\Services\GenereService;
use App\Support\RequestInput;
use Slim\Psr7\Response;

class GenereApiController
{
    public function deleteGenere(RequestInput $requestInput, Response $response){
        $ag = $requestInput->getArguments();
        $genereService = new GenereService();
        $genere = new Genere();
        $genere->setIdGenere($ag['idGenere']);
        if ( $genereService->deleteGenere($genere) == 1) {
            session()->flash()->set(
                'succesMessage',
                'Genere eliminato con successo!'
            );
        } else {
            session()->flash()->set(
                'errorMessage',
                'Errore nella cancellazione del Gnenere !'
            );
        }
        return $response;
    }
}