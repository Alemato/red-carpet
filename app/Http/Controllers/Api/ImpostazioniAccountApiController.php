<?php


namespace App\Http\Controllers\Api;


use App\Services\UserService;

class ImpostazioniAccountApiController
{
    public function deleteImg($response){
        $userService = new UserService();
        $session = app()->resolve(\Boot\Foundation\Http\Session::class);
        $res =$userService->deleteImgUser();
        if($res !==0){
            $userService->refreshSessionUser($userService->getUser());
            $session->flash()->set('DeleteStatus', 'ok');
            $obj = ['DeleteStatus' => 'ok'];
        } else{
            $session->flash()->set('DeleteStatus', 'fail');
            $obj = ['DeleteStatus' => 'fail'];
        }
        $response->getBody()->write(json_encode( $obj, JSON_PRETTY_PRINT));
        return $response->withHeader('Content-Type', 'application/json');
    }
}