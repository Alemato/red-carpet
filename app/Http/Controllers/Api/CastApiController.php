<?php


namespace App\Http\Controllers\Api;


use App\Model\CastApi;
use App\Model\Immagine;
use App\Services\FilmMakerService;
use App\Support\RequestInput;

class CastApiController
{
    public function show(RequestInput $requestInput, $response){
        $filmmakersService = new FilmMakerService();
        try {
            $filmmakersObjs = $filmmakersService->getFilmmakarByNameOrSurname($requestInput->getQueryString()['q']);
            $json = array();
            foreach ($filmmakersObjs as $filmmakersObj){
                $castApiObj = new CastApi();
                $castApiObj->id = $filmmakersObj->getIdFilmMaker();
                $avatar = $filmmakersObj->getImgAvatar();
                if($avatar != new Immagine()){
                    $castApiObj->avatar_url = asset($filmmakersObj->getImgAvatar()->getDato());
                }else{
                    $castApiObj->avatar_url = asset('img/filmmakers/avatar/star_big.jpg');
                }
                $castApiObj->full_name = $filmmakersObj->getNome()." ".$filmmakersObj->getCognome();
                $castApiObj->description = decodifica_array_filmmakers_to_string($filmmakersObj->getTipologiaPrincipale());
                array_push($json, $castApiObj);
            }
            $obj = new \stdClass();
            $obj->total_count = count($filmmakersObjs);
            $obj->incomplete_results = true;
            $obj->items = $json;
        }
        catch (\Exception $exception){
            $obj = new \stdClass();
            $obj->total_count = 0;
            $obj->incomplete_results = true;
            $obj->items = array();
        }
        $response->getBody()->write(json_encode( $obj, JSON_PRETTY_PRINT));
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function deleteFilmmaker(RequestInput $requestInput)
    {
        $parameters = $requestInput->getArguments();
        $filmmakersService = new FilmMakerService();
        $filmmaker = $filmmakersService->getFilmakerById($parameters['idFilmmaker']);
        $filmmakersService->deleteFilmMakerNominationByFilmmaker($filmmaker);

        if ($filmmakersService->deleteFilmmaker($filmmaker) == 1) {
            session()->flash()->set(
                'succesMessage',
                'Filmmaker ' . $filmmaker->getNome() . ' ' . $filmmaker->getCognome() . ' eliminato con successo!'
            );
        } else {
            session()->flash()->set(
                'errorMessage',
                'Errore nella cancellazione del Film ' . $filmmaker->getNome() . ' ' . $filmmaker->getCognome() . '!'
            );
        }
    }
}