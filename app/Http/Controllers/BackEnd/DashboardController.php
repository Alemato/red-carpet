<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\FilmService;
use App\Services\RecensioneService;
use App\Services\SerieTvService;
use App\Services\UserService;
use App\Support\View;

class DashboardController
{
    public function  show(View $view){
        $filmService = new FilmService();
        $lastFilm = $filmService->getUltimiDieciFilmCreati();
        $serieTvService = new SerieTvService();
        $lastSerietv = $serieTvService->getUltimeDieciSerieTvCreate();
        $recensioneService = new RecensioneService();
        $filmSerieTvTot = $filmService->countFilm() + $serieTvService->countSerieTv();
        $totRec = $recensioneService->countRecensioni();
        $userService = new UserService();
        $totUtenti = $userService->countUser();
     return $view('back.dashboard', ['lastFilm'=>$lastFilm, 'lastSerieTv'=>$lastSerietv, 'filmSerieTvTot'=>$filmSerieTvTot, 'totRec'=>$totRec, 'totUtenti'=> $totUtenti]);
    }

    public function users(View $view){
        $userService = new UserService();
        $lastUsers = $userService->getUltimiTrentaUtentiRegistrati();
        return $view('components.dashboard_users_component', ['lastUsers'=>$lastUsers]);
    }

    public function usersNormale(View $view){
        $userService = new UserService();
        $lastUsers = $userService->getUltimiDieciUtentiNormaliRegistrati();
        return $view('components.dashboard_users_component', ['lastUsers'=>$lastUsers]);
    }

    public function usersCritico(View $view){
        $userService = new UserService();
        $lastUsers = $userService->getUltimiDieciUtentiCriticiRegistrati();
        return $view('components.dashboard_users_component', ['lastUsers'=>$lastUsers]);
    }

    public function usersAdmin(View $view){
        $userService = new UserService();
        $lastUsers = $userService->getUltimiDieciUtentiAdminRegistrati();
        return $view('components.dashboard_users_component', ['lastUsers'=>$lastUsers]);
    }

    public function reviews(View $view){
        $reviewService = new RecensioneService();
        $lastReview = $reviewService->getUltimiTrentaRecensioniRegistrate();
        foreach ($lastReview as $k => $review){
            $lastReview[$k]->filmOrSerietv = $reviewService->getFilmORSerietvFromRecensione($review);
        }
        return $view('components.dashboard_recensioni_component', ['lastReview'=>$lastReview]);
    }

    public function reviewsNormale(View $view){
        $reviewService = new RecensioneService();
        $lastReview = $reviewService->getUltimiQuindiciRecensioniUtenteNormaleRegistrate();
        foreach ($lastReview as $k => $review){
            $lastReview[$k]->filmOrSerietv = $reviewService->getFilmORSerietvFromRecensione($review);
        }
        return $view('components.dashboard_recensioni_component', ['lastReview'=>$lastReview]);
    }
    public function reviewsCritici(View $view){
        $reviewService = new RecensioneService();
        $lastReview = $reviewService->getUltimiQuindiciRecensioniUtenteCriticoRegistrate();
        foreach ($lastReview as $k => $review){
            $lastReview[$k]->filmOrSerietv = $reviewService->getFilmORSerietvFromRecensione($review);
        }
        return $view('components.dashboard_recensioni_component', ['lastReview'=>$lastReview]);
    }
}