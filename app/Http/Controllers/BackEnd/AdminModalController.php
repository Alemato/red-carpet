<?php


namespace App\Http\Controllers\BackEnd;


use App\Model\Distributore;
use App\Model\Genere;
use App\Services\DistributoreService;
use App\Services\FilmMakerService;
use App\Services\GenereService;
use App\Support\RequestInput;
use App\Support\View;

class AdminModalController
{
    public function show(RequestInput $requestInput, View $view)
    {
        //dd($requestInput->getArguments());
        switch ($requestInput->getArguments()['modal']) {
            case "create_distributore":
                return $view('components.create_distributore_modal');
            case "create_genere":
                return $view('components.create_genere_modal');
            case "create_filmmakers":
                return $view('components.create_filmmakers_modal');
            default:
                return $view('components.error');
        }
    }

    public function store(RequestInput $requestInput, View $view)
    {
        switch ($requestInput->getArguments()['modal']) {
            case "create_distributore":
            {
                $data = $requestInput->all();
                //dd($data);
                $distributoreObj = new Distributore();
                $distributoreObj->setNome($data['nome_distributore']);
                $distributoreObj->setDescrizione($data['descrizione']);
                $distributoreService = new DistributoreService();
                $value = $distributoreService->insertDistributore($distributoreObj);
                return $view('components.error', ['success_mex' => ['title' => 'Distributore aggiunto con successo!', 'mex' => $distributoreObj->getNome() . ' è stato aggiunto con successo, clicca sulla x o su annulla per chiudere il modal.', 'mex_1' => 'Oppure inserisci un altro Distributore!', 'type' => 'distributore', 'text' => $distributoreObj->getNome(), 'value' => $value]]);
            }
            case "create_genere":
            {
                $data = $requestInput->all();
                //dd($data);
                $genereObj = new Genere();
                $genereObj->setNome($data['nome_genere']);
                $genereObj->setDescrizione($data['descrizione']);
                $genereService = new GenereService();
                $value = $genereService->insertGenere($genereObj);
                return $view('components.error', ['success_mex' => ['title' => 'Genere aggiunto con successo!', 'mex' => $genereObj->getNome() . ' è stato aggiunto con successo, clicca sulla x o su annulla per chiudere il modal.', 'mex_1' => 'Oppure inserisci un altro genere!', 'type' => 'genere', 'text' => $genereObj->getNome(), 'descrione' => $genereObj->getDescrizione(), 'value' => $value]]);
            }
            case "create_filmmakers":
            {
                $directory = public_path('img/filmmakers/avatar');
                $filmmakersService = new FilmMakerService();
                $filmmakersObj = $filmmakersService->insertFilmmakerModal($requestInput, $directory);
                return $view('components.error', ['success_mex' => ['title' => 'Filmmakers aggiunto con successo!', 'mex' => $filmmakersObj->getNome() . ' è stato aggiunto con successo, clicca sulla x o su Chiudi per chiudere il modal.', 'mex_1' => '', 'type' => 'filmmaker']]);
            }
            default:
                return redirect('/admin/create_film');
        }

    }
}