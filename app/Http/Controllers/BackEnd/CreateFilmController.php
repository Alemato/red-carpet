<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\FilmService;
use App\Support\RequestInput;
use App\Support\View;

class CreateFilmController
{
    public function show(View $view)
    {
        $filmService = new FilmService();
        return $view('back.create-film', $filmService->show());
    }

    public function store(RequestInput $requestInput){
        $filmService = new FilmService();
        if ($filmService->storeFilm($requestInput)) return redirect(asset('admin/film'));
        else return redirect(asset('admin/create_film'));
    }

}