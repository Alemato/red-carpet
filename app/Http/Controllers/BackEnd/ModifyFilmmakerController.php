<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\FilmMakerService;
use App\Support\RequestInput;
use App\Support\View;

class ModifyFilmmakerController
{
    public function show(RequestInput $requestInput, View $view)
    {
        $filmmakerService = new FilmMakerService();
        return $view('back.update-filmmaker', $filmmakerService->showModify($requestInput));
    }

    public function store(RequestInput $requestInput)
    {
        $filmmakerService = new FilmMakerService();
        if ($filmmakerService->modifyFilmmaker($requestInput)) return redirect(asset('admin/listfilmmaker'));
        else return redirect(asset('admin/modify_filmmaker/'.$requestInput->getArguments()['idFilmmaker']));
    }

}