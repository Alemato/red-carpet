<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\FilmService;
use App\Support\RequestInput;
use App\Support\View;

class ModifyFilmController
{
    public function show(RequestInput $requestInput, View $view)
    {
        $filmService = new FilmService();
        return $view('back.update-film', $filmService->showModify($requestInput));
    }

    public function store(RequestInput $requestInput)
    {
        $filmService = new FilmService();
        if($filmService->modifyFilm($requestInput)) return redirect(asset('admin/film'));
        else return redirect(asset('admin/modify_film/').$requestInput->getArguments()['idFilm']);
    }
}