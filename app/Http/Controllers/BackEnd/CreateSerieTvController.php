<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\SerieTvService;
use App\Support\RequestInput;
use App\Support\View;

class CreateSerieTvController
{
    public function show(View $view)
    {
        $serieTvService = new SerieTvService();
        return $view('back.create-serie-tv', $serieTvService->show());
    }

    public function store(RequestInput $requestInput)
    {
        $serieTvService = new SerieTvService();
        //$serieTvService->store($requestInput);
        if($serieTvService->store($requestInput)) return redirect(asset('admin/serietv'));
        else return redirect(asset('admin/create_serietv'));
    }

}