<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\FilmMakerService;
use App\Support\RequestInput;
use App\Support\View;

class CreateFilmmakerController
{
    public function show(View $view)
    {
        $filmmakerService = new FilmMakerService();
        return $view('back.create-filmmaker', $filmmakerService->show());
    }

    public function store(RequestInput $requestInput)
    {
        $filmmakerService = new FilmMakerService();
        if($filmmakerService->storeFilmmaker($requestInput)) return redirect(asset('admin/filmmaker'));
        else return redirect(asset('admin/create_filmmaker'));
    }
}