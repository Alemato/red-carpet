<?php


namespace App\Http\Controllers;


use App\Http\Request\RecenzioneRequest;
use App\Model\Recensione;
use App\Services\FilmService;
use App\Services\RecensioneService;
use App\Services\SerieTvService;
use App\Services\UserService;
use App\Support\View;

class RecenzioneController
{
    public function storeModalFilm(RecenzioneRequest $recenzioneInput, View $view){
        if($recenzioneInput->failed()) return $view('components.error' );
        //dd($recenzioneInput->idRecenzione, $recenzioneInput->titolo, $recenzioneInput->valutazione, $recenzioneInput->contenuto);
        $recenzioneService = new RecensioneService();
        $filmService = new FilmService();
        $userService = new UserService();
        $recezioneObj = new Recensione();
        $args = $recenzioneInput->getArguments();
        $film = $filmService->getFilmById($args['idFilm']);
        $recezioneObj->setTitolo($recenzioneInput->titolo);
        $recezioneObj->setContenutoRecensione($recenzioneInput->contenuto);
        $recezioneObj->setVoto($recenzioneInput->valutazione);
        $recezioneObj->setDataRecensione(date(DATE_ISO8601));
        $recezioneObj->setUtente($userService->getUser());
        $recenzioneService->insertRecensioneFilm($recezioneObj, $film->getIdFilm());
        $url_referer = $recenzioneInput->getRequest()->getServerParams()["HTTP_REFERER"]."?shwcr=true";
        return $view('components.error', ["redirect"=> true, "url_referer"=>$url_referer]);
    }

    public function storeModalSerietv(RecenzioneRequest $recenzioneInput, View $view){
        if($recenzioneInput->failed()) return $view('components.error' );
        //dd($recenzioneInput->idRecenzione, $recenzioneInput->titolo, $recenzioneInput->valutazione, $recenzioneInput->contenuto);
        $recenzioneService = new RecensioneService();
        $serieTvService = new SerieTvService();
        $userService = new UserService();
        $recezioneObj = new Recensione();
        $args = $recenzioneInput->getArguments();
        $film = $serieTvService->getSerieTvById($args['idSerieTv']);
        $recezioneObj->setTitolo($recenzioneInput->titolo);
        $recezioneObj->setContenutoRecensione($recenzioneInput->contenuto);
        $recezioneObj->setVoto($recenzioneInput->valutazione);
        $recezioneObj->setDataRecensione(date(DATE_ISO8601));
        $recezioneObj->setUtente($userService->getUser());
        $recenzioneService->insertRecensioneSerieTv($recezioneObj, $film->getIdSerieTv());
        $url_referer = $recenzioneInput->getRequest()->getServerParams()["HTTP_REFERER"]."?shwcr=true";
        return $view('components.error', ["redirect"=> true, "url_referer"=>$url_referer]);
    }

    public function updateModal(RecenzioneRequest $recenzioneInput, View $view){
        if($recenzioneInput->failed()) return $view('components.error' );
        $recenzioneService = new RecensioneService();
        $userService = new UserService();
        $recezioneObj = new Recensione();
        if($recenzioneInput->failed()) return $view('components.error' );
        $args = $recenzioneInput->getArguments();
        $recezioneObjOriginal = $recenzioneService->getRecensioneById($args['idRecenzione']);
        $recezioneObj->setIdRecensione($recezioneObjOriginal->getIdRecensione());
        $recezioneObj->setTitolo($recenzioneInput->titolo);
        $recezioneObj->setContenutoRecensione($recenzioneInput->contenuto);
        $recezioneObj->setVoto($recenzioneInput->valutazione);
        $recezioneObj->setDataRecensione(date(DATE_ISO8601));
        $recezioneObj->setUtente($userService->getUser());
        $recenzioneService->updateRecensione($recezioneObj);
        $url_referer = $recenzioneInput->getRequest()->getServerParams()["HTTP_REFERER"]."?shwcr=true";
        return $view('components.error', ["redirect"=> true, "url_referer"=>$url_referer]);
    }
}