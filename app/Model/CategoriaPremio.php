<?php


namespace App\Model;



class CategoriaPremio
{
    /* rappresenta la tabella intermedia Premio_TipologiaPremio */

    private $idCategoriaPremio;

    private TipologiaPremio $tipologia;        // Oggetto TipologiaPremio
    private Premio $premio;           // Oggetto Premio

    /**
     * CategoriaPremio constructor.
     */
    public function __construct()
    {
        $this->tipologia = new TipologiaPremio();
        $this->premio = new Premio();
    }

    /**
     * @return mixed
     */
    public function getIdCategoriaPremio()
    {
        return $this->idCategoriaPremio;
    }

    /**
     * @param mixed $idCategoriaPremio
     */
    public function setIdCategoriaPremio($idCategoriaPremio): void
    {
        $this->idCategoriaPremio = $idCategoriaPremio;
    }

    /**
     * @return mixed
     */
    public function getTipologia()
    {
        return $this->tipologia;
    }

    /**
     * @param mixed $tipologia
     */
    public function setTipologia($tipologia): void
    {
        $this->tipologia = $tipologia;
    }

    /**
     * @return mixed
     */
    public function getPremio()
    {
        return $this->premio;
    }

    /**
     * @param mixed $premio
     */
    public function setPremio(Premio $premio): void
    {
        $this->premio = $premio;
    }

}