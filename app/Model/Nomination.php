<?php


namespace App\Model;




class Nomination
{
    private $idNomination;
    private $descrizione;
    private $dataPremiazione;
    private $stato;                // falso = nomination, true = vittoria

    private Filmmaker $filmMaker;
    private CategoriaPremio $categoriaPremio;   //Oggetto tabbella intermedia tra Premio e Categoria

    /**
     * Nomination constructor.
     */
    public function __construct()
    {
        $this->filmMaker = new Filmmaker();
        $this->categoriaPremio = new CategoriaPremio();
    }

    /**
     * @return mixed
     */
    public function getIdNomination()
    {
        return $this->idNomination;
    }

    /**
     * @param mixed $idNomination
     */
    public function setIdNomination($idNomination): void
    {
        $this->idNomination = $idNomination;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @return mixed
     */
    public function getDataPremiazione()
    {
        return $this->dataPremiazione;
    }

    /**
     * @param mixed $dataPremiazione
     */
    public function setDataPremiazione($dataPremiazione): void
    {
        $this->dataPremiazione = $dataPremiazione;
    }

    /**
     * @return mixed
     */
    public function getFilmMaker()
    {
        return $this->filmMaker;
    }

    /**
     * @param mixed $filmMaker
     */
    public function setFilmMaker(FilmMaker $filmMaker): void
    {
        $this->filmMaker = $filmMaker;
    }

    /**
     * @return mixed
     */
    public function getCategoriaPremio()
    {
        return $this->categoriaPremio;
    }

    /**
     * @param mixed $categoriaPremio
     */
    public function setCategoriaPremio(CategoriaPremio $categoriaPremio): void
    {
        $this->categoriaPremio = $categoriaPremio;
    }

    /**
     * @return mixed
     */
    public function getStato()
    {
        return $this->stato;
    }

    /**
     * @param mixed $stato
     */
    public function setStato($stato): void
    {
        $this->stato = $stato;
    }


}