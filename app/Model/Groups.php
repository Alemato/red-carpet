<?php


namespace App\Model;


class Groups
{
    private $idGruppo;
    private $nome;
    private $descrizione;

    private array $utenti;
    private array $servizi;

    /**
     * Groups constructor.
     */
    public function __construct()
    {
        $this->utenti = [new User()];
        $this->servizi = [new Services()];
    }

    /**
     * @return mixed
     */
    public function getIdGruppo()
    {
        return $this->idGruppo;
    }

    /**
     * @param mixed $idGruppo
     */
    public function setIdGruppo($idGruppo): void
    {
        $this->idGruppo = $idGruppo;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @return array
     */
    public function getUtenti(): array
    {
        return $this->utenti;
    }

    /**
     * @param array $utenti
     */
    public function setUtenti(array $utenti): void
    {
        $this->utenti = $utenti;
    }

    /**
     * @return array
     */
    public function getServizi(): array
    {
        return $this->servizi;
    }

    /**
     * @param array $servizi
     */
    public function setServizi(array $servizi): void
    {
        $this->servizi = $servizi;
    }


}