<?php


namespace App\Model;


class Services
{
    private $idServizio;
    private $nome;
    private $descrizione;

    /**
     * Services constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIdServizio()
    {
        return $this->idServizio;
    }

    /**
     * @param mixed $idServizio
     */
    public function setIdServizio($idServizio): void
    {
        $this->idServizio = $idServizio;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }


}