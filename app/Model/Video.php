<?php


namespace App\Model;


class Video
{
    private $idVideo;
    private $nome;
    private $descrizione;
    private $dato;

    private $immagine;

    /**
     * Video constructor.
     */
    public function __construct()
    {
        $this->immagine = new Immagine();
    }

    /**
     * @return mixed
     */
    public function getIdVideo()
    {
        return $this->idVideo;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @return mixed
     */
    public function getDato()
    {
        return $this->dato;
    }

    /**
     * @return mixed
     */
    public function getImmagine()
    {
        return $this->immagine;
    }

    /**
     * @param mixed $idVideo
     */
    public function setIdVideo($idVideo): void
    {
        $this->idVideo = $idVideo;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @param mixed $dato
     */
    public function setDato($dato): void
    {
        $this->dato = $dato;
    }

    /**
     * @param mixed $immagine
     */
    public function setImmagine($immagine): void
    {
        $this->immagine = $immagine;
    }

}