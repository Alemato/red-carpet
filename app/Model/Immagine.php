<?php


namespace App\Model;


class Immagine
{
    private $idImmagione;
    private $nome;
    private $descrizione;
    private $dato;
    private $tipo;

    /**
     * Immagine constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIdImmagione()
    {
        return $this->idImmagione;
    }

    /**
     * @param mixed $idImmagione
     */
    public function setIdImmagione($idImmagione): void
    {
        $this->idImmagione = $idImmagione;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @return mixed
     */
    public function getDato()
    {
        return $this->dato;
    }

    /**
     * @param mixed $dato
     */
    public function setDato($dato): void
    {
        $this->dato = $dato;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo): void
    {
        $this->tipo = $tipo;
    }

}