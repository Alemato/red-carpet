<?php


namespace App\Model;


class Genere
{
    private $idGenere;
    private $nome;
    private $descrizione;

    /**
     * Genere constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIdGenere()
    {
        return $this->idGenere;
    }

    /**
     * @param mixed $idGenere
     */
    public function setIdGenere($idGenere): void
    {
        $this->idGenere = $idGenere;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }


}