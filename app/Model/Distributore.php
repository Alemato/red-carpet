<?php


namespace App\Model;

use App\Model\Immagine as Img;

class Distributore
{
    private $idDistributore;
    private $nome;
    private $descrizione;    //dati Aziendali

    private $img;

    /**
     * Distributore constructor.
     */
    public function __construct()
    {
        $this->img = new Img();
    }

    /**
     * @return mixed
     */
    public function getIdDistributore()
    {
        return $this->idDistributore;
    }

    /**
     * @param mixed $idDistributore
     */
    public function setIdDistributore($idDistributore): void
    {
        $this->idDistributore = $idDistributore;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg(Img $img): void
    {
        $this->img = $img;
    }

}