<?php


namespace App\Model;

use App\Model\Immagine as Img;

class MediaFilmSerieTv
{
    private $idMediaFilmSerieTv;

    private Immagine $img;
    private Filmmaker $filmMaker;

    /**
     * MediaFilmSerieTv constructor.
     */
    public function __construct()
    {
        $this->img = new Immagine();
        $this->filmMaker = new Filmmaker();
    }

    /**
     * @return mixed
     */
    public function getIdMediaFilmSerieTv()
    {
        return $this->idMediaFilmSerieTv;
    }

    /**
     * @param mixed $idMediaFilmSerieTv
     */
    public function setIdMediaFilmSerieTv($idMediaFilmSerieTv): void
    {
        $this->idMediaFilmSerieTv = $idMediaFilmSerieTv;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg(Img $img): void
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getFilmMaker()
    {
        return $this->filmMaker;
    }

    /**
     * @param mixed $filmMaker
     */
    public function setFilmMaker(Filmmaker $filmMaker): void
    {
        $this->filmMaker = $filmMaker;
    }

}