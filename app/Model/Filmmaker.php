<?php


namespace App\Model;

use App\Model\Immagine as Img;

class Filmmaker
{
    private $idFilmMaker;
    private $nome;
    private $cognome;
    private $biografia;
    private $breveDescrizione;
    private $citazione;
    private $tipologiaPrincipale;

    private $filmSerieTvCitazione;
    private Immagine $avatar;
    private Immagine $imgProfilo;
    private array $immaginiFilmMakers;      // Immagini che non riguardano Film e Serie TV

    /**
     * Filmmaker constructor.
     */
    public function __construct()
    {
        $this->avatar = new Immagine();
        $this->imgProfilo = new Immagine();
        $this->immaginiFilmMakers = [new Immagine()];
    }

    /**
     * @return mixed
     */
    public function getIdFilmMaker()
    {
        return $this->idFilmMaker;
    }

    /**
     * @param mixed $idFilmaker
     */
    public function setIdFilmMaker($idFilmMaker): void
    {
        $this->idFilmMaker = $idFilmMaker;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * @param mixed $cognome
     */
    public function setCognome($cognome): void
    {
        $this->cognome = $cognome;
    }

    /**
     * @return mixed
     */
    public function getBiografia()
    {
        return $this->biografia;
    }

    /**
     * @param mixed $biografia
     */
    public function setBiografia($biografia): void
    {
        $this->biografia = $biografia;
    }

    /**
     * @return mixed
     */
    public function getBreveDescrizione()
    {
        return $this->breveDescrizione;
    }

    /**
     * @param mixed $breveDescrizione
     */
    public function setBreveDescrizione($breveDescrizione): void
    {
        $this->breveDescrizione = $breveDescrizione;
    }

    /**
     * @return mixed
     */
    public function getCitazione()
    {
        return $this->citazione;
    }

    /**
     * @param mixed $citazione
     */
    public function setCitazione($citazione): void
    {
        $this->citazione = $citazione;
    }

    /**
     * @return mixed
     */
    public function getTipologiaPrincipale()
    {
        return $this->tipologiaPrincipale;
    }

    /**
     * @param mixed $tipologiaPrincipale
     */
    public function setTipologiaPrincipale($tipologiaPrincipale): void
    {
        $this->tipologiaPrincipale = $tipologiaPrincipale;
    }

    /**
     * @return mixed
     */
    public function getFilmSerieTvCitazione()
    {
        return $this->filmSerieTvCitazione;
    }

    /**
     * @param mixed $filmSerieTvCitazione
     */
    public function setFilmSerieTvCitazione($filmSerieTvCitazione): void
    {
        $this->filmSerieTvCitazione = $filmSerieTvCitazione;
    }

    /**
     * @return mixed
     */
    public function getImgAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setImgAvatar(Img $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getImgProfilo()
    {
        return $this->imgProfilo;
    }

    /**
     * @param mixed $imgProfilo
     */
    public function setImgProfilo(Img $imgProfilo): void
    {
        $this->imgProfilo = $imgProfilo;
    }

    /**
     * @return array
     */
    public function getImmaginiFilmMakers(): array
    {
        return $this->immaginiFilmMakers;
    }

    /**
     * @param array $immaginiFilmMakers
     */
    public function setImmaginiFilmMakers(array $immaginiFilmMakers): void
    {
        $this->immaginiFilmMakers = $immaginiFilmMakers;
    }

}