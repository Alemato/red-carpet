<?php


namespace App\Model;




class Ruolo
{
    private $idRuolo;
    private $ruoloInternoOpera;
    private $tipologia;

    private array $immaginiDiRuolo;      // Immagini di attori all'interno del film NULL
    private Filmmaker $filmMaker;

    /**
     * Ruolo constructor.
     */
    public function __construct()
    {
        $this->immaginiDiRuolo = [new Immagine()];
        $this->filmMaker = new Filmmaker();
    }

    /**
     * @return mixed
     */
    public function getIdRuolo()
    {
        return $this->idRuolo;
    }

    /**
     * @param mixed $idRuolo
     */
    public function setIdRuolo($idRuolo): void
    {
        $this->idRuolo = $idRuolo;
    }

    /**
     * @return mixed
     */
    public function getRuoloInternoOpera()
    {
        return $this->ruoloInternoOpera;
    }

    /**
     * @param mixed $ruoloInternoOpera
     */
    public function setRuoloInternoOpera($ruoloInternoOpera): void
    {
        $this->ruoloInternoOpera = $ruoloInternoOpera;
    }

    /**
     * @return mixed
     */
    public function getTipologia()
    {
        return $this->tipologia;
    }

    /**
     * @param mixed $tipologia
     */
    public function setTipologia($tipologia): void
    {
        $this->tipologia = $tipologia;
    }

    /**
     * @return array
     */
    public function getImmaginiDiRuolo(): array
    {
        return $this->immaginiDiRuolo;
    }

    /**
     * @param array $immaginiDiRuolo
     */
    public function setImmaginiDiRuolo(array $immaginiDiRuolo): void
    {
        $this->immaginiDiRuolo = $immaginiDiRuolo;
    }

    /**
     * @return mixed
     */
    public function getFilmMaker()
    {
        return $this->filmMaker;
    }

    /**
     * @param mixed $filmMaker
     */
    public function setFilmMaker(Filmmaker $filmMaker): void
    {
        $this->filmMaker = $filmMaker;
    }

}