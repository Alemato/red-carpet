<?php


namespace App\Model;

use App\Model\User as User;

class Recensione
{
    private $idRecensione;
    private $titolo;
    private $contenutoRecensione;
    private $voto;
    private $dataRecensione;

    private $utente;

    /**
     * Recensione constructor.
     */
    public function __construct()
    {
        $this->utente = new User();
    }

    /**
     * @return mixed
     */
    public function getIdRecensione()
    {
        return $this->idRecensione;
    }

    /**
     * @param mixed $idRecensione
     */
    public function setIdRecensione($idRecensione): void
    {
        $this->idRecensione = $idRecensione;
    }

    /**
     * @return mixed
     */
    public function getTitolo()
    {
        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo): void
    {
        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getContenutoRecensione()
    {
        return $this->contenutoRecensione;
    }

    /**
     * @param mixed $contenutoRecensione
     */
    public function setContenutoRecensione($contenutoRecensione): void
    {
        $this->contenutoRecensione = $contenutoRecensione;
    }

    /**
     * @return mixed
     */
    public function getVoto()
    {
        return $this->voto;
    }

    /**
     * @param mixed $voto
     */
    public function setVoto($voto): void
    {
        $this->voto = $voto;
    }

    /**
     * @return mixed
     */
    public function getUtente()
    {
        return $this->utente;
    }

    /**
     * @param mixed $utente
     */
    public function setUtente(User $utente): void
    {
        $this->utente = $utente;
    }

    /**
     * @return mixed
     */
    public function getDataRecensione()
    {
        return $this->dataRecensione;
    }

    /**
     * @param mixed $dataRecensione
     */
    public function setDataRecensione($dataRecensione): void
    {
        $this->dataRecensione = $dataRecensione;
    }

}