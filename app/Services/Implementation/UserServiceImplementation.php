<?php


namespace App\Services\Implementation;

use App\Http\Request\Rules\PasswordRules;
use App\Model\User;
use DB;
use Slim\Exception\HttpBadRequestException;

class UserServiceImplementation
{

    /**
     * UserServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setUserObject(User $user, $result): User
    {
        $user->setIdUser($result->idUser);
        $user->setNome($result->nome);
        $user->setCognome($result->cognome);
        $user->setEmail($result->email);
        $user->setPassword($result->password);
        $user->setToken($result->token);
        $user->setDataNascita($result->dataNascita);
        $user->setTipologiaUtenza($result->tipologiaUtenza);
        if (isset($result->codiceAlbo)) {
            $user->setCodiceAlbo($result->codiceAlbo);
        }
        if (isset($result->nomeAzienda)) {
            $user->setNomeAzienda($result->nomeAzienda);
        }
        $user->setBiografia($result->biografia);
        $user->setStato($result->stato);
        return $user;
    }

    private function setUserObjectList($userList, $result) {
        foreach ($result as $obj) {
            $user = new User();
            $user = $this->setUserObject($user, $obj);
            array_push($userList, $user);
        }
        return $userList;
    }

    private function setUserObjectForUltimi(User $user, $result): User{
        $user->setIdUser($result->idUser);
        $user->setNome($result->nome);
        $user->setCognome($result->cognome);
        $user->setTipologiaUtenza($result->tipologiaUtenza);
        $user->createDate = $result->createDate;
        return $user;
    }


    private function setUserListObjectForUltimi($userList, $result) {
        foreach ($result as $obj) {
            $user = new User();
            $user = $this->setUserObjectForUltimi($user, $obj);
            array_push($userList, $user);
        }
        return $userList;
    }

    public function getUserByEmail(string $email): User
    {
        $user = new User();
        $result = DB::select('select * from users where email = ?', [$email]);
        if (empty($result) || count($result) > 1) {
            return $user;
        }
        return self::setUserObject($user, $result[0]);
    }

    public function getUserByIdByEmailByPassword(int $id, string $email, string $password)
    {
        $user = new user();
        $result = DB::select('select * from users where idUser = ? and email = ? and password = ?', [$id, $email, $password]);
        return self::setUserObject($user, $result[0]);
    }

    public function insertUserCritico(User $user)
    {
        $result = DB::insert('INSERT INTO users (nome, cognome, email, password, dataNascita, tipologiaUtenza, codiceAlbo, nomeAzienda, biografia, stato) VALUE (?,?,?,?,?,?,?,?,?,?)',
            [$user->getNome(), $user->getCognome(), $user->getEmail(), $user->getPassword(), $user->getDataNascita(), $user->getTipologiaUtenza(), $user->getCodiceAlbo(), $user->getNomeAzienda(), $user->getBiografia(), $user->getStato()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertUserGroupsTwo(User $user)
    {
        $result = DB::insert('INSERT INTO users_has_groups (user, gruppo) VALUES (?, 2)', [$user->getIdUser()]);
        throw_when(!($result), "errore nel inserimento nel gruppo due (utenti critici)",  HttpBadRequestException::class);
    }

    public function insertUserNormale(User $user)
    {
        $result = DB::insert('INSERT INTO users (nome, cognome, email, password, dataNascita, tipologiaUtenza, biografia, stato) VALUE (?,?,?,?,?,?,?,?)',
            [$user->getNome(), $user->getCognome(), $user->getEmail(), $user->getPassword(), $user->getDataNascita(), $user->getTipologiaUtenza(), $user->getBiografia(), $user->getStato()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertUserGroupsOne(User $user)
    {
        $result = DB::insert('INSERT INTO users_has_groups (user, gruppo) VALUES (?, 1)', [$user->getIdUser()]);
        throw_when(!($result), "errore nel inserimento nel gruppo uno (utenti normali)",  HttpBadRequestException::class);
    }

    public function insertUserAdmin(User $user)
    {
        $result = DB::insert('INSERT INTO users (nome, cognome, email, password, dataNascita, tipologiaUtenza, stato) VALUE (?,?,?,?,?,?,?)',
            [$user->getNome(), $user->getCognome(), $user->getEmail(), $user->getPassword(), $user->getDataNascita(), $user->getTipologiaUtenza(), $user->getStato()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertUserGroupsThree(User $user)
    {
        $result = DB::insert('INSERT INTO users_has_groups (user, gruppo) VALUES (?, 3)', [$user->getIdUser()]);
        throw_when(!($result), "errore nel inserimento nel gruppo tre (utenti admin)",  HttpBadRequestException::class);
    }

    public function UpdateUser(User $user)
    {
        $result = DB::update('UPDATE users SET nome = ?, cognome=?, email=?, password=?, dataNascita=?, tipologiaUtenza=?, codiceAlbo=?, nomeAzienda=?, biografia=?, stato=? WHERE idUser=?',
            [$user->getNome(), $user->getCognome(), $user->getEmail(), $user->getPassword(), $user->getDataNascita(), $user->getTipologiaUtenza(), $user->getCodiceAlbo(), $user->getNomeAzienda(), $user->getBiografia(), $user->getStato(), $user->getIdUser()]);
        return $result;
    }

    public function toggleStateUser(User $user) {
        return DB::update('UPDATE users SET stato = ? WHERE idUser = ?', [$user->getStato(), $user->getIdUser()]);
    }

    public function getAllUser() {
        $result = DB::select("SELECT * FROM users");
        return self::setUserObjectList([], $result);
    }

    public function getUltimiDieciUtentiNormaliRegistrati() {
        $result = DB::select("SELECT * FROM users WHERE tipologiaUtenza = 0 ORDER BY createDate DESC LIMIT 10");
        return self::setUserListObjectForUltimi([], $result);
    }

    public function getUltimiDieciUtentiCriticiRegistrati() {
        $result = DB::select("SELECT * FROM users WHERE tipologiaUtenza = 1 ORDER BY createDate DESC LIMIT 10");
        return self::setUserListObjectForUltimi([], $result);
    }

    public function getUltimiDieciUtentiAdminRegistrati() {
        $result = DB::select("SELECT * FROM users WHERE tipologiaUtenza = 2 ORDER BY createDate DESC LIMIT 10");
        return self::setUserListObjectForUltimi([], $result);
    }

    public function getUltimiTrentaUtentiRegistrati() {
        $result = DB::select("SELECT * FROM users WHERE (tipologiaUtenza = 0 OR tipologiaUtenza = 1 OR tipologiaUtenza = 2) ORDER BY createDate DESC LIMIT 30");
        return self::setUserListObjectForUltimi([], $result);
    }

    public function getUserById(int $idUser) {
        $user = new User();
        $result = DB::select("SELECT * FROM users WHERE idUser = ?", [$idUser]);
        return self::setUserObject($user, $result[0]);
    }

    public function deleteUser(User $user) {
        return DB::delete('DELETE FROM users WHERE idUser = ?', [$user->getIdUser()]);
    }

    public function countUser(){
        $result = DB::select("SELECT COUNT(idUser) as numerouser FROM users ");
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerouser;
    }
}