<?php


namespace App\Services;


use App\Model\Film;
use App\Model\Filmmaker;
use App\Model\Immagine;
use App\Model\SerieTv;
use App\Services\Implementation\FilmMakerServiceImplementation;
use App\Services\Implementation\FilmServiceImplementation;
use App\Services\Implementation\ImmagineServiceImplementation;
use App\Services\Implementation\SerieTvServiceImplementation;
use App\Support\RequestInput;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Exception\HttpInternalServerErrorException;

class FilmMakerService
{

    /**
     * FilmMakerService constructor.
     */
    public function __construct()
    {
    }

    public function getFilmakerById(int $idFilmMaker): Filmmaker {
        $filmMakerServiceImpl = new FilmMakerServiceImplementation();
        $filmaker = $filmMakerServiceImpl->getFilmMakerById($idFilmMaker);

        if($filmaker->getFilmSerieTvCitazione() !== null){
            if ($filmaker->getFilmSerieTvCitazione() instanceof Film) {
                $filmServiceIlmp = new FilmServiceImplementation();
                $filmaker->setFilmSerieTvCitazione($filmServiceIlmp->getFilmById($filmaker->getFilmSerieTvCitazione()->getIdFilm()));
            } else {
                $serieTvServiceImpl = new SerieTvServiceImplementation();
                $filmaker->setFilmSerieTvCitazione($serieTvServiceImpl->getSerieTvById($filmaker->getFilmSerieTvCitazione()->getIdSerieTv()));
            }
        }

        $imgService = new ImmagineService();
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $idFilmMaker, 0);
        if (!empty($result)) {
            $filmaker->setImgAvatar($result[0]);
        }
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $idFilmMaker, 1);
        if (!empty($result)) {
            $filmaker->setImgProfilo($result[0]);
        }

        $filmaker->setImmaginiFilmMakers($imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $idFilmMaker, 5));

        return $filmaker;
    }

    public function getFilmMakerByIdNomination($idNomination): Filmmaker {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $filmaker = $filmmakerServiceImpl->getFilmMakerByIdNomination($idNomination);
        if($filmaker->getFilmSerieTvCitazione() !== null){
            if ($filmaker->getFilmSerieTvCitazione() instanceof Film) {
                $filmServiceIlmp = new FilmServiceImplementation();
                $filmaker->setFilmSerieTvCitazione($filmServiceIlmp->getFilmById($filmaker->getFilmSerieTvCitazione()->getIdFilm()));
            } else {
                $serieTvServiceImpl = new SerieTvServiceImplementation();
                $filmaker->setFilmSerieTvCitazione($serieTvServiceImpl->getSerieTvById($filmaker->getFilmSerieTvCitazione()->getIdSerieTv()));
            }
        }

        $imgService = new ImmagineService();
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmaker->getIdFilmMaker(), 0);
        if (!empty($result)) {
            $filmaker->setImgAvatar($result[0]);
        }
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmaker->getIdFilmMaker(), 1);
        if (!empty($result)) {
            $filmaker->setImgProfilo($result[0]);
        }
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmaker->getIdFilmMaker(), 5);
        if (!empty($result)) {
            $filmaker->setImmaginiFilmMakers($result);
        }
        return $filmaker;
    }

    public function getFilmMakerByIdNominationParziale($idNomination): Filmmaker {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->getFilmMakerByIdNomination($idNomination);
    }

    public function getFilmAndSerieTvByTitoloSearch($titolo): array {
        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $filmList = $filmService->getFilmByTitoloSearch($titolo);
        $serieTvList = $serieTvService->getSerieTvByTitoloSearch($titolo);
        foreach ($serieTvList as $serieTv) {
            array_push($filmList, $serieTv);
        }
        return $filmList;
    }

    public function getFilmAndSerieTvByFilmmaker(Filmmaker $filmmaker): array {
        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $filmList = $filmService->getFilmByFilmmaker($filmmaker);
        $serieTvList = $serieTvService->getSerieTvByFilmmaker($filmmaker);
        foreach ($serieTvList as $serieTv) {
            array_push($filmList, $serieTv);
        }
        return $filmList;
    }

    public function getFilmmakarByNameOrSurname($queryString)
    {
        $filmMakerServiceImpl = new FilmMakerServiceImplementation();
        $filmMakers = $filmMakerServiceImpl->searchFilmMakerByNameAndSurname($queryString);
        $flimMakersComplete = array();
        foreach ($filmMakers as $filmaker){
            if($filmaker->getFilmSerieTvCitazione() !== null) {
                if ($filmaker->getFilmSerieTvCitazione() instanceof Film) {
                    $filmServiceIlmp = new FilmServiceImplementation();
                    $filmaker->setFilmSerieTvCitazione($filmServiceIlmp->getFilmById($filmaker->getFilmSerieTvCitazione()->getIdFilm()));
                } else {
                    $serieTvServiceImpl = new SerieTvServiceImplementation();
                    $filmaker->setFilmSerieTvCitazione($serieTvServiceImpl->getSerieTvById($filmaker->getFilmSerieTvCitazione()->getIdSerieTv()));
                }
            }

            $imgServiceImpl = new ImmagineServiceImplementation();
            $res = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmaker->getIdFilmMaker(), 0);
            if(count($res) > 0){
                $filmaker->setImgAvatar($res[0]);
            }
            $res = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmaker->getIdFilmMaker(), 1);
            if(count($res) > 0){
                $filmaker->setImgProfilo($res[0]);
            }
            $filmaker->setImmaginiFilmMakers($imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmaker->getIdFilmMaker(), 5));
            array_push($flimMakersComplete, $filmaker);
        }
        return $flimMakersComplete;
    }

    public function insertFilmmaker(Filmmaker $filmmaker) {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $idFilmmaker = $filmmakerServiceImpl->insertFilmMaker($filmmaker);
        $filmmaker->setIdFilmMaker($idFilmmaker);

        $imgService = new ImmagineService();
        $imgService->insertImgFilmmaker($filmmaker->getImgAvatar(), $filmmaker->getIdFilmMaker());
        $imgService->insertImgFilmmaker($filmmaker->getImgProfilo(), $filmmaker->getIdFilmMaker());
        foreach ($filmmaker->getImmaginiFilmMakers() as $immagineFilmMaker) {
            $imgService->insertImgFilmmaker($immagineFilmMaker, $filmmaker->getIdFilmMaker());
        }
        return $idFilmmaker;
    }

    public function insertFilmmakerModal(RequestInput $requestInput, string $directory){
        $data = $requestInput->all();
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();
        $uploadedFile = $uploadedFiles['file_avatar'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = movefiles($directory, $uploadedFile);
            $imgObj = new Immagine();
            $imgObj->setNome($uploadedFile->getClientFilename());
            $imgObj->setDescrizione('img avatar');
            $imgObj->setDato('img/filmmakers/avatar' . DIRECTORY_SEPARATOR . $filename);
            $imgObj->setTipo(0);
        }
        $filmmakersObj = new Filmmaker();
        $filmmakersObj->setNome($data['nome']);
        $filmmakersObj->setCognome($data['cognome']);
        $filmmakersObj->setBiografia($data['biografia']);
        $filmmakersObj->setBreveDescrizione($data['descrizione_breve']);
        $filmmakersObj->setCitazione($data['frase_campione']);
        $filmmakersObj->setTipologiaPrincipale(codifica_array($data['tipologia_filmmakers']));
        if (isset($imgObj)){
            $filmmakersObj->setImgAvatar($imgObj);
        }else{
            $imgObj = new Immagine();
            $imgObj->setNome('star_big.jpg');
            $imgObj->setDescrizione('img avatar');
            $imgObj->setDato('img/filmmakers/avatar' . DIRECTORY_SEPARATOR . 'star_big.jpg');
            $imgObj->setTipo(0);
            $filmmakersObj->setImgAvatar($imgObj);
        }
        //dd($filmmakersObj);
        $filmmakersServiceI = new FilmMakerServiceImplementation();
        $last_insert = $filmmakersServiceI->insertFilmMaker($filmmakersObj);
        $imgServiceImpl = new ImmagineServiceImplementation();
        $imgServiceImpl->insertImg($filmmakersObj->getImgAvatar(), $last_insert);
        return $filmmakersObj;
    }

    public function udpdataFilmmaker(Filmmaker $filmmaker) {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->updateFilmMaker($filmmaker);
    }

    public function deleteFilmMakerNominationByFilmmaker($idFilmmaker) {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->deleteFilmMakerNominationByFilmmaker($idFilmmaker);
    }

    public function deleteFilmMakerNominationByIdFilmmakerAndIdNomination($idFilmmaker, $idNomination) {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->deleteFilmMakerNominationByIdFilmmakerAndIdNomination($idFilmmaker, $idNomination);
    }

    public function deleteFilmmaker(Filmmaker $filmmaker)
    {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $immagineService = new ImmagineService();
        $ruoloService = new RuoloService();

        if ($filmmaker->getImgAvatar() !== new Immagine()) {
            $result = $immagineService->deleteImg($filmmaker->getImgAvatar());
            throw_when($result === 0, "errore non riesco a cancellare l'immagine", HttpInternalServerErrorException::class);
            if ($filmmaker->getImgAvatar()->getDato() != null) {
                deleteFile(public_path($filmmaker->getImgAvatar()->getDato()));
            }
        }
        if ($filmmaker->getImgProfilo() !== new Immagine()) {
            $result = $immagineService->deleteImg($filmmaker->getImgProfilo());
            throw_when($result === 0, "errore non riesco a cancellare l'immagine", HttpInternalServerErrorException::class);
            if ($filmmaker->getImgProfilo()->getDato() != null) {
                deleteFile(public_path($filmmaker->getImgProfilo()->getDato()));
            }
        }
        if (!empty($filmmaker->getImmaginiFilmMakers())) {
            foreach ($filmmaker->getImmaginiFilmMakers() as $img) {
                if (isset($img) && $img !== new Immagine()) {
                    $result = $immagineService->deleteImg($img);
                    throw_when($result === 0, "errore non riesco a cancellare l'immagine", HttpInternalServerErrorException::class);
                    if ($img->getDato() != null) {
                        deleteFile(public_path($img->getDato()));
                    }
                }
            }
        }
        $ruoloService->deleteRuoloByIdFilmmaker($filmmaker->getIdFilmMaker());
        return $filmmakerServiceImpl->deleteFilmMaker($filmmaker);
    }

    public function getAllFilmmakerParziali()
    {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->getAllFilmMaker();
    }

    public function storeFilmmaker(RequestInput $requestInput): bool{
        $data = $requestInput->all();
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();

        $filmmakerObj = new Filmmaker();
        $filmmakerObj->setNome($data['nome']);
        $filmmakerObj->setCognome($data['cognome']);
        $filmmakerObj->setBiografia($data['biografia']);
        $filmmakerObj->setBreveDescrizione($data['descrizione_breve']);
        $filmmakerObj->setCitazione($data['frase_campione']);
        $filmmakerObj->setTipologiaPrincipale(codifica_array($data['tipologia_filmmakers']));

        $uploadedFile = $uploadedFiles['immagine_profilo'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $directory = public_path('img/filmmakers/profilo');
            $filename = movefiles($directory, $uploadedFile);
            $imgObj = new Immagine();
            $imgObj->setNome($uploadedFile->getClientFilename());
            $imgObj->setDescrizione('img profilo');
            $imgObj->setDato('img/filmmakers/profilo' . DIRECTORY_SEPARATOR . $filename);
            $imgObj->setTipo(1);
            $filmmakerObj->setImgProfilo($imgObj);
        }

        $uploadedFile = $uploadedFiles['name_avatar_static_squared'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $directory = public_path('img/filmmakers/avatar');
            $filename = movefiles($directory, $uploadedFile);
            $imgObj = new Immagine();
            $imgObj->setNome($uploadedFile->getClientFilename());
            $imgObj->setDescrizione('img avatar');
            $imgObj->setDato('img/filmmakers/avatar' . DIRECTORY_SEPARATOR . $filename);
            $imgObj->setTipo(0);
            $filmmakerObj->setImgAvatar($imgObj);
        } else {
            $imgObj = new Immagine();
            $imgObj->setNome('star_big.jpg');
            $imgObj->setDescrizione('img avatar');
            $imgObj->setDato('img/filmmakers/avatar' . DIRECTORY_SEPARATOR . 'star_big.jpg');
            $imgObj->setTipo(0);
            $filmmakerObj->setImgAvatar($imgObj);
        }

        $listImgs = array();
        foreach ($data['media']['descrizione'] as $k => $descrizione) {
            $uploadedFile = $uploadedFiles['media']['img'][$k];
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $directory = public_path('img/filmmakers/imgs');
                $filename = movefiles($directory, $uploadedFile);
                $imgObj = new Immagine();
                $imgObj->setNome($uploadedFile->getClientFilename());
                $imgObj->setDescrizione($descrizione);
                $imgObj->setDato('img/filmmakers/imgs' . DIRECTORY_SEPARATOR . $filename);
                $imgObj->setTipo(5);
                array_push($listImgs, $imgObj);
            }
        }
        $filmmakerObj->setImmaginiFilmMakers($listImgs);

        // salvo il filmmaker
        $res = self::insertFilmmaker($filmmakerObj);
        $filmmakerObj->setIdFilmMaker($res);
        session()->flash()->set("successCreationFilmmaker", "Il Filmmaker: ".$filmmakerObj->getNome()." ".$filmmakerObj->getCognome()." è stato creato con successo");
        return true;
        //dd($requestInput->all(), $requestInput->getRequest()->getUploadedFiles(), $filmmakerObj);
    }

    public function getFilmmakerFilmByNomeCognome(string $persona){
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $imgService = new ImmagineService();
        $filmmakerList = $filmmakerServiceImpl->getFilmmakerFilmByNomeCognome($persona);
        foreach ($filmmakerList as $k=>$filmmaker){
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
            if (!empty($result)) {
                $filmmaker->setImgAvatar($result[0]);
            }
        }
        return $filmmakerList;
    }
    public function getFilmmakerByNomeOrCognome(string $persona){
    $filmmakerServiceImpl = new FilmMakerServiceImplementation();
    $imgService = new ImmagineService();
    $filmmakerList = $filmmakerServiceImpl->getFilmmakerByNomeOrCognome($persona);
    foreach ($filmmakerList as $k=>$filmmaker){
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
        if (!empty($result)) {
            $filmmaker->setImgAvatar($result[0]);
        }
    }
    return $filmmakerList;
}

    public function getFilmmakerFilmByNomeCognomeRuolo(string $persona, int $ruolo){
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $imgService = new ImmagineService();
        $filmmakerList = $filmmakerServiceImpl->getFilmmakerFilmByNomeCognomeRuolo($persona, $ruolo);
        foreach ($filmmakerList as $k=>$filmmaker){
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
            if (!empty($result)) {
                $filmmaker->setImgAvatar($result[0]);
            }
        }
        return $filmmakerList;
    }

    public function getFilmmakerSerieTvByNomeCognome(string $persona){
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $imgService = new ImmagineService();
        $filmmakerList = $filmmakerServiceImpl->getFilmmakerSerieTvByNomeCognome($persona);
        foreach ($filmmakerList as $k=>$filmmaker){
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
            if (!empty($result)) {
                $filmmaker->setImgAvatar($result[0]);
            }
        }
        return $filmmakerList;
    }

    public function getFilmmakerSerieTvByNomeCognomeRuolo(string $persona, int $ruolo){
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $imgService = new ImmagineService();
        $filmmakerList = $filmmakerServiceImpl->getFilmmakerSerieTvByNomeCognomeRuolo($persona, $ruolo);
        foreach ($filmmakerList as $k=>$filmmaker){
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
            if (!empty($result)) {
                $filmmaker->setImgAvatar($result[0]);
            }
        }
        return $filmmakerList;
    }

    public function show(){
        $premioService = new PremioService();
        $tipologiaPremioService = new TipologiaPremioService();
        $listPremi = $premioService->getAllPremio();
        $listTipologiaPremi = $tipologiaPremioService->getAllTipologiaPremioForFilmmakers();
        return ['premi' => $listPremi, 'tipologiaPremi' => $listTipologiaPremi];
    }

    public function showModify(RequestInput $requestInput){
        $arguments = $requestInput->getArguments();
        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $nominationService = new NominationService();
        $premioService = new PremioService();
        $tipologiaPremioService = new TipologiaPremioService();
        $ruoloService = new RuoloService();
        $filmmaker = self::getFilmakerById($arguments['idFilmmaker']);
        $listRuoli = $ruoloService->getRuoliByIdFilmmaker($arguments['idFilmmaker']);
        $listPremi = $premioService->getAllPremio();
        $listTipologiaPremi = $tipologiaPremioService->getAllTipologiaPremioForFilmmakers();
        $listFilm = $filmService->getAllFilmByRuoliAndFilmaker($listRuoli, $filmmaker);
        $listSerieTv = $serieTvService->getAllSerieTvByRuoli($listRuoli, $filmmaker);
        $listTipologiePrincipali = decodifica_array($filmmaker->getTipologiaPrincipale());
        $selectTipologiePrincipali = [
            '0' => 'Altro',
            '1' => 'Attore',
            '2' => 'Regista',
            '3' => 'Distributore',
            '4' => 'Produttore',
            '5' => 'Sceneggiatore',
            '6' => 'Fotografia',
            '7' => 'Montaggio',
            '8' => 'Musica',
            '9' => 'Scenografo',
            '10' => 'Costumi',
            '11' => 'Effetti',
            '12' => 'Art Director',
            '13' => 'Trucco'
        ];
        $listTipologieFilmmaker = array();
        foreach ($selectTipologiePrincipali as $ks => $tipo_select) {
            $flag = false;
            foreach ($listTipologiePrincipali as $k => $tipo_scelto) {
                if ($ks == $tipo_scelto) {
                    $flag = true;
                    break;
                }
            }
            if ($flag) {
                $obj = [$tipo_select => 'selected=selected'];
                array_push($listTipologieFilmmaker, $obj);
            } else {
                $obj = [$tipo_select => ''];
                array_push($listTipologieFilmmaker, $obj);
            }
        }
        $listFilmografia = array();
        $ids = 0;
        foreach ($listFilm as $kf => $film) {
            if ($ids != $film->getIdFilm()) {
                $ids = $film->getIdFilm();
                array_push($listFilmografia, $film);
            }
        }
        $ids = 0;
        foreach ($listSerieTv as $ks => $serieTv) {
            if ($ids != $serieTv->getIdSerieTv()) {
                $ids = $serieTv->getIdSerieTv();
                array_push($listFilmografia, $serieTv);
            }
        }
        //dd($filmmaker->getFilmSerieTvCitazione() !== null);
        //dd($listFilmografia[0]->getNominations(), $listFilmografia[1]->getNominations(), $listFilmografia[2]->getNominations());
        //dd('premi' , $listPremi, 'tipologiaPremi' , $listTipologiaPremi, 'filmmaker' , $filmmaker, "listTipologieFilmmaker" ,$listTipologieFilmmaker, "listRuoli", $listRuoli, "listFilm", $listFilm, "listSerieTv", $listSerieTv, "listFilmografia" , $listFilmografia);
        return ['premi' => $listPremi, 'tipologiaPremi' => $listTipologiaPremi, 'filmmaker' => $filmmaker, "listTipologieFilmmaker" => $listTipologieFilmmaker, "listRuoli" => $listRuoli, "listFilm" => $listFilm, "listSerieTv" => $listSerieTv, "listFilmografia" => $listFilmografia];
    }

    public function modifyFilmmaker(RequestInput $requestInput){

        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $imgService = new ImmagineService();
        $arguments = $requestInput->getArguments();
        $data = $requestInput->all();
        $filmmakerObjOriginal = self::getFilmakerById($arguments['idFilmmaker']);
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();

        $filmmakerObjNew = new Filmmaker();
        $filmmakerObjNew->setIdFilmMaker($filmmakerObjOriginal->getIdFilmMaker());
        $filmmakerObjNew->setNome($data['nome']);
        $filmmakerObjNew->setCognome($data['cognome']);
        $filmmakerObjNew->setBiografia($data['biografia']);
        $filmmakerObjNew->setBreveDescrizione($data['descrizione_breve']);
        $filmmakerObjNew->setCitazione($data['frase_campione']);
        $filmmakerObjNew->setTipologiaPrincipale(codifica_array($data['tipologia_filmmakers']));

        if (isset($data['citazione_film'])) {
            if (str_starts_with($data['citazione_film'], 'f_')) {
                $filmmakerObjNew->setFilmSerieTvCitazione($filmService->getFilmById(explode("f_", $data['citazione_film'])[1]));
            } else {
                if (str_starts_with($data['citazione_film'], 's_')) {
                    $filmmakerObjNew->setFilmSerieTvCitazione($serieTvService->getSerieTvById(explode("s_", $data['citazione_film'])[1]));
                } else if (str_starts_with($data['citazione_film'], '')) {
                    $filmmakerObjNew->setFilmSerieTvCitazione(null);
                }
            }
        }

        $uploadedFile = $uploadedFiles['immagine_profilo'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            if($filmmakerObjOriginal->getImgProfilo()!==null){
                $directory = public_path('img/filmmakers/profilo');
                deleteFile(public_path($filmmakerObjOriginal->getImgProfilo()->getDato()));
                $filename = movefiles($directory, $uploadedFile);
                $imgObj = new Immagine();
                $imgObj->setIdImmagione($filmmakerObjOriginal->getImgProfilo()->getIdImmagione());
                $imgObj->setNome($uploadedFile->getClientFilename());
                $imgObj->setDescrizione('img profilo');
                $imgObj->setDato('img/filmmakers/profilo' . DIRECTORY_SEPARATOR . $filename);
                $imgObj->setTipo(1);
                //aggionro img
                $imgService->updateImgFilmmaker($imgObj, $filmmakerObjOriginal->getIdFilmMaker());
                $filmmakerObjNew->setImgProfilo($imgObj);
            } else {
                $directory = public_path('img/filmmakers/profilo');
                deleteFile(public_path($filmmakerObjOriginal->getImgProfilo()->getDato()));
                $filename = movefiles($directory, $uploadedFile);
                $imgObj = new Immagine();
                $imgObj->setNome($uploadedFile->getClientFilename());
                $imgObj->setDescrizione('img profilo');
                $imgObj->setDato('img/filmmakers/profilo' . DIRECTORY_SEPARATOR . $filename);
                $imgObj->setTipo(1);
                //aggionro img
                $imgService->insertImgFilmmaker($imgObj, $filmmakerObjOriginal->getIdFilmMaker());
                $filmmakerObjNew->setImgProfilo($imgObj);
            }

        }else {
            $filmmakerObjNew->setImgProfilo($filmmakerObjOriginal->getImgProfilo());
        }


        $uploadedFile = $uploadedFiles['name_avatar_static_squared'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $directory = public_path('img/filmmakers/avatar');
            $filename = movefiles($directory, $uploadedFile);
            $imgObj = new Immagine();
            $imgObj->setIdImmagione($filmmakerObjOriginal->getImgAvatar()->getIdImmagione());
            $imgObj->setNome($uploadedFile->getClientFilename());
            $imgObj->setDescrizione('img avatar');
            $imgObj->setDato('img/filmmakers/avatar' . DIRECTORY_SEPARATOR . $filename);
            $imgObj->setTipo(0);
            //aggionro img
            $imgService->updateImgFilmmaker($imgObj, $filmmakerObjNew->getIdFilmMaker());
            $filmmakerObjNew->setImgAvatar($imgObj);
        } else {
            if($filmmakerObjOriginal->getImgAvatar() !== null){
                $filmmakerObjNew->setImgAvatar($filmmakerObjOriginal->getImgAvatar());
            } else {
                $imgObj = new Immagine();
                $imgObj->setNome('star_big.jpg');
                $imgObj->setDescrizione('img avatar');
                $imgObj->setDato('img/filmmakers/avatar' . DIRECTORY_SEPARATOR . 'star_big.jpg');
                $imgObj->setTipo(0);
                //aggionro img
                $imgService->updateImgFilmmaker($imgObj, $filmmakerObjNew->getIdFilmMaker());
                $filmmakerObjNew->setImgAvatar($imgObj);
            }
        }


        // immaggini
        $array_new_img = array();
        $array_new_img_id = array();
        $array_old_img_id = array();
        $array_da_cancellare_img=array();
        $array_da_inserire_img=array();
        if(isset($data['media']['id_imgs'])){
            foreach ($data['media']['id_imgs'] as $kp => $idImg) {
                foreach ($filmmakerObjOriginal->getImmaginiFilmMakers() as $k => $imgOriginal) {
                    if ($imgOriginal->getIdImmagione() == $idImg) {
                        $img = new Immagine();
                        $uploadedFile=$uploadedFiles['media']['img'][$kp];
                        // controllo se ho caricato un file video
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/filmmakers/imgs');
                            deleteFile(public_path($imgOriginal->getDato()));
                            $filename = movefiles($directory, $uploadedFile);
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($uploadedFile->getClientFilename());
                            $img->setDescrizione($data['media']['descrizione'][$kp]);
                            $img->setDato("img/filmmakers/imgs". DIRECTORY_SEPARATOR . $filename);
                            $img->setTipo(5);

                            // aggiorno l'img
                            $imgService->updateImgFilmmaker($img, $filmmakerObjNew->getIdFilmMaker());

                        } else {
                            // la foto è quella vecchia
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($imgOriginal->getNome());
                            $img->setDato($imgOriginal->getDato());
                            $img->setDescrizione($imgOriginal->getDescrizione());
                            $img->setTipo($imgOriginal->getTipo());

                            //controllo se la descrione
                            if($imgOriginal->getDescrizione() != $data['media']['descrizione'][$kp]){
                                $img->setDescrizione($data['media']['descrizione'][$kp]);
                                // aggiorno descizione
                                $imgService->updateImgFilmmaker($img, $filmmakerObjNew->getIdFilmMaker());
                            }
                        }
                        array_push($array_new_img, $img);
                    }
                }
            }

            // array_old_id
            foreach ($filmmakerObjOriginal->getImmaginiFilmMakers() as $k => $immagine) {
                array_push($array_old_img_id, strval($immagine->getIdImmagione()));
            }
            // array_new_id
            foreach ($data['media']['id_imgs'] as $kp => $idImmagine) {
                array_push($array_new_img_id, $idImmagine);
            }

            // array di id da cancellare
            $array_da_cancellare_img = array_diff($array_old_img_id, $array_new_img_id);

            foreach ($array_da_cancellare_img as $k => $idImg){
                foreach($filmmakerObjOriginal->getImmaginiFilmMakers() as $ki => $immagine){
                    if($immagine->getIdImmagione() == $idImg){
                        // cancella le img
                        deleteFile(public_path($immagine->getDato()));
                        $imgService->deleteImg($immagine);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_img = array_diff($array_new_img_id, $array_old_img_id);

            foreach ($array_da_inserire_img as $kArray => $id) {
                foreach ($data['media']['id_imgs'] as $k => $idImg) {
                    if ($kArray === $k && $id === $idImg) {
                        $uploadedFile = $uploadedFiles['media']['img'][$k];
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/filmmakers/imgs');
                            $filename = movefiles($directory, $uploadedFile);
                            $imgCopertina = new Immagine();
                            $imgCopertina->setNome($uploadedFile->getClientFilename());
                            $imgCopertina->setDescrizione($data['media']['descrizione'][$k]);
                            $imgCopertina->setDato("img/filmmakers/imgs". DIRECTORY_SEPARATOR . $filename);
                            $imgCopertina->setTipo(5);

                            // aggiorno descizione
                            $imgService->insertImgFilmmaker($imgCopertina, $filmmakerObjNew->getIdFilmMaker());
                            array_push($array_new_img, $imgCopertina);
                        }
                    }
                }
            }
        } else{
            //elimina tutte le immagini
            foreach ($filmmakerObjOriginal->getImmaginiFilmMakers() as $k => $immagine){
                deleteFile(public_path($immagine->getDato()));
                $imgService->deleteImg($immagine);
            }
        }
        //$filmmakerObjNew->setImmaginiFilmMakers($array_new_img);

        self::udpdataFilmmaker($filmmakerObjNew);
        //dd($requestInput->all(), $requestInput->getRequest()->getUploadedFiles());
        session()->flash()->set("successModifyFilmmaker", "Il Filmmaker: ".$filmmakerObjNew->getNome()." ".$filmmakerObjNew->getCognome()." è stato modificato con successo");
        return true;
    }

    public function getFilmmakerByFilmAndTipologiaRuolo(Film $film, int $ruolo): array
    {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->getFilmmakerByFilmAndTipologiaRuolo($film, $ruolo);
    }

    public function getFilmmakerBySerieTvAndTipologiaRuolo(SerieTv $serieTv, int $ruolo): array
    {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->getFilmmakerBySerieTvAndTipologiaRuolo($serieTv, $ruolo);
    }

    public function getFilmmakerByNomeOrCognomeSearch(string $persona): array
    {
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $immagineService = new ImmagineService();

        $resultFilmmaker = $filmmakerServiceImpl->getFilmmakerByNomeOrCognome($persona);
        if (!empty($resultFilmmaker)) {
            $objList = array();
            foreach ($resultFilmmaker as $key => $filmmaker) {
                $obj = new \stdClass();
                $obj->titolo = $filmmaker->getNome() . ' ' . $filmmaker->getCognome();
                $imgProfilo = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
                if (!empty($imgProfilo)) {
                    $obj->datoImg = asset($imgProfilo[0]->getDato());
                } else {
                    $obj->datoImg = asset('img/filmmakers/avatar/star_big.jpg');
                }
                $obj->tipo = decodifica_array_filmmakers_to_string($filmmaker->getTipologiaPrincipale());
                $obj->link = asset('filmmaker/' . $filmmaker->getIdFilmMaker());
                array_push($objList, $obj);
            }
            return $objList;
        }
        return [];
    }

    public function getFilmmakerByTipologiaPrincipale($tp, $limit, $offset){
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $imgService = new ImmagineService();
        $filmmakerList = $filmmakerServiceImpl->getFilmmakerByTipologiaPrincipale((int)$tp, $limit, $offset);
        foreach ($filmmakerList as $k=>$filmmaker){
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
            if (!empty($result)) {
                $filmmaker->setImgAvatar($result[0]);
            }
        }
        return $filmmakerList;
    }

    public function getFilmmakerByTipologiaFilmmaker($tp, $limit, $offset){
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        $imgService = new ImmagineService();
        $filmmakerList = $filmmakerServiceImpl->getFilmmakerByTipologiaFilmmaker((int)$tp, $limit, $offset);
        foreach ($filmmakerList as $k=>$filmmaker){
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0);
            if (!empty($result)) {
                $filmmaker->setImgAvatar($result[0]);
            }
        }
        return $filmmakerList;
    }

    public function countFilmmakerByTipologiaPrincipale($tp){
        $filmmakerServiceImpl = new FilmMakerServiceImplementation();
        return $filmmakerServiceImpl->countFilmmakerByTipologiaPrincipale((int)$tp);
    }
}