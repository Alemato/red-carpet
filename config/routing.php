<?php

return [
    'controllers' => [
        'namespaces' => [
            '\\App\\Http\\Controllers\\',
            '\\App\\Http\\Controllers\\FrontEnd\\',
            '\\App\\Http\\Controllers\\BackEnd\\',
            '\\App\\Http\\Controllers\\Api\\'
        ]
    ]
];
